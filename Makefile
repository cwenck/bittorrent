CC := gcc
CFLAG_INCLUDE := -Iinclude -I.
CFLAGS := -Wall $(CFLAG_INCLUDE) -std=gnu99
LDFLAGS := -lcrypto -pthread -lm
#-lreadline
VPATH :=src:include:tests

MAIN_SRC := client.c
COMMON_SRC := log.c hash.c fileio.c tracker.c metafile_parse.c bencode.c helper.c piece_set.c util.c netmsg_packer.c netbuffer.c command.c peer.c common.c lockless_linklist.c thread_registry.c argtable3.c peer_thread.c decider.c accept.c
TEST_SRC := test-common.c test-fileio.c test-tracker.c test-hash.c test-piece-set.c test-netmsg.c test-linklist.c


TEST_OBJ := $(COMMON_SRC:.c=.o) $(TEST_SRC:.c=.o)
CLIENT_OBJ := $(COMMON_SRC:.c=.o) $(MAIN_SRC:.c=.o)
TEST_LOG_LEVEL := 0

#Tell make these targets don't create files
.PHONY : clean all release dev tests coverage gitlab-coverage gcov

TARGET=client

all: dev

debug: CFLAGS += -ggdb
debug: $(TARGET)

release: CFLAGS += -O3
release: $(TARGET)

# -pg allows the program to be profiled
dev: CFLAGS += -ggdb -fsanitize=address
dev: $(TARGET)
dev: LDFLAGS := -lasan $(LDFLAGS)

thread: CFLAGS += -ggdb -fsanitize=thread
thread: $(TARGET)
thread: LDFLAGS := -ltsan $(LDFLAGS)

profile: CFLAGS += -ggdb -pg -fprofile-arcs -ftest-coverage
profile: $(TARGET)
profile: LDFLAGS := -pg -lgcov $(LDFLAGS)

client: $(CLIENT_OBJ)
	@# $@ is the make target
	$(CC) -o $@ $(CLIENT_OBJ) $(LDFLAGS)

tests: test
	@ clear
	@ ./test -j0; true # the true will prevent make from generating an error when tests fail
	@ rm -f *.gcov *.gcno *.gcda

test: CFLAGS += -fsanitize=address -ggdb -D LOG_LEVEL=$(TEST_LOG_LEVEL) --coverage
test: LDFLAGS := -lasan $(LDFLAGS) -lcriterion -lgcov
test: clean $(TEST_OBJ) test-common.h
	@# $@ is the make target
	$(CC) -o test $(TEST_OBJ) $(LDFLAGS)

RUN_COVERAGE := gcov $(shell ls *.o | grep -v "test-" | grep -v "log.o" | grep -v "client.o") | sed -E "/^File\s'/{N;s/Lines executed/File lines executed/g}" || echo "Test Failure\nLines executed:0.00% of 0"; true

coverage: test
	@ clear
	@ ./test -q -j0 > /dev/null
	@ $(RUN_COVERAGE)
	@ rm -f *.gcov *.gcno *.gcda

# Assumes tests have already been compiled and run
gitlab-coverage:
	@ $(RUN_COVERAGE)
	@ rm -f *.gcov *.gcno *.gcda

gcov: test
	@ ./test -q -j0; true
	@ echo "GCOV files generated"

%.o: %.c %.h log.h
	@# $< is the first make dependency (the c file in this case)
	$(CC) -c $< $(CFLAGS)

clean:
	rm -rf *.o *.out *.gcov *.gcno *.gcda include/*.h.gch client test download.part
