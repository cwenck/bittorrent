#ifndef __FILEIO_H__
#define __FILEIO_H__

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>


/**
 * Initialize fileio with a specific file
 * @param  filepath     : path to the download file (will create it if it doesn't exist)
 * @param  total_length : length of the final file (will allocate space for it)
 * @param  piece_size   : size of a piece for this download
 * @return              : true or false depending on if the initialization was successful
 */
bool fileio_init(const char *filepath, size_t total_length, size_t piece_size);

/* Cleanup fileio data */
void fileio_destroy();

/**
 * Write a block of data to the file specified in fileio_init
 * @param  piece_index : zero indexed piece number
 * @param  offset      : offset in bytes to write to from the start of the piece
 * @param  data        : the block data to write
 * @param  data_len    : length of the block data to write
 * @return             : true or false depending on if the write was successful
 */
bool fileio_write_block(uint32_t piece_index, size_t offset, void *data, size_t data_len);

/**
 * Read a block of data from the file specified in fileio_init
 * @param  piece_index : zero indexed piece number
 * @param  offset      : offset in bytes to write to from the start of the piece
 * @param  data        : the block data to read
 * @param  data_len    : length of the block data to read
 * @return             : true or false depending on if the read was successful
 */
bool fileio_read_block(uint32_t piece_index, size_t offset, void *data, size_t data_len);

/**
 * Write a full piece of data to the file specified in fileio_init
 * @param  piece_index : zero indexed piece number
 * @param  data        : the full piece data to write
 * @return             : true or false depending on if the write was successful
 */
bool fileio_write_piece(uint32_t piece_index, void *data);

/**
 * Read a full piece of data from the file specified in fileio_init
 * @param  piece_index : zero indexed piece number
 * @param  data        : the full piece data to read
 * @return             : true or false depending on if the read was successful
 */
bool fileio_read_piece(uint32_t piece_index, void *data);


/**
 * Verify that a piece is correct.
 * @param  piece_index   : zero indexed piece number
 * @param  piece_len     : length of the piece
 * @param  expected_hash : expected hash of the piece to check against
 * @return               : true if the hash matches, or false otherwise
 */
bool fileio_verify_piece(uint32_t piece_index, size_t piece_len, void *expected_hash);

/* Finalize the download by generating the actual files (Should be started as its own thread) */
void *fileio_finalize(void *thread_args);

#endif