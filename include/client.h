#ifndef __CLIENT_H__
#define __CLIENT_H__

#include <stdint.h>
#include <time.h>

struct client{
    uint8_t peer_id[20];
    int32_t port;
    int64_t uploaded;
    int64_t downloaded;
    struct timespec begin_time;
    struct timespec end_time;
};

#endif
