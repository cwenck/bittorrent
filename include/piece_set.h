#ifndef __PIECE_SET_H__
#define __PIECE_SET_H__

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>

#include "common.h"
#include "hash.h"
#include "peer.h"

#define MAX_ALLOWED_BLOCK_SIZE 32768    // Max block size our client will accept from others
#define DEFAULT_BLOCK_SIZE 16384        // Default block size our client will send requests for

#define BLOCK_TIMEOUT_SEC 10           // Timeout after which blocks should be changed to missing

enum piece_status {
    PIECE_MISSING = 0,          // No known peers have the piece
    PIECE_DOWNLOADING,          // Piece is currently being downloaded
    PIECE_FULLY_REQUESTED,      // All blocks have been requested
    PIECE_STORED,               // Piece is fully available on disk
};

enum block_status {
    BLOCK_MISSING = 0,          // Block has not been requested or downloaded from anyone
    BLOCK_REQUESTED,            // Block has been requested
    BLOCK_STORED                // Block has been downloaded and has been written to disk
};

struct block_data {
    enum block_status status;
    uint32_t piece_index;
    uint32_t block_index;
    size_t offset;
    size_t len;

    uint32_t pending_requests;  // Number of threads that have requested this block
};

struct piece_data {
    enum piece_status status;   // what state the piece is currently in
    size_t len;                 // length of this piece (typically piece_size)

    uint32_t piece_index;       // index of the piece

    struct block_data *blocks;  // the data blocks we are waiting to receive
    size_t block_count;         // the number of blocks
    uint8_t hash[SHA1_SIZE];    // the expected hash of the piece

    uint32_t blocks_completed;
    uint32_t blocks_requested;

    pthread_rwlock_t lock;      // lock for piece to handle multiple threads
};

struct file_piece_set {
    const char *filepath;
    size_t file_size;
    size_t piece_size;
    size_t block_size;

    uint32_t completed_pieces;
    uint32_t missing_pieces;

    uint32_t piece_count;
    struct piece_data *pieces;

    bool use_random;
    uint32_t *piece_rarity;

    bool finalized;             // Set to true when the final file has been generated

    pthread_rwlock_t lock;      // Only needs to be used for altering the completed_pieces count
};

struct piece_data *pieceset_find_piece(uint32_t index);
void pieceset_timeout_block(struct block_data *block);
bool pieceset_blocks_missing();

/**
 * Get a piece from the piece set
 * @param  piece_index : index of the piece to get
 * @return             : a pointer to the piece_data struct or NULL if it doesn't exist
 */
struct piece_data *pieceset_piece(uint32_t piece_index) ;

/**
 * Initialize the piece set from a the metafile.
 * @param  filepath         : the name of the temporary downloaded file
 * @param  random_selection : use random selection of pieces instead of rarest first
 * @return                  : true or false indicating if the initialization was successful
 */
bool pieceset_init_from_metafile(const char *filepath, bool random_selection);

/**
 * Initialize the piece set using the metafile and read in the file to use for seeding.
 * @param  filepath         : the name of the file to seed with
 * @param  random_selection : use random selection of pieces instead of rarest first
 * @return                  : true or false indicating if the initialization was successful
 */
bool pieceset_init_for_seeding(const char *filepath, bool random_selection);

/* Destroy the piece set. */
void pieceset_destroy();

/**
 * Print a singe piece.
 * @param piece_index : the zero based index of the piece to print
 */
void pieceset_print_piece(uint32_t piece_index);

/* Print all the pieces */
void pieceset_print_pieces();

/**
 * Print a particular block.
 * @param piece_index : the zero based index of the piece
 * @param block_index : the zero based index of the block
 */
void pieceset_print_block(uint32_t piece_index, uint32_t block_index);

/* Print all the blocks for this piece. */
void pieceset_print_blocks(uint32_t piece_index);

/* Print all the pieces and their corresponding blocks. */
void pieceset_print_all();

/* Print stats about the piece set */
void pieceset_print_stats();

/**
 * Generate the bit field array for the current state of the pieces.
 * The bitfield length can be gotten by calling piece_bitfield_size().
 * @param  bitfield : (CALLER MUST FREE) outputs the generated bitfield
 * @return          : true or false indicating success or failure
 */
bool pieceset_generate_bitfield(uint8_t **bitfield);

/**
 * Generate the piece map array for the current state of the pieces.
 * The piece map length can be gotten by calling piece_map_size().
 * @param  bitfield : (CALLER MUST FREE) outputs the generated piece map
 * @return          : true or false indicating success or failure
 */
bool pieceset_generate_piecemap(bool **piece_map);

/**
 * Check if there are available blocks to request.
 * @param  piece_index : index of the piece to check
 * @return             : true if there are available blocks, false otherwise
 */
bool pieceset_available_blocks(struct piece_data *piece);

/**
 * Check if there are available blocks to request in endgame.
 * @param  piece_index : index of the piece to check
 * @return             : true if there are available blocks, false otherwise
 */
bool pieceset_endgame_available_blocks(struct piece_data *piece);

/**
 * Get the next block to try and request for a particular piece.
 * @param  piece_index : the zero based index of the piece to get the next block for
 * @return             : the next block, or NULL if either all blocks have been stored
 */
struct block_data *pieceset_next_block(struct piece_data *piece);

/**
 * Get the next block to try and request for a particular piece, but for endgame.
 * @param  piece      : the zero based index of the piece to get the next block for
 * @param  last_block : (nullable) last block requested to start searching at
 * @return            : the next block, or NULL if either all blocks have been stored
 */
struct block_data *pieceset_endgame_next_block(struct piece_data *piece, struct block_data *last_block);

/**
 * Get the block data corresponding to a piece at a particular offset. This should be
 * called to get the block data when a PIECE message has been received.
 * @param  piece_index : index of the piece
 * @param  offset      : offset from the start of the piece
 * @return             : the block data corresponding to the piece index at the offset specified,
 *                       or NULL if it doesn't exist
 */
struct block_data *pieceset_find_block(uint32_t piece_index, uint32_t offset);

/* Notify the pieceset of a new have message, used to track piece rarity */
void pieceset_peer_has(uint32_t piece_index);

/**
 * Notify the pieceset of a new peer, used to track piece rarity
 * @param peer_data : the peer that was added
 */
void pieceset_peer_bitfield(struct peer *peer_data);

/**
 * Notify the pieceset of a deleted peer, used to track piece rarity
 * @param peer_data : the peer being deleted
 */
void pieceset_del_peer(struct peer *peer_data);

/**
 * Write a block to disk.
 * @param block : the block to write data to
 * @param data  : the data buffer for the block
 */
void pieceset_write_block(struct block_data *block, void *data);


/**
 * Read a stored block.
 * @param  piece_index : index of the piece the block is located in
 * @param  offset      : offset from the start of the piece to start reading data from
 * @param  data        : the buffer to write the data for this block into
 * @param  len         : length of the block
 * @return             : true if the data was successfully written, or false otherwise
 */
bool pieceset_read_block(uint32_t piece_index, size_t offset, void *data, size_t len);

/**
 * Select the piece to download blocks for.
 * @param  peer : the peer we are trying to download blocks from
 * @return      : the piece to download blocks for
 */
struct piece_data *pieceset_select_piece(struct peer *peer);

/**
 * Select the piece to download blocks for in endgame mode.
 * @param  peer : the peer we are trying to download blocks from
 * @return      : the piece to download blocks for
 */
struct piece_data *pieceset_endgame_select_piece(struct peer *peer);

/**
 * Check if all pieces been completed and stored.
 * @return : true if the pieces are all stored, false otherwise
 */
bool pieceset_done();

/* The number of pieces that have been completed */
uint32_t pieceset_pieces_completed();

/**
 * Check if a piece map has any pieces that are missing.
 * @param  piece_map : piece map to check
 * @return           : true if there are new pieces, false otherwise
 */
bool pieceset_has_new(bool *piece_map);

/**
 * Check if a piece map has any pieces that are not stored.
 * @param  piece_map : piece map to check
 * @return           : true if there are new pieces, false otherwise
 */
bool pieceset_endgame_has_new(bool *piece_map);

/* Get the piece size. */
uint32_t pieceset_piece_size();

/* Get the number of pieces */
uint32_t piece_count();

/**
 * Get the size of a bitfield.
 * @return : size of the bitfield
 */
size_t piece_bitfield_size();

/**
 * Get the size of a piece map.
 * @return : size of a piece map
 */
size_t piece_map_size();

/**
 * Generate a bitfield from a piece map.
 * @param  piece_map : piece map to generate the bitfield for
 * @param  bitfield  : (CALLER MUST FREE) output bitfield
 * @return           : false if there was a memory failure, true otherwise
 */
bool piece_map_to_bitfield(bool *piece_map, uint8_t **bitfield);

/**
 * Generate a bitfield from a piece map.
 * @param  bitfield  : bitfield to generate the piece map for
 * @param  piece_map : (CALLER MUST FREE) output piece map
 * @return           : false if there was a memory failure, true otherwise
 */
bool piece_bitfield_to_map(uint8_t *bitfield, bool **piece_map);


#endif