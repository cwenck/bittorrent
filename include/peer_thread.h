#ifndef PEER_THREAD_H
#define PEER_THREAD_H

#include "peer.h"
void update_recv_timestamp(struct peer *peer);

struct peer_thread_options{
	struct peer *self;
};

void *peer_thread(void *data);

#endif