#ifndef __PEER_H__
#define __PEER_H__

#include <arpa/inet.h>
#include <pthread.h>
#include <uthash.h>
#include <stdbool.h>
#include <poll.h>
#include <semaphore.h>

#include "lockless_linklist.h"

#define NUM_OUTGOING 125
#define NUM_INCOMING 200

struct request {
    bool completed;
    struct block_data *block;
    struct timespec req_at;

};

struct in_request {
    bool completed;
    size_t piece_index, offset, len;
};

struct peer {
    int sockfd, notify;             /* The UTHash key */
    struct sockaddr_in addr;

    char pstr[256];
    uint8_t peer_id[20];

    bool *piece_map;        /* Array of pieces this peer has */

    bool sent_handshake;    /* did our client send a handshake */
    bool recv_handshake;    /* did our client recv a handshake */

    bool am_choking;        /* We are choking this peer*/
    bool am_interested;     /* We are intereseted in this peer*/
    bool peer_choking;      /* We are being choked by this peer*/
    bool peer_interested;   /* This peer is interested in us */

    struct piece_data *piece_downloading;

    struct timespec last_sent;
    size_t bytes_received_last;
    size_t bytes_received;

    struct timespec last_recv;
    size_t bytes_sent_limit;
    size_t bytes_sent;

    uint8_t *block_send_buf;
    uint32_t block_send_len;
    bool buf_recv_b;

    uint8_t *recv_buf;
    uint32_t recv_len;
    bool len_recv_b;

    uint8_t len_bytes_recv;
    uint64_t buf_bytes_recv;

    struct request outgoing_requests[NUM_OUTGOING];
    size_t requests_pending;

    struct in_request incoming_requests[NUM_INCOMING];
    uint8_t incoming_requests_pending;

    size_t block_index;

    unsigned int rand_seed;

    bool endgame;
    struct block_data *endgame_last_block;

    pthread_rwlock_t lock;
    UT_hash_handle hh;      /* Make peers hashable */
};

#include "piece_set.h"

struct peer_hash {
    struct peer *head;      /* Head of the hash table */

    pthread_rwlock_t tbl_lock;
};


void peer_rdlock(struct peer *peer_data);
void peer_wrlock(struct peer *peer_data);
void peer_unlock(struct peer *peer_data);

/**
 * Initialize the global peers struct.
 * @return : true if successful, or false otherwise
 */
bool global_peers_init();

/**
 * Cleanup memory from the global peers struct.
 */

void global_peers_destroy();

/**
 * Create an empty peer struct
 * @return : a pointer to a peer struct, or NULL upon failure
 */
struct peer *peer_create();

/**
 * Free all resources associated with a peer struct
 * @param peer_data : the peer struct to destroy
 */
void peer_destroy(struct peer *peer_data);

/**
 * Interfaces with UTHASH and allows you to add a new peer to the global hashtable.
 * @param  peer_data : the filled-in struct to add to the hashtable
 * @return           : returns true if added, false if the key already exists
 */
bool peer_add(struct peer *peer_data);

/**
 * Finds a peer in the global hashtable and returns it if it exists.
 * @param  sockfd : the socket of the peer to look for
 * @return        : returns a peer corresponding to the socket, or NULL if not found
 */
struct peer *peer_find(int sockfd);

/**
 * Deletes a peer in the hashtable corresponding to the given socket.
 * @param sockfd : the socket of the peer to delete
 */
void peer_del(int sockfd);

/**
 * Gives the number of peers in the hashtable.
 * @return : number of peers
 */
int peer_count();


/**
 * Disconnect a peer.
 * @param peer_data : the peer to disconnect
 */
void peer_disconnect(struct peer *peer_data);

/**
 * Send requests that are not blocks of data
 * @param  peer_data [description]
 * @param  data      [description]
 * @param  len       [description]
 * @return           [description]
 */
bool peer_send(struct peer *peer_data, void *data, size_t len);
void *peer_process_requests(void *args);

//bool peer_process_request(struct peer *peer_data, void *request, size_t len);

#endif
