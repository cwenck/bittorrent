#ifndef METAFILE_PARSE_H
#define METAFILE_PARSE_H

#include <stdint.h>
#include <stdbool.h>
#include <arpa/inet.h>

#include "heapless-bencode/bencode.h"

struct tracker_peer {
	char *id, *address;
	long int port;
};

struct tracker_resp {
	struct tracker_peer *peers;
	char *failure, *warning, *tracker_id;
	long int interval, min_interval, complete, incomplete, peer_count;
};

struct mf_piece{
	uint8_t hash[20];
};

struct mf_file{
	char *path, *md5sum;
	long int length;
};

struct mf_info{
	uint8_t hash[20];
	struct mf_piece *pieces;
	struct mf_file *files;
	char *folder_name;
	long int piece_length, private, piece_count, file_count;
};

struct mf_announce_group{
	char **list;
	long int size;
};

struct mf_announce_list{
	struct mf_announce_group *groups;
	long int size;
};

struct metafile{
	struct mf_info info;
	struct mf_announce_list announce_list;
	char *announce, *comment, *by, *encoding;
	long int creation;
};

struct dict_pair_parse{
	char *key;
	bool  (*parse)(void *data, bencode_t *value);
};

struct tracker_resp *parse_tracker_resp(void *buf, int len);
void print_tracker_resp(struct tracker_resp *resp);
void tracker_resp_cleanup(struct tracker_resp *resp);

struct metafile *parse_metafile(void *buf, int len);
void metafile_cleanup(struct metafile *metafile);
void print_metafile(struct metafile *metafile, int print_hashes);
int64_t get_filesize(struct metafile *metafile);

#endif
