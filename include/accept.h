#ifndef __ACCEPT_THREAD_H__
#define __ACCEPT_THREAD_H__

void *accept_thread(void *args);

/* Creates the thread above so you don't have to deal with that */

void begin_accept();

#endif