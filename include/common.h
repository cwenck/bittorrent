#ifndef __COMMON_H__
#define __COMMON_H__

/* Global variables */
#include <stdint.h>
#include <stdbool.h>

extern struct metafile *g_metafile;
extern struct client *g_client_info;
extern struct tracker *g_tracker_info;
extern struct peer_hash *g_peers;

extern uint32_t *new_pieces;
extern uint32_t pieces_count;

extern bool use_propshare;

int upload_limit;
int download_limit;

#define CONNECT_TIMEOUT_MILLIS 1000
#define PEER_LIMIT 35

#define UNUSED(x) (void)(x)

#endif
