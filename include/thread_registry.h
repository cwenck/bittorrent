#ifndef __THREAD_REGISTRY_H__
#define __THREAD_REGISTRY_H__

#define MAX_THREADS 1000

#include <stdbool.h>
/**
 * Start a new thread, and add its tid to the internal registry.
 * @param start_routing : the function to start running in the new thread
 * @param arg           : a argument to pass to the function
 * @return              : true if the tread was successfully started or false
 *                        if the thread couldn't be started because too many
 *                        other threads were currently running
 */
bool thread_start(void *(*start_routine)(void *), void *arg);

/**
 * A function to be called from within the running thread just before it exists to
 * remove itself from the thread registry.
 */
void thread_finished();

/**
 * Setup for the thread registry.
 * @return : true if setup succeeded or false otherwise
 */
bool thread_setup();

/**
 * Cleanup any state from the registry.
 * TODO cancel all running threads in here
 */
void thread_cleanup();


#endif