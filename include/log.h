#ifndef __LOG_H__
#define __LOG_H__

#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>

/**
 * LOG_LEVEL VALUES
 *
 * 0 = NONE
 * 1 = ERROR
 * 2 = WARN
 * 3 = INFO
 * 4 = VERBOSE
 * 5 = DEBUG
 * 6 = TRACE
 */

#ifndef LOG_LEVEL
#define LOG_LEVEL 6
#endif

/**/
#define LOG_INCLUDE_LEVEL 1
#define LOG_INCLUDE_FUNCTION 1
#define LOG_INCLUDE_FILE 1

/* Only set this to 1 if you are looking for where a print statement is located */
#ifndef LOG_INCLUDE_PRINT_LOCATION
#define LOG_INCLUDE_PRINT_LOCATION 0
#endif

#define LOG_OUTPUT_STREAM stderr
#define LOG_ERROR_STREAM stderr

#define LOGPRINT_OUTPUT_STREAM stdout
#define LOGPRINT_ERROR_STREAM stdout

/* Will not blacklist errors/warnings from these files */
#define LOG_BLACKLIST_FILES {}
// #define LOG_BLACKLIST_FILES { "src/peer_thread.c" }

#define TXT_ALL_RESET "\e[0m"

#define TXT_FMT_BOLD "\e[1m"
#define TXT_FMT_DIM "\e[2m"
#define TXT_FMT_NOBOLD "\e[21m"
#define TXT_FMT_NODIM "\e[22m"

#define TXT_COLOR_DEFAULT "\e[39m"
#define TXT_COLOR_BLACK "\e[30m"
#define TXT_COLOR_RED "\e[31m"
#define TXT_COLOR_GREEN "\e[32m"
#define TXT_COLOR_YELLOW "\e[33m"
#define TXT_COLOR_BLUE "\e[34m"
#define TXT_COLOR_MAGENTA "\e[35m"
#define TXT_COLOR_CYAN "\e[36m"
#define TXT_COLOR_LIGHT_GRAY "\e[37m"
#define TXT_COLOR_DARK_GRAY "\e[90m"
#define TXT_COLOR_LIGHT_RED "\e[91m"
#define TXT_COLOR_LIGHT_GREEN "\e[92m"
#define TXT_COLOR_LIGHT_YELLOW "\e[93m"
#define TXT_COLOR_LIGHT_BLUE "\e[94m"
#define TXT_COLOR_LIGHT_MAGENTA "\e[95m"
#define TXT_COLOR_LIGHT_CYAN "\e[96m"
#define TXT_COLOR_WHITE "\e[97m"

#define ERROR_COLOR TXT_COLOR_RED
#define WARN_COLOR TXT_COLOR_YELLOW
#define INFO_COLOR TXT_COLOR_GREEN
#define VERBOSE_COLOR TXT_COLOR_MAGENTA
#define DEBUG_COLOR TXT_COLOR_BLUE
#define TRACE_COLOR TXT_COLOR_LIGHT_CYAN

#define ERRNO_COLOR TXT_COLOR_LIGHT_YELLOW

/* Do while is used to give a statement to put a semicolon after that doesnt do anything */
#define _LOGGING_PRINT_PREFIX(print_stream, level, color, function_name, file_name, line) \
    do { \
        if(LOG_INCLUDE_LEVEL){ \
            fprintf(print_stream, TXT_FMT_BOLD color " %-7s " TXT_ALL_RESET, level); \
        } \
        fprintf(print_stream, TXT_FMT_DIM " T%lu " TXT_ALL_RESET, pthread_self());\
        if(LOG_INCLUDE_FUNCTION){ \
            fprintf(print_stream, TXT_FMT_DIM " %s" TXT_ALL_RESET, function_name); \
            if(LOG_INCLUDE_FILE){ \
                fprintf(print_stream, TXT_FMT_DIM "::" TXT_ALL_RESET); \
            } else { \
                fprintf(print_stream, TXT_FMT_DIM ": " TXT_ALL_RESET); \
            } \
        } \
        if(LOG_INCLUDE_FILE){ \
            if(!(LOG_INCLUDE_FUNCTION && LOG_INCLUDE_LEVEL)){ \
                fprintf(print_stream, " "); \
            } \
            fprintf(print_stream, TXT_FMT_DIM "%s:%04lu: " TXT_ALL_RESET, file_name, line); \
        } \
    } while(0)

#define _LOGGING_LOCATION_PREFIX(print_stream, function_name, file_name, line) \
    do { \
        if(LOG_INCLUDE_PRINT_LOCATION) { \
            fprintf(print_stream, TXT_FMT_DIM "(%s::%s:%04lu)" TXT_ALL_RESET, function_name, file_name, line); \
        }\
    } while (0)

#if LOG_LEVEL == 0

#define ERROR 0
#define WARN 0
#define INFO 0
#define VERBOSE 0
#define DEBUG 0
#define TRACE 0

#elif LOG_LEVEL == 1

#define ERROR 1
#define WARN 0
#define INFO 0
#define VERBOSE 0
#define DEBUG 0
#define TRACE 0

#elif LOG_LEVEL == 2

#define ERROR 1
#define WARN 1
#define INFO 0
#define VERBOSE 0
#define DEBUG 0
#define TRACE 0

#elif LOG_LEVEL == 3

#define ERROR 1
#define WARN 1
#define INFO 1
#define VERBOSE 0
#define DEBUG 0
#define TRACE 0

#elif LOG_LEVEL == 4

#define ERROR 1
#define WARN 1
#define INFO 1
#define VERBOSE 1
#define DEBUG 0
#define TRACE 0

#elif LOG_LEVEL == 5

#define ERROR 1
#define WARN 1
#define INFO 1
#define VERBOSE 1
#define DEBUG 1
#define TRACE 0

#elif LOG_LEVEL == 6

#define ERROR 1
#define WARN 1
#define INFO 1
#define VERBOSE 1
#define DEBUG 1
#define TRACE 1

#endif /* LOG_LEVEL - IF ELSE */

/***************************************/
/* Logging Functions (DON'T USE THESE) */
/***************************************/

/**
 * For any of the buffer_dump functions below,
 * NULL can be passed to not print a label for the buffer.
 * No newlines are needed in the labels for the buffer dump functions.
*/
void trace_detailed_log(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...);
void trace_detailed_buffer_dump(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len, const char *format_str, ...);

void debug_detailed_log(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...);
void debug_detailed_buffer_dump(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len, const char *format_str, ...);

void verbose_detailed_log(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...);
void verbose_detailed_buffer_dump(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len, const char *format_str, ...);

void info_detailed_log(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...);
void info_detailed_buffer_dump(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len, const char *format_str, ...);

/* WARN log levels will write the ERROR_STREAM */
void warn_detailed_log(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...);
void warn_detailed_buffer_dump(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len, const char *format_str, ...);

/* Error exit will still exit even if LOG_LEVEL is 0 */
/* ERROR log levels will write the ERROR_STREAM */
void error_detailed_log(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...);
void error_detailed_buffer_dump(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len, const char *format_str, ...);
void error_detailed_exit(const char *func_name, const char *file, uint64_t line_num, int exit_code, const char *format_str, ...);

void trace_detailed_print(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...);
void debug_detailed_print(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...);
void verbose_detailed_print(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...);
void info_detailed_print(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...);
void warn_detailed_print(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...);
void error_detailed_print(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...);

void trace_detailed_hexprint(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len);
void debug_detailed_hexprint(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len);
void verbose_detailed_hexprint(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len);
void info_detailed_hexprint(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len);
void warn_detailed_hexprint(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len);
void error_detailed_hexprint(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len);

/**************************************/
/* Logging Macros (USE THESE INSTEAD) */
/**************************************/

/* Log a message with the corresponding level */
#define trace_log(format_str, ...) trace_detailed_log(__FUNCTION__, __FILE__, __LINE__, format_str, ##__VA_ARGS__)
#define debug_log(format_str, ...) debug_detailed_log(__FUNCTION__, __FILE__, __LINE__, format_str, ##__VA_ARGS__)
#define verbose_log(format_str, ...) verbose_detailed_log(__FUNCTION__, __FILE__, __LINE__, format_str, ##__VA_ARGS__)
#define info_log(format_str, ...) info_detailed_log(__FUNCTION__, __FILE__, __LINE__, format_str, ##__VA_ARGS__)
#define warn_log(format_str, ...) warn_detailed_log(__FUNCTION__, __FILE__, __LINE__, format_str, ##__VA_ARGS__)
#define error_log(format_str, ...) error_detailed_log(__FUNCTION__, __FILE__, __LINE__, format_str, ##__VA_ARGS__)

/**
 * These functions will print the contents of a buffer along with a label to the corresponding log level.
 * @param  buffer      : (void *) the buffer to print the contents of
 * @param  length      : (size_t) the length of the buffer
 * @param  format_str  : (char *) a label to print before the buffer [Pass NULL for nothing] possibly with format specifiers
 */
#define trace_buffer_dump(buffer, length, format_str, ...) trace_detailed_buffer_dump(__FUNCTION__, __FILE__, __LINE__, buffer, length, format_str, ##__VA_ARGS__)
#define debug_buffer_dump(buffer, length, format_str, ...) debug_detailed_buffer_dump(__FUNCTION__, __FILE__, __LINE__, buffer, length, format_str, ##__VA_ARGS__)
#define verbose_buffer_dump(buffer, length, format_str, ...) verbose_detailed_buffer_dump(__FUNCTION__, __FILE__, __LINE__, buffer, length, format_str, ##__VA_ARGS__)
#define info_buffer_dump(buffer, length, format_str, ...) info_detailed_buffer_dump(__FUNCTION__, __FILE__, __LINE__, buffer, length, format_str, ##__VA_ARGS__)
#define warn_buffer_dump(buffer, length, format_str, ...) warn_detailed_buffer_dump(__FUNCTION__, __FILE__, __LINE__, buffer, length, format_str, ##__VA_ARGS__)
#define error_buffer_dump(buffer, length, format_str, ...) error_detailed_buffer_dump(__FUNCTION__, __FILE__, __LINE__, buffer, length, format_str, ##__VA_ARGS__)

/* Print an error message and then exit with the exit_code. */
#define error_log_exit(exit_code, format_str, ...) error_detailed_exit(__FUNCTION__, __FILE__, __LINE__, exit_code, format_str, ##__VA_ARGS__)

/* Special Case Convenience MACROS */
#define mem_errlog() error_log("Memory allocation failure");
#define mem_errexit(exit_code) error_log_exit(exit_code, "Memory allocation failure");

/**
 * Print a message (usually multiline) without the logging prefix and terminating newline
 * that still follows the blacklist of files and log level for hiding output.
 */
#define trace_print(format_str, ...) trace_detailed_print(__FUNCTION__, __FILE__, __LINE__, format_str, ##__VA_ARGS__)
#define debug_print(format_str, ...) debug_detailed_print(__FUNCTION__, __FILE__, __LINE__, format_str, ##__VA_ARGS__)
#define verbose_print(format_str, ...) verbose_detailed_print(__FUNCTION__, __FILE__, __LINE__, format_str, ##__VA_ARGS__)
#define info_print(format_str, ...) info_detailed_print(__FUNCTION__, __FILE__, __LINE__, format_str, ##__VA_ARGS__)
#define warn_print(format_str, ...) warn_detailed_print(__FUNCTION__, __FILE__, __LINE__, format_str, ##__VA_ARGS__)
#define error_print(format_str, ...) error_detailed_print(__FUNCTION__, __FILE__, __LINE__, format_str, ##__VA_ARGS__)

/**
 * These functions will print the contents of a buffer in hex
 * @param  buffer      : (void *) the buffer to print the contents of in hex
 * @param  length      : (size_t) the length of the buffer
 */
#define trace_hex_print(buffer, length) trace_detailed_hexprint(__FUNCTION__, __FILE__, __LINE__, buffer, length)
#define debug_hex_print(buffer, length) debug_detailed_hexprint(__FUNCTION__, __FILE__, __LINE__, buffer, length)
#define verbose_hex_print(buffer, length) verbose_detailed_hexprint(__FUNCTION__, __FILE__, __LINE__, buffer, length)
#define info_hex_print(buffer, length) info_detailed_hexprint(__FUNCTION__, __FILE__, __LINE__, buffer, length)
#define warn_hex_print(buffer, length) warn_detailed_hexprint(__FUNCTION__, __FILE__, __LINE__, buffer, length)
#define error_hex_print(buffer, length) error_detailed_hexprint(__FUNCTION__, __FILE__, __LINE__, buffer, length)

#endif /* __LOG_H__ */
