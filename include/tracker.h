#ifndef __TRACK_H__
#define __TRACK_H__

#include <arpa/inet.h>
#include <pthread.h>
#include <uthash.h>
#include "peer.h"

#define DEFAULT_TRACKER_UPDATE_TIME 120

struct tracker {
    pthread_rwlock_t lock;
    uint64_t interval, min_interval;
    uint64_t complete, incomplete;
};

struct cnct_req {
    int64_t proto_id;
    int32_t action, trans_id;
};

struct cnct_resp {
    int32_t action, trans_id;
    int64_t cnct_id;
};

struct annc_req {
    int64_t cnct_id;
    int32_t action, trans_id;
    uint8_t info_hash[20];
    uint8_t peer_id[20];
    int64_t downloaded, left, uploaded;
    int32_t event, ip, key, num_want;
    int16_t port;
};

struct annc_peer {
    char ip[4];
    char port[2];
};

struct annc_resp {
    int32_t action, trans_id, interval, leechers, seeders;
    struct annc_peer peers[]; /* Variable length based on response! */
};

char *encode(uint8_t *blob, uint32_t size);
int32_t http_stop();
int32_t http_complete();
int32_t udp_stop();
int32_t udp_complete();
pthread_t init_tracker();

#endif
