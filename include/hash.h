#ifndef HASH_H
#define HASH_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#include <openssl/evp.h>

#define SHA1 (EVP_sha1())
#define MD5 (EVP_md5())

#define SHA1_SIZE 20
#define MD5_SIZE 16

/**
 * Initialize the checksum contetxt
 * @param  ctx        : the current context
 * @param  hash_type  : type of hash to use, see defines above (i.e. SHA1 or MD5)
 * @return            : true or false indicating if the initialzation was successful
 */
bool checksum_init(EVP_MD_CTX *ctx, const EVP_MD *hash_type);

/**
 * Add data to the current context
 * @param  ctx       : the current context
 * @param  data      : the data to add to the context
 * @param  data_len  : the length of the data
 * @return           : true or false indicating if the update was successful
 */
bool checksum_update(EVP_MD_CTX *ctx, const void *data, size_t data_len);

/**
 * Finalize a hash context and generate the actual hash.
 * @param  ctx      : the current context
 * @param  data     : (pass NULL if no more data to add) data to add to the context before generating the final hash
 * @param  data_len : (pass 0 if not more data to add) length of the data to add
 * @param  hash     : output buffer that the hash will be written to when generated
 *                    (MUST be able to hold 16 bytes for MD5)
 *                    (MUST be able to hold 20 bytes for SHA1)
 * @param  hash_len : the size of your buffer for the hash (to help avoid buffer overflows)
 * @return          : true or false indicating if the finish was successful
 */
bool checksum_finish(EVP_MD_CTX *ctx, const void *data, size_t data_len, void *hash, size_t hash_len);

/* Destroys the context */
void checksum_cleanup(EVP_MD_CTX *ctx);

#endif

