#ifndef __COMMAND_H__
#define __COMMAND_H__

#define CMD_MAX_LENGTH 1000

struct cmd;
typedef void (* CmdRunner)(struct cmd *cmd_data, char *arg_str);

struct cmd {
    const char *cmd_str;            // CMD string to match
    const char *usage_str;          // Usage error string for the command
    const char *description_str;    // Usage error string for the command
    CmdRunner run;                  // Function to execute the command
};

void command_loop();

#endif