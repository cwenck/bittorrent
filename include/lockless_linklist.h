#ifndef __LOCKLESS_LINKLIST_H__
#define __LOCKLESS_LINKLIST_H__

#include <stdbool.h>
#include <stdint.h>
#include <semaphore.h>

struct link_list_node {
    void *data;
    size_t data_len;
    struct link_list_node *next;
};

struct link_list {
    struct link_list_node *head;
    struct link_list_node *tail;
    sem_t list_sem;
};

typedef void (*LinkListDataCleanup)(void *);

/**
 * Create a new linked list.
 * @return : the created linked list, or NULL if the creation failed
 */
struct link_list *linklist_create();

/**
 * Destroy a linked list and free any data elements still stored.
 * @param link_list : linked list to free
 * @param cleanup   : (nullable) a cleanup function that will be called on each data element in the list
 */
void linklist_destroy(struct link_list *link_list, LinkListDataCleanup cleanup);

/**
 * Append an element to the list.
 * @param link_list : the list to append the data to
 * @param data      : the data to append to the list
 */
bool linklist_append(struct link_list *link_list, void *data, size_t data_len);

/**
 * Read an element from the list, and return immediately if the list is empty.
 * @param  link_list : the list to read data from
 * @param  data      : outputs the data buffer
 * @param  data_len  : (nullable) outputs the length of the data buffer
 * @return           : true, or false when the list is empty or if an error occurred
 */
bool linklist_tryread(struct link_list *link_list, uint8_t **data, size_t *data_len);

/**
 * Read an element from the list, and block if the list is empty.
 * @param  link_list : the list to read data from
 * @param  data      : outputs the data buffer
 * @param  data_len  : (nullable) outputs the length of the data buffer
 * @return           : true, or false if an error occurred
 */
bool linklist_read(struct link_list *link_list, uint8_t **data, size_t *data_len);


/**
 * Get the size of the linked list. Not necessarily exact
 * since the size may have changed by the time this function returns.
 * @param  link_list : the list to get the size of
 * @return           : the size of the list
 */
size_t linklist_size(struct link_list *link_list);

#endif