#ifndef __HELPER_H__
#define __HELPER_H__

#include <arpa/inet.h>

/**
 * Creates a new socket and connects on the specified address / port.
 * Returns -1 on failure.
 */
int32_t open_connection(struct sockaddr_in *addr);
/**
 * Creates a new socket for sending UDP data. Returns -1 on failure.
 */
int32_t open_udp_connection();

#endif
