#ifndef __SEND_H__
#define __SEND_H__

#include "peer.h"

/** Needs to be spawned as a thread.
 * starts listening on the port and accepts new connections.
 */
void *send_thread(void *);

/** 
 * Begin sending keepalives, please spawn as a thread.
 */
void *timestamp_send_thread(void *);

void update_send_timestamp(struct peer *);

#endif