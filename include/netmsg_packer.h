#ifndef __NETWORK_MESSAGE_PACKER_H__
#define __NETWORK_MESSAGE_PACKER_H__

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

enum msg_type {
    MSG_TYPE_CHOKE = 0,
    MSG_TYPE_UNCHOKE = 1,
    MSG_TYPE_INTERESTED = 2,
    MSG_TYPE_NOT_INTERESTED = 3,
    MSG_TYPE_HAVE = 4,
    MSG_TYPE_BITFIELD = 5,
    MSG_TYPE_REQUEST = 6,
    MSG_TYPE_PIECE = 7,
    MSG_TYPE_CANCEL = 8,
    MSG_TYPE_KEEP_ALIVE,
    MSG_TYPE_UNKNOWN
};

struct network_msg {
    enum msg_type type;
    size_t len;
    uint8_t *payload;
};


/**
 * Packing functions. The packet and packet_len parameters are output.
 * Packet length can be NULL if you don't care. The packet is a pointer
 * to a constant static buffer, so free should NOT be called on it.
*/
void pack_keep_alive(uint8_t **packet, size_t *packet_len);
void pack_choke(uint8_t **packet, size_t *packet_len);
void pack_unchoke(uint8_t **packet, size_t *packet_len);
void pack_interested(uint8_t **packet, size_t *packet_len);
void pack_not_interested(uint8_t **packet, size_t *packet_len);

/**
 * Packing functions. The packet and packet_len parameters are output.
 * Packet length can be NULL if you don't care. The packet needs to
 * be freed by the caller.
*/
bool pack_have(uint32_t piece_index, uint8_t **packet, size_t *packet_len);
bool pack_bitfield(void *bitfield, size_t bitfield_len, uint8_t **packet, size_t *packet_len);
bool pack_request(uint32_t piece_index, uint32_t offset, uint32_t block_len, uint8_t **packet, size_t *packet_len);
bool pack_piece(uint32_t piece_index, uint32_t offset, void *block_data, uint32_t block_len, uint8_t **packet, size_t *packet_len);
bool pack_cancel(uint32_t piece_index, uint32_t offset, uint32_t block_len, uint8_t **packet, size_t *packet_len);

/**
 * Packs the info hash and peer id into a packet. Uses Bittorrent's string identifier.
 * Packet must be freed after use.
 * @param info_hash  : 20-byte SHA1 hash of the info key in the metainfo file. This is the same info_hash that is transmitted in tracker requests.
 * @param peer_id    : 20-byte string used as a unique ID for the client. This is usually the same peer_id that is transmitted in tracker requests.
 * @param packet     : pointer in which to place the new packet
 * @param packet_len : length of the packet created by this function
 */
void pack_handshake(uint8_t *info_hash, uint8_t *peer_id, uint8_t **packet, size_t *packet_len);

/**
 * Generic unpacker that unpacks the type and payload from all packets.
 * @param packet      : the raw packet data (determines size from the length included in the packet)
 * @param type        : outputs the type of the packet
 * @param payload     : outputs a pointer to the payload within the packet (uses the same buffer)
 * @param payload_len : outputs the length of the payload
 */
void unpack_msg(void *packet, enum msg_type *type, uint8_t **payload, size_t *payload_len);
void unpack_msg_len(void *packet, size_t len, enum msg_type *type, uint8_t **payload, size_t *payload_len);

/**
 * Messages without message specific unpackers don't need to be unpacked
 * further than that done by the generic unpacker.
 */

/**
 * Message specific unpacker for the HAVE messages
 * @param payload     : the payload data gotten from unpack_msg()
 * @param piece_index : outputs the zero based piece index
 */
void unpack_have(void *payload, uint32_t *piece_index);

/**
 * Message specific unpacker for the BITFIELD messages
 * @param payload     : the payload data gotten from unpack_msg()
 * @param payload_len : the payload length gotten from unpack_msg()
 * @param bitfield    : (caller must free) outputs a bitfield of length payload_len
 */
void unpack_bitfield(void *payload, size_t payload_len, uint8_t **bitfield);

/**
 * Message specific unpacker for the REQUEST messages
 * @param payload     : the payload data gotten from unpack_msg()
 * @param piece_index : outputs the zero based piece index
 * @param offset      : outputs the block offset
 * @param block_len   : outputs the block length
 */
void unpack_request(void *payload, uint32_t *piece_index, uint32_t *offset, uint32_t *block_len);

/**
 * Message specific unpacker for the PIECE message
 * @param payload     : the payload data gotten from unpack_msg()
 * @param payload_len : the payload length gotten from unpack_msg()
 * @param piece_index : outputs the zero based piece index
 * @param offset      : outputs the block offset
 * @param block       : outputs the contents of the received block
 */
void unpack_piece(void *payload, size_t payload_len, uint32_t *piece_index, uint32_t *offset, uint8_t **block);

/**
 * Message specific unpacker for the CANCEL messages
 * @param payload     : the payload data gotten from unpack_msg()
 * @param piece_index : outputs the zero based piece index
 * @param offset      : outputs the block offset
 * @param block_len   : outputs the block length
 */
void unpack_cancel(void *payload, uint32_t *piece_index, uint32_t *offset, uint32_t *block_len);

/**
 * Unpacks a handshake given a packet.
 * If needed, pstr can be NULL, in which case, this function will not update the pstr.
 * pstr should be freed after use.
 * @param packet     : Packet to unpack
 * @param packet_len : Length of packet to unpack (how many bytes from recv)
 * @param pstr       : String identifier of the peer
 * @param info_hash  : 20-byte SHA1 hash of the info key in the metainfo file. This is the same info_hash that is transmitted in tracker requests.
 * @param peer_id    : 20-byte string used as a unique ID for the client. This is usually the same peer_id that is transmitted in tracker requests.
 * @return True on success, false otherwise
 */
void unpack_handshake(void *packet, uint32_t packet_len, char **pstr, uint8_t **info_hash, uint8_t **peer_id);

#endif