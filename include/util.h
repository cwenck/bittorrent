#ifndef __UTIL_H__
#define __UTIL_H__

#include <time.h>
#include <stdint.h>

/* Get a random double between 0 and 1 */
double rand_double();

/**
 * Get a random value in an inclusive range.
 * @param  min : min value in range
 * @param  max : max value in range
 * @return     : randum value
 */
uint32_t rand_range(uint32_t min, uint32_t max);

/**
 * Get the elapsed time between two timesepc structs (order doesn't matter)
 * @param  t1 : time one
 * @param  t2 : time two
 * @return    : elapsed time between the two structs
 */
struct timespec timespec_elapsed(struct timespec *t1, struct timespec *t2);

/**
 * Get the elapsed seconds between two timesepc structs (order doesn't matter)
 * @param  t1 : time one
 * @param  t2 : time two
 * @return    : elapsed seconds between the two structs
 */
uint64_t timespec_elapsed_sec(struct timespec *t1, struct timespec *t2);

#endif