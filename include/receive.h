#ifndef __RECV_H__
#define __RECV_H__

#define MAX_MESSAGE_SIZE 10000

/**
 * Begin polling, should be spawned as a thread
 * sockfd : Our socket to begin listening for new connections on.
 */
void *recv_poll(void *);

/**
 * Begin checking for timestamps, should be spawned as a thread.
 */
void *timestamp_receive_thread(void *);

void *recycle_data_received(void *);

#endif