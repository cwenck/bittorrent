#include <accept.h>

#include <poll.h>
#include <stdio.h>
#include <stdbool.h>
#include <netdb.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include "peer.h"
#include "common.h"
#include "netmsg_packer.h"
#include "log.h"
#include "util.h"
#include "metafile_parse.h"
#include "client.h"
#include "peer_thread.h"
#include "thread_registry.h"
#include "peer_thread.h"

int initialize_socket(char *port) {

    struct addrinfo hints, *res;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if (getaddrinfo(NULL, port, &hints, &res) != 0) {
        error_log("Error with getaddrinfo");
        return -1;
    }

    int sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (sockfd == -1) {
        error_log("Error with socket");
        return -1;
    }

    int enable = 1;
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) {
        error_log("setsockopt(SO_REUSEADDR) failed");
        return -1;
    }

    if (bind(sockfd, res->ai_addr, res->ai_addrlen) == -1) {
        error_log("Error with bind");
        return -1;
    }

    if (listen(sockfd, 100) == -1) {
        error_log("Error with listen: %s", strerror(errno));
        return -1;
    }

    return sockfd;
}


void *accept_thread(void *args) {

    /* Set up socket */

    char port[6] = {'\0'};
    sprintf(port, "%d", g_client_info->port);
    int sockfd = initialize_socket(port);

    if (sockfd == -1) {
        error_log("Invalid socket");
    }

    struct sockaddr_in new_addr;
    socklen_t addrlen = sizeof(new_addr);
    int newfd;

    /* Continuously accept new connections */

    while (sockfd != -1) {
        // info_log("Receiving on socket: %d, waiting for accept", sockfd);
        newfd = accept(sockfd, (struct sockaddr *)&new_addr, &addrlen);
        /* Create the peer */
        if (peer_count() >= PEER_LIMIT) {
            info_log("Not adding this connection - we already have %u peers", PEER_LIMIT);
        } else {
            struct peer *entry = peer_create();

            if (entry != NULL) {

                /* Add peer fields */

                entry->sockfd = newfd;
                entry->addr = new_addr;
                //update_recv_timestamp(entry);

                /* Add the peer to hashtable */

                if (peer_add(entry)) {

                    /* Add peer to thread */

                    // info_log("Connection Accepted");

                    struct peer_thread_options *opts = malloc(sizeof(struct peer_thread_options));
                    opts->self = entry;
                    thread_start(peer_thread, opts);
                }
            }
        }
    }
    return args;
}

void begin_accept() {
    pthread_t accept_t;
    pthread_create(&accept_t, NULL, accept_thread, NULL);
}