#include "receive.h"

#include <poll.h>
#include <stdio.h>
#include <stdbool.h>
#include <netdb.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include "peer.h"
#include "common.h"
#include "netmsg_packer.h"
#include "log.h"
#include "util.h"
#include "metafile_parse.h"
#include "client.h"
#include "send.h"

bool validate_handshake(uint8_t *message, uint32_t message_len, char **pstr, uint8_t **peer_id) {
    uint8_t *info_hash;

    /* Unpack */

    unpack_handshake(message, message_len, pstr, &info_hash, peer_id);

    /* Check valid inputs */

    if (info_hash == NULL || memcmp(info_hash, g_metafile->info.hash, SHA1_SIZE) != 0) {
        error_log("Peer doesn't have the correct info hash, disconnecting it");

        /* Clean up */

        if(info_hash != NULL) {
            free(info_hash);
        }
        if(pstr != NULL) {
            free(*pstr);
        }
        if(peer_id != NULL) {
            free(*peer_id);    
        }
        return false;
    }
    free(info_hash);
    return true;
}

void update_recv_timestamp(struct peer *peer) {
    clock_gettime(CLOCK_REALTIME, &(peer->last_recv));
}

void send_handshake(int sockfd) {
    uint8_t *packet, *bitfield;
    size_t packetlen;
    pack_handshake(g_metafile->info.hash, g_client_info->peer_id, &packet, &packetlen);
    int ret = send(sockfd, packet, packetlen, MSG_NOSIGNAL );

    if(ret < 1){
        warn_log("Send failed");
        peer_disconnect(peer_find(sockfd));
    }

    free(packet);

    pieceset_generate_bitfield(&bitfield);
    pack_bitfield(bitfield, piece_bitfield_size(), &packet, &packetlen);
    free(bitfield);

    ret = send(sockfd, packet, packetlen, MSG_NOSIGNAL );

    if(ret < 1){
        warn_log("Send failed");
        peer_disconnect(peer_find(sockfd));
    }

    free(packet);
}

static void tbl_rdlock() {
    verbose_log("Tbl rd lock %d", pthread_self());
    pthread_rwlock_rdlock(&g_peers->tbl_lock);
}

static void tbl_wrlock() {
    verbose_log("Tbl wr lock %d", pthread_self());
    pthread_rwlock_wrlock(&g_peers->tbl_lock);
}

static void tbl_unlock() {
    verbose_log("Tbl unlock %d", pthread_self());
    pthread_rwlock_unlock(&g_peers->tbl_lock);
}

void *timestamp_receive_thread(void *args) {
    struct peer *entry, *tmp;
    struct timespec tv;

    trace_log("Thread %d", pthread_self());
    /* Continuously check for elapsed timestamps */

    while(1) {
        tbl_rdlock();
        HASH_ITER(hh, g_peers->head, entry, tmp) {

            /* Check times */
            
            peer_rdlock(entry);
            clock_gettime(CLOCK_REALTIME, &tv);
            uint64_t elapsed_sec = timespec_elapsed_sec(&(entry->last_recv), &tv);

            if(elapsed_sec >= 120) {

                /* Unlock and get a write lock */

                peer_unlock(entry);
                peer_wrlock(entry);

                /* Kill him */
                tbl_unlock();
                peer_disconnect(entry);
                tbl_rdlock();

            }
            peer_unlock(entry);
        }

        /* Sleep for not as long - we want to have faster operation here */

        tbl_unlock();
        sleep(5);

    }
}

int initialize_socket(char *port) {
    struct addrinfo hints, *res;

    memset(&hints, 0, sizeof(hints)); 
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if(getaddrinfo(NULL, port, &hints, &res) != 0) {
        error_log("Error with getaddrinfo");
        return -1;
    }

    int sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if(sockfd == -1) {
        error_log("Error with socket\n");
        return -1;
    }

    if(bind(sockfd, res->ai_addr, res->ai_addrlen) == -1) {
        error_log("Error with bind\n");
        return -1;
    }
    
    if(listen(sockfd, 100) == -1) {
        error_log("Error with listen: %s\n", strerror(errno));
        return -1;
    }

    return sockfd;
}

void *recycle_data_received(void *args) {
    struct peer *entry, *tmp;

    trace_log("Thread %d", pthread_self());

    while(1) {
        tbl_wrlock();

        /* Iterate through hashtable to zero out info */

        HASH_ITER(hh, g_peers->head, entry, tmp) {
            
            /* Lock peer */

            peer_wrlock(entry);
            
            /* Some processing here to get top 4/propshare - Chris? */

            entry->bytes_received = 0;

            peer_unlock(entry);

        }
        tbl_unlock();
        sleep(10);
    }
}

void *recv_poll(void *args) {

    trace_log("Thread %d", pthread_self());

    /* Set up first connection, our node itself */
    char port[6] = {'\0'};
    sprintf(port, "%d", g_client_info->port);
    debug_log("Creating listener");
    int sockfd = initialize_socket(port);
    g_peers->recv_poll_fds[0].fd = sockfd;
    g_peers->recv_poll_fds[0].events = POLLIN;
    int nfds = 1 + peer_count();

    /* Continuously poll */

    while (true) {
        if (poll(g_peers->recv_poll_fds, nfds, -1) > 0) { //continuously poll
            /* Check for new connections */

            if (g_peers->recv_poll_fds[0].revents & POLLIN) {

                /* Do something with new connections */

                /* Accept the connection */

                struct sockaddr client;
                socklen_t socklen = sizeof(client);
                int newfd = accept(g_peers->recv_poll_fds[0].fd, &client, &socklen);

                /* Possible Failure */

                if (newfd < 0) {
                    warn_log("Connection refused");
                }

                /* Set up new client */ /* TODO */

                else {

                    /* Create the peer */

                    struct peer *entry = peer_create();

                    /* Add peer fields */

                    entry->sockfd = newfd;
                    update_recv_timestamp(entry);

                    /* Add the peer to hashtable */

                    peer_add(entry);

                    /* Add peer to poll */

                    g_peers->recv_poll_fds[nfds].fd = newfd;
                    g_peers->recv_poll_fds[nfds].events = POLLIN;
                    nfds += 1;
                    info_log("Connection Accepted");
                }

            }

            /* Not starting at 0, we've dealt with that above. */

            for (int i = 1; i < nfds; i++){

                /* Acquire a read lock */

                struct peer *entry = peer_find(g_peers->recv_poll_fds[i].fd);

                if(!entry){
                    error_log("Entry is NULL, WTF?");
                    continue;
                }
                peer_wrlock(entry);

                /* Normal peer sending data */

                if (g_peers->recv_poll_fds[i].revents & POLLIN && g_peers->recv_poll_fds[i].fd != -1) {

                    if(entry->len_bytes_recv == sizeof(entry->recv_len)){
                        debug_log("Reseting recv buf");
                        entry->recv_len = 0;
                        entry->len_bytes_recv = 0;
                    }

                    /* Note that there is a check if fd is -1 */

                    /* Get packet length */
                    if(entry->len_bytes_recv != sizeof(entry->recv_len)){
                        if(entry->recv_handshake == true){
                            ssize_t amount = recv(g_peers->recv_poll_fds[i].fd, ((uint8_t *)(&entry->recv_len))+entry->len_bytes_recv, sizeof(entry->recv_len) - entry->len_bytes_recv, MSG_DONTWAIT);
                            update_recv_timestamp(entry);
    
                            debug_log("Receiving len");
                            if(amount == -1 || amount == 0){
                                warn_log("Recv error %d", amount);
                                peer_unlock(entry);
                                peer_disconnect(entry);
                                continue;
                            }
    
                            entry->len_bytes_recv += amount;
    
                            debug_log("len recvd %d", entry->len_bytes_recv);
                            if (entry->len_bytes_recv == sizeof(entry->recv_len)) {
                                entry->recv_len = ntohl(entry->recv_len);
                                debug_log("Expecting %d", entry->recv_len);
                                entry->recv_buf = malloc(entry->recv_len);
                                entry->buf_bytes_recv = 0;
                            }
                        } else {
                            entry->recv_len = 0;
                            ssize_t amount = recv(g_peers->recv_poll_fds[i].fd, ((uint8_t *)(&entry->recv_len)), 1, MSG_DONTWAIT);
                            update_recv_timestamp(entry);
    
                            debug_log("Receiving handshake len");
                            if(amount == -1 || amount == 0){
                                warn_log("Recv error %d", amount);
                                peer_unlock(entry);
                                peer_disconnect(entry);
                                continue;
                            }
    
                            entry->len_bytes_recv = 4;
    
                            debug_log("len recvd %d", entry->len_bytes_recv);
                            if (entry->len_bytes_recv == sizeof(entry->recv_len)) {
                                entry->recv_len += 48;
                                debug_log("Expecting %d", entry->recv_len);
                                entry->recv_buf = malloc(entry->recv_len);
                                entry->buf_bytes_recv = 0;
                            }
                        }
                    }

                    if(entry->len_bytes_recv == sizeof(entry->recv_len)){
                        /* Get packet itself */
                        ssize_t amount = recv(g_peers->recv_poll_fds[i].fd, entry->recv_buf + entry->buf_bytes_recv, entry->recv_len - entry->buf_bytes_recv, MSG_DONTWAIT);

                        /* Check packet length */

                        if(amount == -1 || amount == 0){
                            warn_log("Recv error %d", amount);
                            free(entry->recv_buf);
                            peer_unlock(entry);
                            peer_disconnect(entry);
                            continue;
                        }

                        entry->bytes_received += amount;
                        entry->buf_bytes_recv += amount;

                        debug_log("recvd %d", entry->buf_bytes_recv);

                        if(entry->buf_bytes_recv == entry->recv_len){

                            trace_buffer_dump(entry->recv_buf, entry->recv_len, "Received %d", entry->recv_len);
                                    /* Have they sent us a handshake yet? */

                            if(entry->recv_handshake) {
                                
                                /* Write correct information */

                                g_peers->recv_poll_fds[i].events = 0;

                                peer_unlock(entry);
                            }
                            else {
                                /* Get fields ready, we're assuming that this is an incoming handshake */

                                char *pstr;
                                uint8_t *peer_id;

                                /* Check if handshake is valid */

                                if(validate_handshake(entry->recv_buf, entry->recv_len, &pstr, &peer_id)) {
                                    
                                    strcpy(entry->pstr, pstr);
                                    memcpy(entry->peer_id, peer_id, 20);
                                    entry->recv_handshake = true;

                                    /* Unlock and clean up */

                                    peer_unlock(entry);
                                    free(pstr);
                                    free(peer_id);

                                    /* Once validated, send it! */

                                    send_handshake(entry->sockfd);
                                    update_send_timestamp(entry);

                                    /* Finally, update the field */

                                    peer_wrlock(entry);
                                    entry->sent_handshake = true;
                                    peer_unlock(entry);
                                }
                                else {
                                    /* They haven't sent us a handshake yet, so bye bye */
                                    peer_unlock(entry);
                                    peer_disconnect(entry);
                                }

                            }
                        } else {
                            peer_unlock(entry);
                            continue;
                        }

                
                    }

                    trace_log("Next");

                }
            }

        }
        /* Continuously update number of fds */
        nfds = 1 + peer_count();
    }
}