#include <stdlib.h>
#include <errno.h>

#include "lockless_linklist.h"
#include "log.h"


/**
 * In this linked list, the head always points to a dummy node.
 * After a node's data has been read it gets moved to the head,
 * and becomes the new dummy node
 */

struct link_list *linklist_create() {
    struct link_list *tmp = malloc(sizeof(struct link_list));
    if (!tmp) {
        mem_errlog();
        return NULL;
    }

    struct link_list_node *dummy = calloc(sizeof(struct link_list_node), 1);
    if (!dummy) {
        mem_errlog();
        free(tmp);
        return NULL;
    }

    if (sem_init(&tmp->list_sem, 0, 0) != 0) {
        error_log("failed to create semaphore");
        free(tmp);
        free(dummy);
        return NULL;
    }

    tmp->head = dummy;
    tmp->tail = dummy;

    return tmp;
}

void linklist_destroy(struct link_list *link_list, LinkListDataCleanup cleanup) {
    struct link_list_node *cur = link_list->head;
    struct link_list_node *next;

    sem_destroy(&link_list->list_sem);

    while (cur != link_list->tail) {
        next = cur->next;

        if (cleanup) {
            cleanup(cur->data);
        }
        free(cur);
        cur = next;
    }

    if (cleanup) {
        cleanup(link_list->tail->data);
    }

    free(link_list->tail);
    free(link_list);
}

bool linklist_append(struct link_list *link_list, void *data, size_t data_len) {
    struct link_list_node *new = calloc(sizeof(struct link_list_node), 1);
    if (!new) {
        mem_errlog();
        return false;
    }

    if (sem_post(&link_list->list_sem) != 0) {
        warn_log("failed to increment semaphore");
        free(new);
        return false;
    }

    new->data = data;
    new->data_len = data_len;

    link_list->tail->next = new;
    link_list->tail = new;

    return true;
}

bool linklist_tryread(struct link_list *link_list, uint8_t **data, size_t *data_len) {
    if (sem_trywait(&link_list->list_sem) == 0) {
        struct link_list_node *next;
        next = link_list->head->next;

        /* Read the data */
        void *data_out = next->data;
        size_t data_len_out = next->data_len;

        /* Assign the new head, and free the old one */
        free(link_list->head);
        link_list->head = next;
        *data = data_out;

        if (data_len) {
            *data_len = data_len_out;
        }

        return true;
    }

    if (errno != EAGAIN) {
        warn_log("failed to decrement semaphore");
    }

    return false;
}

bool linklist_read(struct link_list *link_list, uint8_t **data, size_t *data_len) {
    if (sem_wait(&link_list->list_sem) == 0) {
        struct link_list_node *next;
        next = link_list->head->next;

        /* Read the data */
        void *data_out = next->data;
        size_t data_len_out = next->data_len;

        /* Assign the new head, and free the old one */
        free(link_list->head);
        link_list->head = next;
        *data = data_out;

        if (data_len) {
            *data_len = data_len_out;
        }

        return true;
    }

    warn_log("failed to decrement semaphore");
    return false;
}

size_t linklist_size(struct link_list *link_list) {
    int sem_value;
    sem_getvalue(&link_list->list_sem, &sem_value);
    return sem_value;
}