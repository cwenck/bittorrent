#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>

#include "client.h"
#include "common.h"
#include "log.h"
#include "metafile_parse.h"
#include "tracker.h"
#include "piece_set.h"
#include "thread_registry.h"
#include "send.h"
#include "receive.h"

#define NUM_TOP_PEERS 3

void standard();
void propshare();

uint32_t *new_pieces = NULL;
uint32_t pieces_count = 0;

//static bool last_bitma

void *decide(void *data){
	UNUSED(data);

    if(use_propshare){
        info_log("Running with propshare");
        propshare();
    }
    else {
        info_log("Running with standard bittorrent");
        standard();
    }
    return NULL;
}

void propshare(){
    struct peer *entry, *tmp;
    static bool *last_piece_map = NULL;
    while(1) {

        uint64_t total = 0, total_upload = 0, total_interested = 0, interested_count = 0;

        pthread_rwlock_rdlock(&g_peers->tbl_lock);

        /* Iterate to find all the top bytes recvd */
        HASH_ITER(hh, g_peers->head, entry, tmp) {
            ssize_t bytes_recvd = 0;

            pthread_rwlock_wrlock(&entry->lock);

            bytes_recvd = entry->bytes_received_last;
            bytes_recvd += entry->bytes_received;
            entry->bytes_received_last = entry->bytes_received;
            entry->bytes_received = 0;
            total += entry->bytes_received_last;

            //info_log("Received %d bytes from %s", entry->bytes_received_last, inet_ntoa(entry->addr.sin_addr));
            total_upload += entry->bytes_sent;
            entry->bytes_sent = 0;

            pthread_rwlock_unlock(&entry->lock);

            if(entry->peer_interested) {
                total_interested += entry->bytes_received_last;
                interested_count++;

            }

        }
        info_log("Received %d bytes in 10 s - " TXT_FMT_BOLD TXT_COLOR_CYAN "%.1fMB/s" TXT_ALL_RESET, total, (double) (total / 10) / 1048576.0);
        info_log("Sent %d bytes in 10 s - " TXT_FMT_BOLD TXT_COLOR_GREEN "%.1fMB/s" TXT_ALL_RESET, total_upload, (double) (total_upload / 10) / 1048576.0);


        pthread_rwlock_unlock(&g_peers->tbl_lock);

        // printf("Top 3:\n");
        // for(int j = 0; j < NUM_TOP_PEERS; j++) {
        //     if(highest_peers[j] != 0) {
        //         printf("Peer %d\n", highest_peers[j]);
        //     }
        // }

        /* Set the flags */

        bool *piece_map;

        pieceset_generate_piecemap(&piece_map);

        int t = 0;
        for(unsigned int i = 0; i < piece_count() && last_piece_map; i++){
            if(!last_piece_map[i] && piece_map[i]){
                t++;
            }
        }

        pieces_count = t;
        new_pieces = calloc(sizeof(uint32_t), t);

        int j = 0;
        for(unsigned int i = 0; i < piece_count() && last_piece_map; i++){
            if(!last_piece_map[i] && piece_map[i]){
                new_pieces[j] = i;
                j++;
            }
        }

        free(last_piece_map);
        last_piece_map = piece_map;

        pthread_rwlock_rdlock(&g_peers->tbl_lock);

        HASH_ITER(hh, g_peers->head, entry, tmp) {
            int ret = 0;
            uint64_t val = 0;
            bool done = false;

            if(entry->peer_interested && total_interested){
                entry->bytes_sent_limit = ((upload_limit) * (entry->bytes_received_last << 13)) / total_interested;

                val = 0x8;
                /* If it got here, it should be choked. */
                ret = write(entry->notify, &val, 8);

                if(ret == -1){
                    error_log("Issue with eventfd %d", entry->sockfd);
                }
            } else if(entry->peer_interested && !total_interested) {
                entry->bytes_sent_limit = (upload_limit<< 13) / interested_count;

                val = 0x8;
                /* If it got here, it should be choked. */
                ret = write(entry->notify, &val, 8);

                if(ret == -1){
                    error_log("Issue with eventfd %d", entry->sockfd);
                }
            }
            else if(!done){
                val = 0x4;
                /* If it got here, it should be choked. */
                ret = write(entry->notify, &val, 8);

                if(ret == -1){
                    error_log("Issue with eventfd %d", entry->sockfd);
                }
            }
        }

        pthread_rwlock_unlock(&g_peers->tbl_lock);

        sleep(10);
    }
}

void standard(){
    struct peer *entry, *tmp, *optimistic = NULL;
    /* Counter checks if it's time for a new optimistic unchoke */
    static unsigned int counter = 0, rand_seed = 12345;
    static bool *last_piece_map = NULL;


    /* Continuously check for elapsed timestamps */

    while(1) {

        struct peer *interested_peers[PEER_LIMIT] = {0};
        int highest_peers[NUM_TOP_PEERS] = {0};
        uint64_t total = 0, total_upload = 0, highest_bytes[NUM_TOP_PEERS] = {0}, interested_total = 0;

        pthread_rwlock_rdlock(&g_peers->tbl_lock);

        /* Iterate to find all the top bytes recvd */
        HASH_ITER(hh, g_peers->head, entry, tmp) {
            ssize_t bytes_recvd = 0;

            pthread_rwlock_wrlock(&entry->lock);

            bytes_recvd = entry->bytes_received_last;
            bytes_recvd += entry->bytes_received;
            entry->bytes_received_last = entry->bytes_received;
            entry->bytes_received = 0;
            total += entry->bytes_received_last;

            entry->bytes_sent_limit = (upload_limit) << 11;

            //info_log("Received %d bytes from %s", entry->bytes_received_last, inet_ntoa(entry->addr.sin_addr));
            total_upload += entry->bytes_sent;
            entry->bytes_sent = 0;

            pthread_rwlock_unlock(&entry->lock);

            if(entry->peer_interested) {

                    /* Add to the interested table */
                    interested_peers[interested_total] = entry;
                    interested_total++;

                    for(int j = 0; j < NUM_TOP_PEERS; j++) {

                        /* The peer must be interested in us */

                        if(bytes_recvd >= highest_bytes[j]) {


                            /* One of the top 3 (although may be overwritten later) */

                            for(int k = j; k < NUM_TOP_PEERS-1; k++){
                                highest_bytes[k+1] = highest_bytes[k];
                                highest_peers[k+1] = highest_peers[k];
                            }

                            highest_peers[j] = entry->sockfd;
                            highest_bytes[j] = bytes_recvd;

                            break;
                        }
                    }


                }
                else {

		    /* This peer is not interested in us */

                }

            /* Doing this outside the lock because it'll be faster */



            // i = pieceset_has_new(entry->piece_map);

            // if(i && !entry->am_interested && !pieceset_done()){
            //  val |= 0x2;
            // } else if(!i && entry->am_interested){
            //  val |= 0x4;
            // }

            /* Old code to set it to 0x8, choke */

            // ret = write(entry->notify, &val, 8);

            // if(ret == -1){
            //     error_log("Issue with eventfd");
            // }
        }
        info_log("Received %d bytes in 10 s - " TXT_FMT_BOLD TXT_COLOR_CYAN "%.1fMB/s" TXT_ALL_RESET, total, (double) (total / 10) / 1048576.0);
        info_log("Sent %d bytes in 10 s - " TXT_FMT_BOLD TXT_COLOR_GREEN "%.1fMB/s" TXT_ALL_RESET, total_upload, (double) (total_upload / 10) / 1048576.0);

        /* Find the Optimistic Unchoke */

        int optimistic_index = -1;
        int optimistic_fd = 0;

        /* If there are no interested peers, or it's not time to do this, we'll skip this */

        if(counter == 0) {
            if(interested_total > 3){

                optimistic_index = rand_r(&rand_seed) % interested_total;
                while(interested_total > 3 && optimistic == NULL) {
                    bool in_top = false;
                    for(int j = 0; j < NUM_TOP_PEERS; j++) {

                        /* Make sure it isn't in the top 3 */

                        if(interested_peers[optimistic_index]->sockfd == highest_peers[j]) {
                            in_top = true;
                        }

                    }

                    if(!in_top){
                        optimistic = interested_peers[optimistic_index];
                    }


                }
            } else {
                optimistic = NULL;
            }

            if(optimistic) {


                // printf("Optimistic unchoke: \n");
                // printf("Peer %d\n", interested_peers[optimistic_index]->sockfd);
                optimistic_fd = interested_peers[optimistic_index]->sockfd;
            }
        } else {

        }

        pthread_rwlock_unlock(&g_peers->tbl_lock);

        // printf("Top 3:\n");
        // for(int j = 0; j < NUM_TOP_PEERS; j++) {
        //     if(highest_peers[j] != 0) {
        //         printf("Peer %d\n", highest_peers[j]);
        //     }
        // }

        /* Set the flags */

        bool *piece_map;

        pieceset_generate_piecemap(&piece_map);

        int t = 0;
        for(unsigned int i = 0; i < piece_count() && last_piece_map; i++){
            if(!last_piece_map[i] && piece_map[i]){
                t++;
            }
        }

        pieces_count = t;
        new_pieces = calloc(sizeof(uint32_t), t);

        int j = 0;
        for(unsigned int i = 0; i < piece_count() && last_piece_map; i++){
            if(!last_piece_map[i] && piece_map[i]){
                new_pieces[j] = i;
                j++;
            }
        }

        free(last_piece_map);
        last_piece_map = piece_map;

        pthread_rwlock_rdlock(&g_peers->tbl_lock);

        HASH_ITER(hh, g_peers->head, entry, tmp) {
            int ret = 0;
            uint64_t val = 0;
            bool done = false;

            for(int j = 0; j < NUM_TOP_PEERS; j++) {

                if(highest_peers[j] != 0 && entry->sockfd == highest_peers[j]) {
                    val = 0x8;
                    /* Found the top 3 */
                    ret = write(entry->notify, &val, 8);
                    if(ret == -1){
                        error_log("Issue with eventfd %d", entry->sockfd);
                    }

                    done = true;
                    break;
                }
            }
            if(!done && entry->sockfd == optimistic_fd) {
                /* Found the optimistic unchoke */
                val = 0x8;
                ret = write(entry->notify, &val, 8);
                if(ret == -1){
                    error_log("Issue with eventfd %d", entry->sockfd);
                }

                done = true;
            }
            else if(!done){
                val = 0x4;
                /* If it got here, it should be choked. */
                ret = write(entry->notify, &val, 8);

                if(ret == -1){
                    error_log("Issue with eventfd %d", entry->sockfd);
                }
            }
        }



        // HASH_ITER(hh, g_peers->head, entry, tmp) {
        //     if(entry->peer_interested){
        //         interested_total++;
        //         for(int j = 0; j < 3; j++){
        //             if(totals[j] <= entry->bytes_received + entry->bytes_received_last ){
        //                 for(int k = j; k < 2; k++){

        //                     peers[k+1] = peers[k];
        //                     totals[k+1] = totals[k];
        //                 }

        //                 peers[j] = entry;
        //                 totals[j] = entry->bytes_received + entry->bytes_received_last;
        //                 break;
        //             }
        //         }
        //     }
        // }

        counter = (counter + 1) % 3;
        pthread_rwlock_unlock(&g_peers->tbl_lock);

        sleep(10);
    }
}
