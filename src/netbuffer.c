#include <arpa/inet.h>
#include <endian.h>
#include <string.h>
#include "netbuffer.h"


uint64_t htonll(uint64_t val) {
    return htobe64(val);
}

uint64_t ntohll(uint64_t val) {
    return be64toh(val);
}

void create_network_buffer(struct netbuf *net_buf, void *in_buf, size_t len) {
    net_buf->buf = (uint8_t *) in_buf;
    net_buf->len = len;
    net_buf->position = 0;
}

bool buffer_seek_offset(struct netbuf *net_buf, size_t offset) {
    if (net_buf->position + offset <= net_buf->len) {
        net_buf->position += offset;
        return true;
    }
    return false;
}

bool buffer_seek(struct netbuf *net_buf, size_t position) {
    if (position <= net_buf->len) {
        net_buf->position = position;
        return true;
    }
    return false;
}

bool end_of_buffer(struct netbuf *net_buf) {
    return (net_buf->position >= net_buf->len);
}

bool push_uint8(struct netbuf *net_buf, uint8_t val) {
    if (net_buf->position + 1 <= net_buf->len) {
        memcpy(net_buf->buf + net_buf->position, &val, 1);
        net_buf->position += 1;
        return true;
    }
    return false;
}

bool push_uint16(struct netbuf *net_buf, uint16_t val) {
    if (net_buf->position + 2 <= net_buf->len) {
        uint16_t nbyte_order = htons(val);
        memcpy(net_buf->buf + net_buf->position, &nbyte_order, 2);
        net_buf->position += 2;
        return true;
    }
    return false;
}

bool push_uint32(struct netbuf *net_buf, uint32_t val) {
    if (net_buf->position + 4 <= net_buf->len) {
        uint32_t nbyte_order = htonl(val);
        memcpy(net_buf->buf + net_buf->position, &nbyte_order, 4);
        net_buf->position += 4;
        return true;
    }
    return false;
}

bool push_uint64(struct netbuf *net_buf, uint64_t val) {
    if (net_buf->position + 8 <= net_buf->len) {
        uint64_t nbyte_order = htonll(val);
        memcpy(net_buf->buf + net_buf->position, &nbyte_order, 8);
        net_buf->position += 8;
        return true;
    }
    return false;
}

bool push_generic_uint(struct netbuf *net_buf, uint64_t val, size_t uint_bytes) {
    if (uint_bytes > 8) {
        return false;
    }

    if (net_buf->position + uint_bytes <= net_buf->len) {
        uint64_t nbyte_order = htonll(val);
        memcpy(net_buf->buf + net_buf->position, (((uint8_t *)(&nbyte_order)) + (8 - uint_bytes)), uint_bytes);
        net_buf->position += uint_bytes;
        return true;
    }
    return false;
}

bool push_data(struct netbuf *net_buf, const void *data, size_t data_len) {
    if (data && net_buf->position + data_len <= net_buf->len) {
        memcpy(net_buf->buf + net_buf->position, data, data_len);
        net_buf->position += data_len;
        return true;
    }
    return false;
}

bool read_uint8(struct netbuf *net_buf, uint8_t *out) {
    if (net_buf->position + 1 <= net_buf->len) {
        memcpy(out, net_buf->buf + net_buf->position, 1);
        net_buf->position += 1;
    }
    return false;
}

bool read_uint16(struct netbuf *net_buf, uint16_t *out) {
    if (net_buf->position + 2 <= net_buf->len) {
        uint16_t nbyte_order;
        memcpy(&nbyte_order, net_buf->buf + net_buf->position, 2);
        *out = ntohs(nbyte_order);
        net_buf->position += 2;
    }
    return false;
}

bool read_uint32(struct netbuf *net_buf, uint32_t *out) {
    if (net_buf->position + 4 <= net_buf->len) {
        uint32_t nbyte_order;
        memcpy(&nbyte_order, net_buf->buf + net_buf->position, 4);
        *out = ntohl(nbyte_order);
        net_buf->position += 4;
    }
    return false;
}

bool read_uint64(struct netbuf *net_buf, uint64_t *out) {
    if (net_buf->position + 8 <= net_buf->len) {
        uint64_t nbyte_order;
        memcpy(&nbyte_order, net_buf->buf + net_buf->position, 8);
        *out = ntohll(nbyte_order);
        net_buf->position += 8;
    }
    return false;
}

bool read_generic_uint(struct netbuf *net_buf, uint64_t *out, size_t uint_bytes) {
    if (net_buf->position + uint_bytes <= net_buf->len) {
        uint64_t nbyte_order = 0;
        memcpy(&nbyte_order, net_buf->buf + net_buf->position, uint_bytes);
        nbyte_order <<= 64 - (8 * uint_bytes);
        *out = ntohll(nbyte_order);
        net_buf->position += uint_bytes;
    }
    return false;
}

bool read_data(struct netbuf *net_buf, void *data_out, size_t data_len) {
    if (net_buf->position + data_len <= net_buf->len) {
        memcpy(data_out, net_buf->buf + net_buf->position, data_len);
        net_buf->position += data_len;
        return true;
    }
    return false;
}

bool read_string_data(struct netbuf *net_buf, void *string_out, size_t raw_string_len) {
    bool ret = read_data(net_buf, string_out, raw_string_len);
    ((char *) string_out)[raw_string_len] = '\0'; /* Add the null terminator to the raw string */
    return ret;
}