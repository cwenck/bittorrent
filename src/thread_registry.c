#include "thread_registry.h"

#include <pthread.h>

#include "log.h"

static pthread_t thread_ids[MAX_THREADS] = {0};
static bool tid_filled[MAX_THREADS] = {0};

static pthread_rwlock_t lock;

bool thread_start(void *(*start_routine)(void *), void *arg) {
    pthread_rwlock_rdlock(&lock);
    for (int i = 0; i < MAX_THREADS; i++) {
        if (!tid_filled[i]) {
            // info_log("Creating new thread");
            pthread_rwlock_unlock(&lock);
            pthread_rwlock_wrlock(&lock);
            pthread_create(&thread_ids[i], NULL, start_routine, arg);
            tid_filled[i] = true;
            // info_log("Thread started with tid %lu", thread_ids[i]);
            pthread_rwlock_unlock(&lock);
            return true;
        }
    }
    pthread_rwlock_unlock(&lock);
    warn_log("Thread NOT started, thread limit exceeded");
    return false;
}

void thread_finished() {
    pthread_rwlock_rdlock(&lock);
    pthread_t tid = pthread_self();
    for (int i = 0; i < MAX_THREADS; i++) {
        if (tid_filled[i] && pthread_equal(thread_ids[i], tid)) {
            pthread_rwlock_unlock(&lock);
            pthread_rwlock_wrlock(&lock);
            tid_filled[i] = false;
            pthread_rwlock_unlock(&lock);
            return;
        }
    }
    pthread_rwlock_unlock(&lock);
}

bool thread_setup() {
    return (pthread_rwlock_init(&lock, NULL) == 0);
}

void thread_cleanup() {
    pthread_rwlock_destroy(&lock);
}