#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "common.h"
#include "heapless-bencode/bencode.h"
#include "metafile_parse.h"
#include "hash.h"
#include "log.h"

#define KEYS_LEN 7

bool parse_dict(void *data,  bencode_t *dict, struct dict_pair_parse parsers[], int size);

bool parse_failure(void *data, bencode_t *value);
bool parse_warning(void *data, bencode_t *value);
bool parse_interval(void *data, bencode_t *value);
bool parse_min_interval(void *data, bencode_t *value);
bool parse_tracker_id(void *data, bencode_t *value);
bool parse_complete(void *data, bencode_t *value);
bool parse_incomplete(void *data, bencode_t *value);
bool parse_peers(void *data, bencode_t *value);

bool parse_peer_id(void *data, bencode_t *value);
bool parse_ip(void *data, bencode_t *value);
bool parse_port(void *data, bencode_t *value);

bool parse_info(void *data, bencode_t *value);
bool parse_announce(void *data, bencode_t *value);
bool parse_announce_list(void *data, bencode_t *value);
bool parse_created(void *data, bencode_t *value);
bool parse_comment(void *data, bencode_t *value);
bool parse_creator(void *data, bencode_t *value);
bool parse_encoding(void *data, bencode_t *value);

bool parse_piece_length(void *data, bencode_t *value);
bool parse_pieces(void *data, bencode_t *value);
bool parse_private(void *data, bencode_t *value);
bool parse_name(void *data, bencode_t *value);
bool parse_info_length(void *data, bencode_t *value);
bool parse_info_md5sum(void *data, bencode_t *value);
bool parse_files(void *data, bencode_t *value);

bool parse_files_length(void *data, bencode_t *value);
bool parse_files_md5sum(void *data, bencode_t *value);
bool parse_files_path(void *data, bencode_t *value);

bool process_metafile_dict(struct metafile *, const char *, int, bencode_t *);
bool process_key(char *keys[], const char *key, int len);
char *copy_string(bencode_t *);

void to_hex(void *o, void *a, int len_in);
char *print_hex(void *a, int len_in);

void print_info(struct mf_info *info, int print_hashes);

static struct dict_pair_parse meta_parse[] = {
	{"info", parse_info}, 
	{"announce", parse_announce}, 
	{"announce-list", parse_announce_list}, 
	{"creation date", parse_created}, 
	{"comment", parse_comment}, 
	{"created by", parse_creator}, 
	{"encoding", parse_encoding}
};

static struct dict_pair_parse info_parse[] = {
	{"piece length", parse_piece_length},
	{"pieces", parse_pieces},
	{"private", parse_private},
	{"name", parse_name},
	{"length", parse_info_length},
	{"md5sum", parse_info_md5sum},
	{"files", parse_files}
};

static struct dict_pair_parse files_parse[] = {
	{"length", parse_files_length},
	{"md5sum", parse_files_md5sum},
	{"path", parse_files_path}
};

static struct dict_pair_parse tracker_parse[] = {
	{"failure reason", parse_failure},
	{"warning message", parse_warning},
	{"interval", parse_interval},
	{"min interval", parse_min_interval},
	{"tracker id", parse_tracker_id},
	{"complete", parse_complete},
	{"incomplete", parse_incomplete},
	{"peers", parse_peers}
};

static struct dict_pair_parse peer_parse[] = {
	{"peer id", parse_peer_id},
	{"ip", parse_ip},
	{"port", parse_port}
};

struct tracker_resp *parse_tracker_resp(void *buf, int len){
	bencode_t tracker_resp;
	struct tracker_resp *out = calloc(sizeof(struct tracker_resp), 1);
	int ret;

	bencode_init(&tracker_resp, buf, len);

	if(!bencode_is_dict(&tracker_resp)){
		error_log("Failure. Input is not a bencode dictionary. Expected dictionary. %s", buf);
		free(out);
		return NULL;
	}

	ret = parse_dict(out, &tracker_resp, tracker_parse, 8);

	if(!ret){
		return false;
	}

	return out;
}

bool parse_failure(void *data, bencode_t *value){
	struct tracker_resp *resp = data;

	resp->failure = copy_string(value);
	if(!resp->failure){
		error_log("Failure. Issue with copying failure string.");
		return false;
	}
	return true;
}

bool parse_warning(void *data, bencode_t *value){
	struct tracker_resp *resp = data;

	resp->warning = copy_string(value);
	if(!resp->warning){
		error_log("Failure. Issue with copying warning string.");
		return false;
	}
	return true;
}

bool parse_interval(void *data, bencode_t *value){
	struct tracker_resp *resp = data;

	if(!bencode_is_int(value)){
		error_log("Failure. Expected int for interval");
		return false;
	}

	bencode_int_value(value, &resp->interval);
	return true;
}

bool parse_min_interval(void *data, bencode_t *value){
	struct tracker_resp *resp = data;

	if(!bencode_is_int(value)){
		error_log("Failure. Expected int for minimum interval");
		return false;
	}

	bencode_int_value(value, &resp->min_interval);
	return true;
}

bool parse_tracker_id(void *data, bencode_t *value){
	struct tracker_resp *resp = data;

	resp->tracker_id = copy_string(value);
	if(!resp->tracker_id){
		error_log("Failure. Issue with copying tracker id string.");
		return false;
	}
	return true;
}

bool parse_complete(void *data, bencode_t *value){
	struct tracker_resp *resp = data;

	if(!bencode_is_int(value)){
		error_log("Failure. Expected int for complete");
		return false;
	}

	bencode_int_value(value, &resp->complete);
	return true;
}

bool parse_incomplete(void *data, bencode_t *value){
	struct tracker_resp *resp = data;

	if(!bencode_is_int(value)){
		error_log("Failure. Expected int for incomplete");
		return false;
	}

	bencode_int_value(value, &resp->incomplete);
	return true;
}

bool parse_peers(void *data, bencode_t *value){
	struct tracker_resp *resp = data;

	if(bencode_is_string(value)){
		const uint8_t *str;
		int len, i;

		bencode_string_value(value, (const char **) &str, &len);

		if(len %6){
			error_log("Failure. Expected len to be a multiple of 6");
			return false;
		}

		resp->peer_count = len / 6;

		resp->peers = calloc(sizeof(struct tracker_peer), resp->peer_count);

		for(i = 0; i < resp->peer_count; i++){
			struct in_addr ip;
			char *out;
			int l;

			ip.s_addr = *((uint32_t *)(str+(i*6)));
			out = inet_ntoa(ip);
			l = strlen(out);
			resp->peers[i].address = malloc(l+1);
			strncpy(resp->peers[i].address, out, l);
			*(resp->peers[i].address+l) = '\0';
			resp->peers[i].port = ntohs(*((uint16_t *) (str+4+(i*6))));

		}

	} else if(bencode_is_list(value)){
		bencode_t copy, cur;
		int i=0;

		copy = *value;

		while(bencode_list_has_next(&copy)){
			bencode_list_get_next(&copy, &cur);
			i++;
		}

		resp->peer_count = i;
		resp->peers = calloc(sizeof(struct tracker_peer), resp->peer_count);

		copy = *value;
		i = 0;

		while(bencode_list_has_next(&copy)){
			int ret;
			bencode_list_get_next(&copy, &cur);

			ret = parse_dict(&resp->peers[i], &cur, peer_parse, 3);

			if(!ret){
				printf("Failure in parsing peer");
				return false;
			}
			i++;
		}

	} else {
		error_log("Failure. Expected string or list for peers");
		return false;
	}
	return true;
}

bool parse_peer_id(void *data, bencode_t *value){
	struct tracker_peer *peer = data;

	peer->id = copy_string(value);
	if(!peer->id){
		error_log("Failure. Issue with copying id string.");
		return false;
	}
	return true;
}

bool parse_ip(void *data, bencode_t *value){
	struct tracker_peer *peer = data;

	peer->address = copy_string(value);
	if(!peer->address){
		error_log("Failure. Issue with copying address string.");
		return false;
	}
	return true;
}

bool parse_port(void *data, bencode_t *value){
	struct tracker_peer *peer = data;

	if(!bencode_is_int(value)){
		error_log("Failure. Expected int for creation date");
		return false;
	}

	bencode_int_value(value, &peer->port);
	return true;
}

void print_tracker_resp(struct tracker_resp *resp){
	int i;
	if(resp->failure){
		printf("Failure: %s\n", resp->failure);
		return;
	}

	printf("Warning: %s\n", resp->warning);
	printf("Interval: %ld\n", resp->interval);
	printf("Minimum interval: %ld\n", resp->min_interval);
	printf("Tracker id: %s\n", resp->tracker_id);
	printf("Complete: %ld\n", resp->complete);
	printf("Incomplete: %ld\n", resp->incomplete);
	printf("Peers:\n");

	for(i=0; i < resp->peer_count; i++){
		printf("\tPeer %d:\n", i);
		printf("\t\tId: %s\n", resp->peers[i].id);
		printf("\t\tAddress: %s\n", resp->peers[i].address);
		printf("\t\tPort: %ld\n", resp->peers[i].port);
	}
}

void tracker_resp_cleanup(struct tracker_resp *resp){
	int i;

	for(i=0; i < resp->peer_count; i++){
		free(resp->peers[i].id);
		free(resp->peers[i].address);
	}
	free(resp->peers);
	free(resp->tracker_id);
	free(resp->warning);
	free(resp->failure);
	free(resp);
}

int64_t get_filesize(struct metafile *metafile) {
    uint64_t size = 0;
    uint32_t idx;
    for (idx = 0; idx < metafile->info.file_count; idx++) {
        size += metafile->info.files[idx].length;
    }
    return size;
}

struct metafile *parse_metafile(void *buf, int len){
	bencode_t metafile;
	struct metafile *out = calloc(sizeof(struct metafile), 1);
	int ret;

	bencode_init(&metafile, buf, len);

	if(!bencode_is_dict(&metafile)){
		error_log("Failure. Input is not a bencode dictionary. Expected dictionary.");
		free(out);
		return NULL;
	}

	ret = parse_dict(out, &metafile, meta_parse, 7);

	if(!ret){
		return NULL;
	}

	return out;
}

bool parse_dict(void *data,  bencode_t *dict, struct dict_pair_parse parsers[], int size){
	while(bencode_dict_has_next(dict)){
		bencode_t value;
		const char *key;
		int key_len, i;

		bencode_dict_get_next(dict, &value, &key, &key_len);

		for(i=0; i < size; i++){
			if(!strncmp(parsers[i].key, key, key_len) && strlen(parsers[i].key) == (unsigned int) key_len){
				if(parsers[i].parse(data, &value) == false){
					error_log("Issue parsing");
					return false;
				}
				break;
			}
		}

		if(i == size){
			char *out = calloc(key_len+1, 1);
			strncpy(out, key, key_len);
			info_log("parser for %s not found", out);
			free(out);
		}
	}

	return true;
}

bool parse_info(void *data, bencode_t *value){
	struct metafile *metafile = data;
	EVP_MD_CTX ctx;
	const unsigned char *start;
	int len, ret;

	if(!bencode_is_dict(value)){
		error_log("Failure. Info value is not dict.");
		return false;
	}

	checksum_init(&ctx, SHA1);

	bencode_dict_get_start_and_len(value, (const char **) &start, &len);

	checksum_finish(&ctx, start, len, metafile->info.hash, 20);

	checksum_cleanup(&ctx);

	ret = parse_dict(&metafile->info, value, info_parse, 7);

	if(!ret){
		error_log("Dict parser failed");
		return false;
	}

	if(metafile->info.file_count <= 0){
		error_log("No files. What?");
		return false;
	}

	if(!metafile->info.files[0].path){
		metafile->info.files[0].path = metafile->info.folder_name;
		metafile->info.folder_name = NULL;
	}

	return true;
}

bool parse_announce(void *data, bencode_t *value){
	struct metafile *metafile = data;

	metafile->announce = copy_string(value);
	if(!metafile->announce){
		error_log("Failure. Issue with copying announce string.");
		return false;
	}
	return true;
}

bool parse_announce_list(void *data, bencode_t *value){
	struct metafile *metafile = data;
	bencode_t copy = *value;
	int i = 0;

	if(!bencode_is_list(value)){
		error_log("Failure. Expected list for announce-list");
		return false;
	}

	while(bencode_list_has_next(&copy)){
		bencode_t cur;
		bencode_list_get_next(&copy, &cur);

		if(!bencode_is_list(&cur)){
			error_log("Failure. Expected list for announce-list");
			return false;
		}

		i++;
	}

	metafile->announce_list.groups = calloc(sizeof(struct mf_announce_group), i);
	metafile->announce_list.size = i;

	copy = *value;
	i = 0;

	while(bencode_list_has_next(&copy)){
		bencode_t cur, copy_2;
		int j = 0;

		bencode_list_get_next(&copy, &cur);

		copy_2 = cur;
		while(bencode_list_has_next(&copy_2)){
			bencode_t announce;
			bencode_list_get_next(&copy_2, &announce);

			if(!bencode_is_string(&announce)){
				error_log("Failure. Expected string for announce");
				return false;
			}

			j++;
		}

		metafile->announce_list.groups[i].list = calloc(sizeof(char *), j);
		metafile->announce_list.groups[i].size = j;

		j = 0;
		copy_2 = cur;
		while(bencode_list_has_next(&copy_2)){
			bencode_t announce;
			bencode_list_get_next(&copy_2, &announce);
			const char *url;
			int len;

			bencode_string_value(&announce, &url, &len);

			metafile->announce_list.groups[i].list[j] = calloc(len + 1, 1);
			memcpy(metafile->announce_list.groups[i].list[j], url, len);
			j++;
		}

		i++;
	}

	return true;


}

bool parse_created(void *data, bencode_t *value){
	struct metafile *metafile = data;

	if(!bencode_is_int(value)){
		error_log("Failure. Expected int for creation date");
		return false;
	}

	bencode_int_value(value, &metafile->creation);
	return true;
}

bool parse_comment(void *data, bencode_t *value){
	struct metafile *metafile = data;

	metafile->comment = copy_string(value);
	if(!metafile->comment){
		error_log("Failure. Issue with copying comment string.");
		return false;
	}
	return true;
}

bool parse_creator(void *data, bencode_t *value){
	struct metafile *metafile = data;

	metafile->by = copy_string(value);
	if(!metafile->by){
		error_log("Failure. Issue with copying created by string.");
		return false;
	}
	return true;
}

bool parse_encoding(void *data, bencode_t *value){
	struct metafile *metafile = data;

	metafile->encoding = copy_string(value);
	if(!metafile->encoding){
		error_log("Failure. Issue with copying encoding string.");
		return false;
	}
	return true;
}

bool parse_piece_length(void *data, bencode_t *value){
	struct mf_info *info = data;

	if(!bencode_is_int(value)){
		error_log("Failure. Expected int for piece length.");
		return false;
	}

	bencode_int_value(value, &info->piece_length);
	return true;
}

bool parse_pieces(void *data, bencode_t *value){
	struct mf_info *info = data;
	const char *pieces;
	int len, i;

	if(!bencode_is_string(value)){
		error_log("Failure. Expected string for pieces.");
		return false;
	}

	bencode_string_value(value, &pieces, &len);

	if(len % 20 != 0){
		error_log("Failure. Pieces is not a multiple of 20.");
		return false;
	}

	info->pieces = calloc(sizeof(struct mf_piece), len /  20);
	info->piece_count = len / 20;

	for(i=0; i < info->piece_count; i++){
		memcpy(info->pieces[i].hash, pieces + (20 * i), 20);
	}

	return true;
}

bool parse_private(void *data, bencode_t *value){
	struct mf_info *info = data;

	if(!bencode_is_int(value)){
		error_log("Failure. Expected int for private");
		return false;
	}

	bencode_int_value(value, &info->private);
	return true;
}

bool parse_name(void *data, bencode_t *value){
	struct mf_info *info = data;

	info->folder_name = copy_string(value);
	if(!info->folder_name){
		error_log("Failure. Issue with copying encoding string.");
		return false;
	}
	return true;
}

bool parse_info_length(void *data, bencode_t *value){
	struct mf_info *info = data;

	if(!bencode_is_int(value)){
		error_log("Failure. Expected int for private");
		return false;
	}

	if(info->file_count){
		bencode_int_value(value, &info->files[0].length);
	} else {
		info->file_count = 1;
		info->files = calloc(sizeof(struct mf_file), 1);
		bencode_int_value(value, &info->files[0].length);
	}

	return true;
}

bool parse_info_md5sum(void *data, bencode_t *value){
	struct mf_info *info = data;

	if(info->file_count){
		info->files[0].md5sum = copy_string(value);
	} else {
		info->file_count = 1;
		info->files = calloc(sizeof(struct mf_file), 1);
		info->files[0].md5sum = copy_string(value);
	}

	if(!info->files[0].md5sum){
		error_log("Failure. Issue with copying encoding string.");
		return false;
	}
	return true;
}

bool parse_files(void *data, bencode_t *value){
	struct mf_info *info = data;
	bencode_t file, copy = *value;
	int len = 0, i = 0;

	if(!bencode_is_list(value)){
		error_log("Failure. Expected dict for list");
		return false;
	}

	while(bencode_list_has_next(&copy)){
		bencode_list_get_next(&copy, &file);
		len++;
	}

	info->files = calloc(sizeof(struct mf_file), len);
	info->file_count = len;

	copy = *value;

	while(bencode_list_has_next(&copy)){
		int ret;
		bencode_list_get_next(&copy, &file);

		ret = parse_dict(&info->files[i], &file, files_parse, 3);

		if(!ret){
			error_log("File dict parsing failed");
			return false;
		}

		i++;
	}

	return true;
}

bool parse_files_path(void *data, bencode_t *value){
	struct mf_file *file = data;
	bencode_t str, copy = *value;
	const char *s;
	int len = 0, l;

	if(!bencode_is_list(value)){
		error_log("Failure. Expected list for path");
		return false;
	}
	
	while(bencode_list_has_next(&copy)){
		bencode_list_get_next(&copy, &str);

		if(!bencode_is_string(&str)){
			error_log("Failure. Expected string for path value");
			return false;
		}

		bencode_string_value(&str, &s, &l);
		len += l + 1;
	}

	file->path = calloc(len+1, 1);

	copy = *value;

	len = 0;

	while(bencode_list_has_next(&copy)){
		bencode_list_get_next(&copy, &str);

		bencode_string_value(&str, &s, &l);

		memcpy(file->path + len, s, l);
		len += l;

		*(file->path + len) = '/';
		len++;
	}
	file->path[len-1] = '\0';

	return true;
}

bool parse_files_length(void *data, bencode_t *value){
	struct mf_file *file = data;

	if(!bencode_is_int(value)){
		error_log("Failure. Expected int for private");
		return false;
	}

	
	bencode_int_value(value, &file->length);

	return true;
}

bool parse_files_md5sum(void *data, bencode_t *value){
	struct mf_file *file = data;

	
	file->md5sum = copy_string(value);

	if(!file->md5sum){
		error_log("Failure. Issue with copying encoding string.");
		return false;
	}
	return true;
}

char *copy_string(bencode_t *object){
	char *out;
	const char *str;
	int len;

	if(!bencode_is_string(object)){
		error_log("Failure. Announce value is not string");
		return NULL;
	}
	bencode_string_value(object, &str, &len);
	out = calloc(len + 1, 1);
	memcpy(out, str, len);
	return out;
}


bool process_key(char *keys[], const char *key, int len){
	int i;

	for(i = 0; i < KEYS_LEN; i++){
		if(!strncmp(keys[i], key, len) && strlen(keys[i]) == (unsigned int) len){
			return i;
		}
	}

	return false;
}

void metafile_cleanup(struct metafile *metafile){
	int i, j;
	free(metafile->announce);
	free(metafile->comment);
	free(metafile->by);
	free(metafile->encoding);

	free(metafile->info.folder_name);
	free(metafile->info.pieces);

	for(i = 0; i < metafile->info.file_count; i++){
		free(metafile->info.files[i].path);
		free(metafile->info.files[i].md5sum);
	}

	free(metafile->info.files);

	for(i = 0; i < metafile->announce_list.size; i++){
		for(j = 0; j < metafile->announce_list.groups[i].size; j++){
			free(metafile->announce_list.groups[i].list[j]);
		}
		free(metafile->announce_list.groups[i].list);
	}
	free(metafile->announce_list.groups);

	free(metafile);
}

void print_info(struct mf_info *info, int print_hashes){
	int i;
	printf("\tInfo hash: %s\n", print_hex(info->hash, 20));
	printf("\tPiece length: %ld\n", info->piece_length);
	printf("\tPiece count: %ld\n", info->piece_count);
	if(print_hashes){
		printf("\tPieces:\n");

		for(i=0; i < info->piece_count; i++){
			printf("\t\tPiece %d: %s\n", i, print_hex(info->pieces[i].hash, 20));
		}
	}
	

	if(info->folder_name){
		printf("\tFolder Name: %s\n", info->folder_name);
	} else {
		printf("\tFolder Name: %s\n", "None");
	}

	printf("\tFile count: %ld\n", info->file_count);
	printf("\tFiles:\n");

	for(i=0; i < info->file_count; i++){
		printf("\t\tFile %d:\n", i);
		printf("\t\t\tPath: %s\n", info->files[i].path);
		printf("\t\t\tLength: %ld\n", info->files[i].length);
		printf("\t\tMD5SUM: %s\n", info->files[i].md5sum);
	}

	printf("\tPrivate: %ld\n", info->private);


}

void print_metafile(struct metafile *metafile, int print_hashes){
	int i, j;
	printf("Info\n");
	print_info(&metafile->info, print_hashes);
	printf("Announce: %s\n", metafile->announce);
	for(i=0; i < metafile->announce_list.size; i++){
		printf("\tAnnounce group %d:\n", i);

		for(j=0; j < metafile->announce_list.groups[i].size; j++){
			printf("\t\t%s\n", metafile->announce_list.groups[i].list[j]);
		}
	}

	printf("Comment: %s\n", metafile->comment);
	printf("Created by: %s\n", metafile->by);
	printf("Created at: %ld\n", metafile->creation);
	printf("Encoding: %s\n", metafile->encoding);

}

void to_hex(void *o, void *a, int len_in){
  int i;
  char *out = o;
  uint8_t *in = a;

  for(i=0; i < len_in; i++){
    sprintf(out+(2*i), "%02x", *(in+i));
  }
}

char *print_hex(void *a, int len_in){
  static char hash[41];

  to_hex(hash, a, len_in);
  return hash;
}
