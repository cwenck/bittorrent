#include "hash.h"

#include <string.h>
#include <strings.h>
#include <stdbool.h>

#include "log.h"

/* third party libraries */
#include <openssl/evp.h>


bool checksum_init(EVP_MD_CTX *ctx, const EVP_MD *hash_type) {
    if (!ctx) {
        return false;
    }

    EVP_MD_CTX_init(ctx);
    EVP_DigestInit_ex(ctx, hash_type, NULL);
    return true;
}

bool checksum_update(EVP_MD_CTX *ctx, const void *data, size_t len) {
    return EVP_DigestUpdate(ctx, data, len);
}

bool checksum_finish(EVP_MD_CTX *ctx, const void *data, size_t data_len, void *hash, size_t hash_len) {
    int status = true;

    /* If the data is NULL but the length isn't 0 */
    if (!data && data_len != 0) {
        error_log("data is null with a nonzero length");
        return false;
    }

    if (EVP_MD_type(SHA1) == EVP_MD_CTX_type(ctx) && hash_len < SHA1_SIZE) {
        /* ctx is for SHA1 */
        warn_log("hash buffer is too small for a SHA1 hash, must be at least %u bytes", SHA1_SIZE);
        return false;
    } else if (EVP_MD_type(MD5) == EVP_MD_CTX_type(ctx) && hash_len < MD5_SIZE) {
        /* ctx is for MD5 */
        warn_log("hash buffer is too small for a MD5 hash, must be at least %u bytes", MD5_SIZE);
        return false;
    }

    if (data && data_len) {
        /* There is more data to add */
        status = EVP_DigestUpdate(ctx, data, data_len);
    }

    if (status) {
        return EVP_DigestFinal_ex(ctx, hash, NULL);
    }

    return false;
}

void checksum_cleanup(EVP_MD_CTX *ctx) {
    EVP_MD_CTX_cleanup(ctx);
}