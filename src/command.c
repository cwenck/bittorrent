#include "command.h"

#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>

#include "log.h"
#include "common.h"
#include "util.h"

#include "peer.h"
#include "piece_set.h"
#include "client.h"

static bool parse_args(char *arg_str, int expected_args, char *args[]) {
    char *tmp_arg = strtok_r(arg_str, " \n", &arg_str);
    int arg_count = 0;
    while (tmp_arg) {
        if (arg_count >= expected_args) {
            return false;
        }

        args[arg_count] = tmp_arg;
        arg_count++;

        /* Parse the next argument */
        tmp_arg = strtok_r(arg_str, " \n", &arg_str);
    }
    return arg_count == expected_args;
}

/* Prototypes for CmdRunner Functions */

static void run_test(struct cmd *cmd_data, char *arg_str);
static void run_completed();
static void run_status();
static void run_blocks(struct cmd *cmd_data, char *arg_str);
static void run_peer_count(struct cmd *cmd_data, char *arg_str);
static void run_peer_list(struct cmd *cmd_data, char *arg_str);
static void run_single_peer(struct cmd *cmd_data, char *arg_str);
static void run_help(struct cmd *cmd_data, char *arg_str);

static void exit_com() {
    exit(0);
}

static struct cmd cmd_arr[] = {
    {"test", "Usage: Test <arg1> <arg2>", "a test command", run_test},
    {"peercount", "Usage: peercount", "number of current peers", run_peer_count},
    {"peerlist", "Usage: peerlist", "list all the peers", run_peer_list},
    {"peer", "Usage: peer <sockfd>", "print a peer by socket", run_single_peer},
    {"pieceset", "Usage: pieceset", "print stats for the piece set", pieceset_print_stats},
    {"pieces", "Usage: pieces", "print all the pieces", pieceset_print_pieces},
    {"blocks", "Usage: block <piece index>", "print a block", run_blocks},
    {"blocklist", "Usage: blocklist", "print all the pieces and their blocks", pieceset_print_all},
    {"completed", "Usage: completed", "number of piecs downloaded", run_completed},
    {"status", "Usage: status", "takes over the command line and shows continuous progress", run_status},
    {"exit", "Usage: exit", "exit/quit the client", exit_com},
    {"help", "Usage: help, or Usage: help <CMD>", "get help", run_help},
    {0}
};

void command_loop() {
    char line_buffer[CMD_MAX_LENGTH];

    info_log("Starting command loop");
    while (fgets(line_buffer, CMD_MAX_LENGTH, stdin)) {
        if (feof(stdin)) {
            break;
        }

        char *args = NULL;
        char *cmd_str = strtok_r(line_buffer, " \n", &args);

        struct cmd *tmp_cmd = cmd_arr;
        bool valid_cmd = false;
        while (tmp_cmd->cmd_str && tmp_cmd->run) {
            if (cmd_str != NULL && strcasecmp(cmd_str, tmp_cmd->cmd_str) == 0) {
                valid_cmd = true;
                break;
            }
            tmp_cmd++;
        }

        if (!valid_cmd) {
            info_print("Invalid Command\n");
        } else {
            tmp_cmd->run(tmp_cmd, args);
        }
    }

    info_log("Exiting command loop");
}

static void run_help(struct cmd *cmd_data, char *arg_str) {
    UNUSED(cmd_data);
    UNUSED(arg_str);

    char *cmd_str;
    if (!parse_args(arg_str, 1, &cmd_str)) {
        info_print("The following commands are supported:\n");
        for (struct cmd *cmd = cmd_arr; cmd->cmd_str && cmd->run; cmd++) {
            info_print("   %s", cmd->cmd_str);
            if (cmd->description_str) {
                info_print(" - %s\n", cmd->description_str);
            } else {
                info_print("\n");
            }
        }
    } else {
        struct cmd *tmp_cmd = cmd_arr;
        bool valid_cmd = false;
        while (tmp_cmd->cmd_str && tmp_cmd->run) {
            if (cmd_str != NULL && strcasecmp(cmd_str, tmp_cmd->cmd_str) == 0) {
                valid_cmd = true;
                break;
            }
            tmp_cmd++;
        }

        if (!valid_cmd) {
            info_print("Unknown Command\n");
        } else {
            info_print("   %s", tmp_cmd->cmd_str);
            if (tmp_cmd->description_str) {
                info_print(" - %s\n", tmp_cmd->description_str);
            } else {
                info_print("\n");
            }
            if (tmp_cmd->usage_str) {
                info_print("   %s\n", tmp_cmd->usage_str);
            }
        }
    }
}

static void run_test(struct cmd *cmd_data, char *arg_str) {
    char *split_args[2];
    if (!parse_args(arg_str, 2, split_args)) {
        info_print("%s", cmd_data->usage_str);
        info_print("\n");
    } else {
        info_print("%s :: %s\n", split_args[0], split_args[1]);
    }
}

static void run_completed() {
    uint32_t completed = pieceset_pieces_completed();
    uint32_t count = piece_count();
    info_print("%u/%u - %.2f%%\n", completed, count, 100 * ((double) completed / (double) count));
}

static void run_status() {
    uint32_t count = piece_count();
    uint32_t completed = pieceset_pieces_completed();

    struct pollfd pollfd;
    pollfd.fd = STDIN_FILENO;
    pollfd.events = POLLIN;

    if (completed < count) {
        while (completed < count) {
            int status = poll(&pollfd, 1, 100);
            if (status == 0)  {
                completed = pieceset_pieces_completed();
                struct timespec current;
                clock_gettime(CLOCK_MONOTONIC, &current);
                struct timespec diff = timespec_elapsed(&current, &g_client_info->begin_time);

                uint32_t min = diff.tv_sec / 60;
                uint32_t sec = diff.tv_sec % 60;

                info_print("\33[2K\r%u/%u - %0.2f%% - %u Peer(s) - TIME: %02u:%02u ", completed, count, peer_count(), 100 * ((double) completed / (double) count), min, sec);
                fflush(LOGPRINT_OUTPUT_STREAM);
            } else {
                if ((pollfd.revents & POLLIN) == POLLIN) {
                    char *ret;
                    char line_buffer[CMD_MAX_LENGTH];
                    ret = fgets(line_buffer, CMD_MAX_LENGTH, stdin);
                    UNUSED(ret);
                }
                break;
            }
        }
    } else {
        struct timespec diff = timespec_elapsed(&g_client_info->end_time, &g_client_info->begin_time);

        uint32_t min = diff.tv_sec / 60;
        uint32_t sec = diff.tv_sec % 60;

        info_print("\33[2K\r%u/%u - %0.2f%% - %u Peer(s) - TIME: %02u:%02u ", completed, count, peer_count(), 100 * ((double) completed / (double) count), min, sec);
    }
}

static void run_blocks(struct cmd *cmd_data, char *arg_str) {
    char *args[1];
    if (!parse_args(arg_str, 1, args)) {
        info_print("%s", cmd_data->usage_str);
        info_print("\n");
    } else {
        uint32_t piece_i = atol(args[0]);
        pieceset_print_piece(piece_i);
        pieceset_print_blocks(piece_i);
    }
}

static void run_peer_count(struct cmd *cmd_data, char *arg_str) {
    UNUSED(cmd_data);
    UNUSED(arg_str);
    info_print("Peer Count = %d\n", peer_count());
}

static void tbl_rdlock() {
    pthread_rwlock_rdlock(&g_peers->tbl_lock);
}

static void tbl_unlock() {
    pthread_rwlock_unlock(&g_peers->tbl_lock);
}

static void print_single_peer(struct peer *peer) {
    if (peer == NULL) {
        info_print("Peer does not exist");
        return;
    }
    info_print("Sockfd: %d\n", peer->sockfd);
    info_print("\tPstr: %s (%d)\n", peer->pstr, strlen(peer->pstr));

    char tmp_peer_id[21] = {0};
    memcpy(tmp_peer_id, peer->peer_id, 20);
    info_print("\tPeer id: %s (%d)\n", tmp_peer_id, strlen(tmp_peer_id));

    info_print("\tAddr: %s:%d\n", inet_ntoa(peer->addr.sin_addr), ntohs(peer->addr.sin_port));

    if (peer->sent_handshake) {
        info_print("\tWe have sent a handshake\n");
    } else {
        info_print("\tWe have not sent a handshake\n");
    }

    if (peer->recv_handshake) {
        info_print("\tWe have received a handshake\n");
    } else {
        info_print("\tWe have not received a handshake\n");
    }

    if (peer->am_choking) {
        info_print("\tWe are choking this peer\n");
    } else {
        info_print("\tWe are not choking this peer\n");
    }

    if (peer->am_interested) {
        info_print("\tWe are interested in this peer\n");
    } else {
        info_print("\tWe are not interested in this peer\n");
    }

    if (peer->peer_choking) {
        info_print("\tWe are being choked by this peer\n");
    } else {
        info_print("\tWe are not being choked by this peer\n");
    }

    if (peer->peer_interested) {
        info_print("\tThis peer is interested in us\n");
    } else {
        info_print("\tThis peer is not interested in us\n");
    }

    info_print("\tData last sent %ld.%ld seconds ago\n", peer->last_sent.tv_sec, peer->last_sent.tv_nsec);
    info_print("\tData last received %ld.%ld seconds ago\n", peer->last_recv.tv_sec, peer->last_recv.tv_nsec);

    info_print("\tUpload limit: %lu bytes\n", peer->bytes_sent_limit);
    info_print("\tEndgame: %s\n", (peer->endgame) ? "True" : "False");

    info_print("\n");
}

static void run_single_peer(struct cmd *cmd_data, char *arg_str) {
    UNUSED(cmd_data);
    int sockfd = atoi(arg_str);

    struct peer *peer = peer_find(sockfd);
    if (peer != NULL) {
        print_single_peer(peer);
    } else {
        info_print("Peer does not exist\n");
    }
}

static void run_peer_list(struct cmd *cmd_data, char *arg_str) {
    UNUSED(cmd_data);
    UNUSED(arg_str);

    struct peer *entry, *tmp;
    tbl_rdlock();

    HASH_ITER(hh, g_peers->head, entry, tmp) {
        peer_rdlock(entry);
        print_single_peer(entry);
        peer_unlock(entry);
    }

    tbl_unlock();
}
