#include "peer_thread.h"

#include <sys/select.h>
#include <stdlib.h>
#include <stdbool.h>
#include <netdb.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <poll.h>


#include "piece_set.h"
#include "peer.h"
#include "common.h"
#include "netmsg_packer.h"
#include "log.h"
#include "util.h"
#include "metafile_parse.h"
#include "client.h"
#include "thread_registry.h"

#define RDLOCK(peer) do{peer_rdlock(peer);} while(0)
#define WRLOCK(peer) do{peer_wrlock(peer);} while(0)
#define UNLOCK(peer) do{peer_unlock(peer);} while(0)

bool peer_process_request(struct peer *peer_data, void *request, size_t len);
bool request_block(struct peer *self);
void complete_request(struct peer *self, struct block_data *block);
void clear_requests(struct peer *self);
bool send_am_interested(struct peer *self);

void add_in_request(struct peer *self, size_t piece_index, size_t offset, size_t len);
void cancel_in_request(struct peer *self, size_t piece_index, size_t offset);
void clear_in_request(struct peer *self);
void reply_in_request(struct peer *self);

void update_send_timestamp(struct peer *peer) {
    clock_gettime(CLOCK_REALTIME, &(peer->last_sent));
}

void update_recv_timestamp(struct peer *peer) {
    clock_gettime(CLOCK_REALTIME, &(peer->last_recv));
}

bool validate_handshake(uint8_t *message, uint32_t message_len, char **pstr, uint8_t **peer_id, struct peer *self) {
    uint8_t *info_hash;

    /* Unpack */

    unpack_handshake(message, message_len, pstr, &info_hash, peer_id);


    /* Check valid inputs */

    if (memcmp(*peer_id, g_client_info->peer_id, 20) == 0 || info_hash == NULL || memcmp(info_hash, g_metafile->info.hash, SHA1_SIZE) != 0) {
        error_log("Peer doesn't have the correct info hash, disconnecting it from %s", inet_ntoa(self->addr.sin_addr));

        /* Clean up */

        if (info_hash != NULL) {
            free(info_hash);
        }
        if (pstr != NULL) {
            free(*pstr);
        }
        if (peer_id != NULL) {
            free(*peer_id);
        }
        return false;
    }
    free(info_hash);
    return true;
}

bool send_handshake(int sockfd) {
    uint8_t *packet, *bitfield;
    size_t packetlen;
    pack_handshake(g_metafile->info.hash, g_client_info->peer_id, &packet, &packetlen);
    int ret = send(sockfd, packet, packetlen, MSG_NOSIGNAL);

    if (ret < 1) {
        free(packet);
        warn_log("Send failed");
        return false;
    }

    free(packet);

    pieceset_generate_bitfield(&bitfield);
    pack_bitfield(bitfield, piece_bitfield_size(), &packet, &packetlen);
    free(bitfield);

    ret = send(sockfd, packet, packetlen, MSG_NOSIGNAL);

    if (ret < 1) {
        free(packet);
        warn_log("Send failed");
        return false;
    }

    free(packet);
    return true;
}

bool recv_data(struct peer *self) {
    if (self->buf_recv_b) {
        //debug_log("Reseting recv buf");
        free(self->recv_buf);
        self->recv_len = 0;
        self->len_bytes_recv = 0;
        self->buf_bytes_recv = 0;
        self->buf_recv_b = false;
        self->len_recv_b = false;
    }

    /* Note that there is a check if fd is -1 */

    /* Get packet length */
    if (!self->len_recv_b) {
        if (self->recv_handshake == true) {
            ssize_t amount = recv(self->sockfd, ((uint8_t *)(&self->recv_len)) + self->len_bytes_recv, sizeof(self->recv_len) - self->len_bytes_recv, MSG_DONTWAIT);

            //debug_log("Receiving len");
            if ((amount == -1 && errno != EAGAIN && errno != EWOULDBLOCK) || amount == 0) {
                // warn_log("Recv error %d %s", amount, inet_ntoa(self->addr.sin_addr));
                return false;
            } else if (amount == -1 && (errno == EAGAIN || errno == EWOULDBLOCK)) {
                //debug_log("EAGAIN");
                return true;
            }

            update_recv_timestamp(self);

            self->len_bytes_recv += amount;

            //debug_log("len recvd %d", self->len_bytes_recv);
            if (self->len_bytes_recv == sizeof(self->recv_len)) {
                self->recv_len = ntohl(self->recv_len);
                //debug_log("Expecting %d %s", self->recv_len, inet_ntoa(self->addr.sin_addr));
                if (self->recv_len == 0) {
                    self->recv_buf = NULL;
                    self->buf_recv_b = true;
                } else {
                    self->recv_buf = malloc(self->recv_len);
                }
                self->len_recv_b = true;
            }
        } else {
            self->recv_len = 0;
            ssize_t amount = recv(self->sockfd, ((uint8_t *)(&self->recv_len)), 1, MSG_DONTWAIT);

            //debug_log("Receiving handshake len");
            if ((amount == -1 && errno != EAGAIN && errno != EWOULDBLOCK) || amount == 0) {
                // warn_log("Recv error %d", amount);
                return false;
            } else if (amount == -1 && (errno == EAGAIN || errno == EWOULDBLOCK)) {
                //debug_log("EAGAIN");
                return true;
            }

            update_recv_timestamp(self);

            self->len_bytes_recv = 4;

            //debug_log("len recvd %d", self->len_bytes_recv);
            if (self->len_bytes_recv == sizeof(self->recv_len)) {
                self->recv_len += 48;
                // debug_log("Expecting %d", self->recv_len);
                self->recv_buf = malloc(self->recv_len);
                self->len_recv_b = true;
            }
        }
    }

    if (self->len_recv_b && !self->buf_recv_b) {
        /* Get packet itself */
        ssize_t amount = recv(self->sockfd, self->recv_buf + self->buf_bytes_recv, self->recv_len - self->buf_bytes_recv, MSG_DONTWAIT);

        /* Check packet length */

        if ((amount == -1 && errno != EAGAIN && errno != EWOULDBLOCK) || amount == 0) {
            warn_log("Recv error %d", amount);
            free(self->recv_buf);
            self->recv_buf = NULL;
            return false;
        } else if (amount == -1 && (errno == EAGAIN || errno == EWOULDBLOCK)) {
            return true;
        }

        update_recv_timestamp(self);

        self->buf_bytes_recv += amount;

        //debug_log("recvd %d", self->buf_bytes_recv);

    }

    if (self->buf_bytes_recv == self->recv_len && self->recv_len != 0) {
        //debug_log("Received full buffer");
        self->buf_recv_b = true;
    }

    return true;
}

bool process_message(struct peer *self) {

    //trace_buffer_dump(self->recv_buf, self->recv_len, "Received %d from %s", self->recv_len, inet_ntoa(self->addr.sin_addr));
    /* Have they sent us a handshake yet? */

    if (self->recv_handshake) {
        int ret = true;
        update_send_timestamp(self);

        if (self->recv_len) {
            ret = peer_process_request(self, self->recv_buf, self->recv_len);
        }

        return ret;

    } else {
        /* Get fields ready, we're assuming that this is an incoming handshake */

        char *pstr;
        uint8_t *peer_id;

        /* Check if handshake is valid */

        if (validate_handshake(self->recv_buf, self->recv_len, &pstr, &peer_id, self)) {
            int ret;

            strcpy(self->pstr, pstr);
            memcpy(self->peer_id, peer_id, 20);
            self->recv_handshake = true;

            /* Unlock and clean up */

            free(pstr);
            free(peer_id);

            /* Once validated, send it! */
            if (!self->sent_handshake) {
                ret = send_handshake(self->sockfd);

                if (!ret) {
                    return false;
                }

                update_send_timestamp(self);

                /* Finally, update the field */

                self->sent_handshake = true;
            }
        } else {
            /* They haven't sent us a handshake yet, so bye bye */
            error_log("Handshake did not validate");
            return false;
        }

    }

    return true;
}

bool peer_process_request(struct peer *peer_data, void *request, size_t len) {

    if (peer_data->sent_handshake && peer_data->recv_handshake) {

        /* Unpack the generic message to determine the type */
        enum msg_type type;
        uint8_t *payload = NULL;
        size_t payload_len = 0;
        unpack_msg_len(request, len, &type, &payload, &payload_len);

        switch (type) {
            case MSG_TYPE_CHOKE:
                //trace_log("Setting choke");
                peer_data->peer_choking = true;
                clear_requests(peer_data);
                break;
            case MSG_TYPE_UNCHOKE:
                //trace_log("Setting unchoke");
                peer_data->peer_choking = false;
                break;
            case MSG_TYPE_INTERESTED:
                // trace_log("Setting interested");
                peer_data->peer_interested = true;
                break;
            case MSG_TYPE_NOT_INTERESTED:
                // trace_log("Setting uninterested");
                peer_data->peer_interested = false;
                break;
            case MSG_TYPE_HAVE: {
                //trace_log("Setting have");
                uint32_t piece_index = 0;
                unpack_have(payload, &piece_index);
                if (piece_index < piece_map_size()) {
                    peer_data->piece_map[piece_index] = true;
                    pieceset_peer_has(piece_index);

                    if (!peer_data->am_interested && pieceset_available_blocks(pieceset_piece(piece_index))) {
                        return send_am_interested(peer_data);
                    }
                } else {
                    info_log("Invalid HAVE message for piece index that doesn't exist");
                }
                break;
            }
            case MSG_TYPE_BITFIELD: {
                uint8_t *bitfield = NULL;
                unpack_bitfield(payload, payload_len, &bitfield);
                //trace_log("Setting bitfield");
                if (bitfield) {
                    bool *new_piece_map = NULL;
                    if (piece_bitfield_to_map(bitfield, &new_piece_map) && new_piece_map) {
                        free(peer_data->piece_map);
                        peer_data->piece_map = new_piece_map;
                    } else {
                        error_log("Failed to create piece map");
                    }
                    pieceset_peer_bitfield(peer_data);
                }
                free(bitfield);

                int i = pieceset_has_new(peer_data->piece_map);
                if (i && !peer_data->am_interested && !pieceset_done()) {
                    return send_am_interested(peer_data);
                }
                break;
            }
            case MSG_TYPE_REQUEST: {
                // info_log("WIP : Piece requested (TODO : Make better / add a queue)");
                uint32_t piece_index = 0;
                uint32_t offset = 0;
                uint32_t block_len = 0;
                unpack_request(payload, &piece_index, &offset, &block_len);

                add_in_request(peer_data, piece_index, offset, block_len);
                break;
            }
            case MSG_TYPE_PIECE: {
                uint32_t piece_index = 0;
                uint32_t piece_offset = 0;
                uint8_t *block = NULL;

                unpack_piece(payload, payload_len, &piece_index, &piece_offset, &block);

                struct block_data *block_info = pieceset_find_block(piece_index, piece_offset);
                if (block_info) {
                    //info_log("Writing { piece = %u, offset = %u }", piece_index, piece_offset);
                    pieceset_write_block(block_info, block);
                    complete_request(peer_data, block_info);
                    WRLOCK(peer_data);
                    peer_data->bytes_received += payload_len - 1;
                    UNLOCK(peer_data);
                } else {
                    info_log("Invalid block received %d %d", piece_index, piece_offset);
                    return false;
                }

                free(block);

                break;
            }
            case MSG_TYPE_CANCEL: {
                uint32_t piece_index = 0;
                uint32_t piece_offset = 0, len = 0;

                unpack_cancel(payload, &piece_index, &piece_offset, &len);

                cancel_in_request(peer_data, piece_index, piece_offset);
                break;
            }
            case MSG_TYPE_KEEP_ALIVE:
                //info_log("Received Keep Alive");
                break;
            default:
                warn_log("Unknown message type received %x", type);
                break;
        }
    }
    return true;
}

void add_in_request(struct peer *self, size_t piece_index, size_t offset, size_t len) {
    if (self) {
        for (int i = 0; i < NUM_INCOMING; i++) {
            if (self->incoming_requests[i].completed) {
                self->incoming_requests[i].piece_index = piece_index;
                self->incoming_requests[i].offset = offset;
                self->incoming_requests[i].len = len;
                self->incoming_requests[i].completed = false;
                break;
            }
        }
    }
}

void clear_in_request(struct peer *self) {
    if (self) {
        info_log("Clearing requests");
        for (int i = 0; i < NUM_INCOMING; i++) {
            if (self->incoming_requests[i].completed) {
                self->incoming_requests[i].completed = true;
            }
        }
    }
}

void cancel_in_request(struct peer *self, size_t piece_index, size_t offset) {
    if (self) {
        for (int i = 0; i < NUM_INCOMING; i++) {
            if (!self->incoming_requests[i].completed && self->incoming_requests[i].piece_index == piece_index && self->incoming_requests[i].offset == offset) {
                self->incoming_requests[i].completed = true;
                info_log("Canceling request");
            }
        }
    }
}

bool reply_in_requests(struct peer *self) {
    if (self && !self->am_choking && self->peer_interested) {
        WRLOCK(self);
        for (int i = 0; i < NUM_INCOMING && self->bytes_sent <= self->bytes_sent_limit; i++) {
            if (!self->incoming_requests[i].completed) {
                struct in_request *req = &self->incoming_requests[i];
                uint8_t *packet;
                size_t len;
                int ret;

                pack_piece(req->piece_index, req->offset, NULL, req->len, &packet, &len);

                pieceset_read_block(req->piece_index, req->offset, packet + 13, req->len);

                self->bytes_sent += req->len;

                UNLOCK(self);

                ret = send(self->sockfd, packet, len, MSG_NOSIGNAL);

                free(packet);
                req->completed = true;

                if (ret == -1) {
                    return false;
                }

                WRLOCK(self);
            }
        }
        UNLOCK(self);
    }
    return true;
}

void clear_requests(struct peer *self) {
    for (int i = 0; i < NUM_OUTGOING; i++) {
        self->outgoing_requests[i].completed = true;
        if (self->outgoing_requests[i].block && !self->outgoing_requests[i].completed && self->outgoing_requests[i].block->status == BLOCK_REQUESTED) {
            pieceset_timeout_block(self->outgoing_requests[i].block);
        }
    }
    self->requests_pending = 0;
    self->piece_downloading = NULL;
}

bool send_cancel(struct peer *self, struct block_data *block) {

    if (self && block) {
        size_t len;
        uint8_t *packet;
        int ret;

        pack_cancel(block->piece_index, block->offset, block->len, &packet, &len);

        ret = send(self->sockfd, packet, len, MSG_NOSIGNAL);

        free(packet);

        if (ret == -1) {
            error_log("Issue with send");
            return false;
        }

        update_send_timestamp(self);
    }
    return true;
}

void complete_request(struct peer *self, struct block_data *block) {
    if (self && block) {
        for (int i = 0; i < NUM_OUTGOING; i++) {
            if (self->outgoing_requests[i].block == block) {
                self->outgoing_requests[i].completed = true;
                self->requests_pending--;
            }
        }
    }
}

bool request_block(struct peer *self) {
    if (self->am_interested && !self->peer_choking) {
        //trace_log("Requesting block %p", self->piece_downloading);
        struct timespec cur;
        clock_gettime(CLOCK_REALTIME, &cur);

        for (int i = 0; i < NUM_OUTGOING; i++) {
            struct block_data *block = NULL;
            size_t len;
            ssize_t ret;
            uint8_t *msg;
            if (self->outgoing_requests[i].block && self->outgoing_requests[i].block->status == BLOCK_STORED) {
                if (!self->outgoing_requests[i].completed) {
                    if (!send_cancel(self, self->outgoing_requests[i].block)) {
                        return false;
                    }
                    self->requests_pending--;
                    self->outgoing_requests[i].completed = true;
                }
            }
            if(self->endgame && self->outgoing_requests[i].completed){
                if(!self->piece_downloading || self->piece_downloading->status == PIECE_STORED){
                    self->piece_downloading = pieceset_endgame_select_piece(self);
                    self->endgame_last_block = NULL;
                }
                if (self->piece_downloading) {
                    block = pieceset_endgame_next_block(self->piece_downloading, self->endgame_last_block);
                    if (block) {
                        self->endgame_last_block = block;
                        self->requests_pending++;
                        self->outgoing_requests[i].completed = false;
                        self->outgoing_requests[i].block = block;
                        clock_gettime(CLOCK_REALTIME, &(self->outgoing_requests[i].req_at));
                        //info_log("Requesting piece %d %d %d", block->piece_index, block->block_index, self->requests_pending);
                    } else {
                        self->piece_downloading = pieceset_endgame_select_piece(self);
                        self->endgame_last_block = NULL;
                    }
                } else {
                    return true;
                }
            } else if (self->outgoing_requests[i].completed) {
                if (self->piece_downloading == NULL || !pieceset_available_blocks(self->piece_downloading)) {
                    //info_log("No blocks available");
                    self->piece_downloading = pieceset_select_piece(self);
                }
                if (self->piece_downloading != NULL) {
                    block = pieceset_next_block(self->piece_downloading);

                    //info_log("Requesting piece %d", self->piece_downloading);
                    //pieceset_print_piece(self->piece_downloading);
                    if (!block) {
                        return true;
                    }

                    self->outgoing_requests[i].block = block;
                    self->outgoing_requests[i].completed = false;
                    clock_gettime(CLOCK_REALTIME, &(self->outgoing_requests[i].req_at));
                    self->requests_pending++;
                } else {
                    return true;
                }
            } else if (self->outgoing_requests[i].completed == false && (cur.tv_sec - self->outgoing_requests[i].req_at.tv_sec) >= 30) {

                block = self->outgoing_requests[i].block;
                //info_log("Resending request Piece: %d Block: %d Peer: %s", block->piece_index, block->block_index, inet_ntoa(self->addr.sin_addr));
                clock_gettime(CLOCK_REALTIME, &(self->outgoing_requests[i].req_at));
            }
            if (block) {
                pack_request(block->piece_index, block->offset, block->len, &msg, &len);

                ret = send(self->sockfd, msg, len, MSG_NOSIGNAL);

                free(msg);

                if (ret == -1) {
                    error_log("Issue with send");
                    return false;
                }

                update_send_timestamp(self);

                break;
            }


        }


    }

    //trace_log("Requested block");
    return true;
}

bool send_am_interested(struct peer *self) {
    uint8_t *packet;
    uint64_t len;
    int ret;
    pack_interested(&packet, &len);
    // info_log("Sending interested %p", packet);

    ret = send(self->sockfd, packet, len, MSG_NOSIGNAL);


    if (ret == -1) {
        return false;
    }

    update_send_timestamp(self);
    self->am_interested = true;

    return true;
}

void disconnect(struct peer *self) {
    // warn_log("Disconnecting peer %d", self->sockfd);
    clear_requests(self);
    peer_disconnect(self);
    thread_finished();
}

void *peer_thread(void *data) {
    struct peer_thread_options *opts = data;
    struct peer *self = opts->self;
    if (self == NULL) {
        return NULL;
    }
    int ret;

    self->rand_seed = pthread_self();


    WRLOCK(self);

    ret = send_handshake(self->sockfd);

    if (!ret) {
        UNLOCK(self);
        disconnect(self);
        return NULL;
    }

    // info_log("Starting thread for %d %d", self->sockfd, self->addr.sin_port);
    update_send_timestamp(self);
    update_recv_timestamp(self);
    self->sent_handshake = true;

    UNLOCK(self);

    for (;;) {
        int r;

        //r = poll(fds, 2, 5000);

        //WRLOCK(self);
        if(true){

            bool cont = true;
            //debug_log("Exiting select");
            while (cont) {
                ret = recv_data(self);
                //debug_log("Continueing select");
                if (!ret) {
                    disconnect(self);
                    return NULL;
                }

                if (self->buf_recv_b) {
                    if (!process_message(self)) {
                        disconnect(self);
                        return NULL;
                    }
                } else {
                    cont = false;
                }
            }

        }

        if(true){
            uint64_t val, ret;

            r = read(self->notify, &val, 8);

            if(r == 8){
                if(val & 0x4 && !self->am_choking){
                    uint8_t *packet;
                    size_t len;


                    clear_requests(self);

                    self->am_choking = true;
                    pack_choke(&packet, &len);

                    ret = send(self->sockfd, packet, len, MSG_NOSIGNAL);

                    if(ret == -1){
                        disconnect(self);
                        return NULL;
                    }

                }

                if(val & 0x8 && self->am_choking){
                    uint8_t *packet;
                    size_t len;

                    self->am_choking = false;
                    pack_unchoke(&packet, &len);

                    ret = send(self->sockfd, packet, len, MSG_NOSIGNAL);

                    if(ret == -1){
                        disconnect(self);
                        return NULL;
                    }

                }

                if(self->piece_downloading || pieceset_done()){
                    int i = pieceset_has_new(self->piece_map);
                    if(i && !self->am_interested && !pieceset_done()){
                        if(!send_am_interested(self)){
                            disconnect(self);
                            return NULL;
                        }

                    }
                }

                for(unsigned int j = 0; j < pieces_count && new_pieces; j++){
                    if(!self->piece_map[new_pieces[j]]){
                        uint8_t *packet;
                        size_t len;
                        pack_have(new_pieces[j], &packet, &len);


                        ret = send(self->sockfd, packet, len, MSG_NOSIGNAL);

                        free(packet);

                        if(ret == -1){
                            disconnect(self);
                            return NULL;
                        }
                    }
                }
            }

        }



        ret = request_block(self);

        if (!ret) {
            disconnect(self);
            return NULL;
        }

        ret = reply_in_requests(self);

        if (!ret) {
            disconnect(self);
            return NULL;
        }

        if ((self->requests_pending == 0 || pieceset_done()) && self->am_interested) {
            uint64_t len;
            int i = pieceset_has_new(self->piece_map);
            //trace_log("Checking if no longer interested %p", self->piece_downloading);
            if (!i || pieceset_done()) {
                uint8_t *packet;
                pack_not_interested(&packet, &len);

                ret = send(self->sockfd, packet, len, MSG_NOSIGNAL);


                if (ret == -1) {
                    disconnect(self);
                    return NULL;
                }

                clear_requests(self);
                update_send_timestamp(self);
                self->am_interested = false;
                self->endgame = false;
                self->endgame_last_block = NULL;
            }
        }

        if (!self->endgame && self->am_interested && !self->peer_choking && /*!pieceset_done() &&*/ !pieceset_blocks_missing()) {
            info_log("Entering endgame");
            self->endgame = true;
        }

        struct timespec cur;
        clock_gettime(CLOCK_REALTIME, &cur);
        if ((cur.tv_sec - self->last_recv.tv_sec) >= 120) {
            warn_log("Peer Timed out");
            disconnect(self);
            return NULL;
        } else if ((cur.tv_sec - self->last_recv.tv_sec) >= 30) {
            uint8_t packet[] = {0, 0, 0, 0};

            ret = send(self->sockfd, packet, 4, MSG_NOSIGNAL);

            if (ret == -1) {
                disconnect(self);
                return NULL;
            }

            update_send_timestamp(self);
        }
        //nanosleep(&time, NULL);

    }

}


