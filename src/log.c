#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "log.h"

static void generic_buffer_dump(FILE *stream, uint8_t *buffer, size_t len) {
    size_t printed_chars = 0;
    while (printed_chars < len) {
        if (printed_chars != 0) {
            if (printed_chars % 16 == 0) {
                fprintf(stream, TXT_FMT_DIM "\n   %5lu:  " TXT_ALL_RESET, (unsigned long)(printed_chars));
            } else if (printed_chars % 4 == 0) {
                fprintf(stream, "  ");
            }
        } else {
            fprintf(stream, TXT_FMT_DIM "   %5d:  " TXT_ALL_RESET, 0);
        }
        if (buffer[printed_chars] == 0x00) {
            fprintf(stream, TXT_COLOR_GREEN "%02x " TXT_ALL_RESET, buffer[printed_chars]);
        } else if (buffer[printed_chars] == 0xff) {
            fprintf(stream, TXT_COLOR_RED "%02x " TXT_ALL_RESET, buffer[printed_chars]);
        } else {
            fprintf(stream, "%02x ", buffer[printed_chars]);
        }
        printed_chars++;
    }

    fprintf(stream, "\n");
}

static void print_hex(FILE *stream, uint8_t *buffer, size_t len) {
    size_t printed_bytes = 0;
    while (printed_bytes < len) {
        fprintf(stream, "%02x", buffer[printed_bytes]);
        printed_bytes++;
    }
}

static bool use_file(const char *filename) {
    char *blacklist_files[] = LOG_BLACKLIST_FILES;
    size_t len = sizeof(blacklist_files) / sizeof(void *);

    for (size_t i = 0; i < len; i++) {
        if (strcmp(blacklist_files[i], filename) == 0) {
            return false;
        }
    }
    return true;
}

void trace_detailed_log(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...) {
    if (TRACE && use_file(file)) {
        va_list args;
        va_start(args, format_str);

        _LOGGING_PRINT_PREFIX(LOG_OUTPUT_STREAM, "TRACE", TRACE_COLOR, func_name, file, line_num);
        vfprintf(LOG_OUTPUT_STREAM, format_str, args);
        fprintf(LOG_OUTPUT_STREAM, "\n");

        va_end(args);
    }
}

void trace_detailed_buffer_dump(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len, const char *format_str, ...) {
    if (TRACE && use_file(file)) {

        _LOGGING_PRINT_PREFIX(LOG_OUTPUT_STREAM, "TRACE", TRACE_COLOR, func_name, file, line_num);
        if (format_str) {
            va_list args;
            va_start(args, format_str);
            vfprintf(LOG_OUTPUT_STREAM, format_str, args);
            va_end(args);
        }
        fprintf(LOG_OUTPUT_STREAM, "\n\n");
        generic_buffer_dump(LOG_OUTPUT_STREAM, (uint8_t *) buffer, len);
        fprintf(LOG_OUTPUT_STREAM, "\n");

    }
}

void debug_detailed_log(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...) {
    if (DEBUG && use_file(file)) {
        va_list args;
        va_start(args, format_str);

        _LOGGING_PRINT_PREFIX(LOG_OUTPUT_STREAM, "DEBUG", DEBUG_COLOR, func_name, file, line_num);
        vfprintf(LOG_OUTPUT_STREAM, format_str, args);
        fprintf(LOG_OUTPUT_STREAM, "\n");

        va_end(args);
    }
}


void debug_detailed_buffer_dump(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len, const char *format_str, ...) {
    if (DEBUG && use_file(file)) {

        _LOGGING_PRINT_PREFIX(LOG_OUTPUT_STREAM, "DEBUG", DEBUG_COLOR, func_name, file, line_num);
        if (format_str) {
            va_list args;
            va_start(args, format_str);
            vfprintf(LOG_OUTPUT_STREAM, format_str, args);
            va_end(args);
        }
        fprintf(LOG_OUTPUT_STREAM, "\n\n");
        generic_buffer_dump(LOG_OUTPUT_STREAM, (uint8_t *) buffer, len);
        fprintf(LOG_OUTPUT_STREAM, "\n");

    }
}

void verbose_detailed_log(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...) {
    if (VERBOSE && use_file(file)) {
        va_list args;
        va_start(args, format_str);

        _LOGGING_PRINT_PREFIX(LOG_OUTPUT_STREAM, "VERBOSE", VERBOSE_COLOR, func_name, file, line_num);
        vfprintf(LOG_OUTPUT_STREAM, format_str, args);
        fprintf(LOG_OUTPUT_STREAM, "\n");

        va_end(args);
    }
}


void verbose_detailed_buffer_dump(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len, const char *format_str, ...) {
    if (VERBOSE && use_file(file)) {

        _LOGGING_PRINT_PREFIX(LOG_OUTPUT_STREAM, "VERBOSE", VERBOSE_COLOR, func_name, file, line_num);
        if (format_str) {
            va_list args;
            va_start(args, format_str);
            vfprintf(LOG_OUTPUT_STREAM, format_str, args);
            va_end(args);
        }
        fprintf(LOG_OUTPUT_STREAM, "\n\n");
        generic_buffer_dump(LOG_OUTPUT_STREAM, (uint8_t *) buffer, len);
        fprintf(LOG_OUTPUT_STREAM, "\n");

    }
}

void info_detailed_log(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...) {
    if (INFO && use_file(file)) {
        va_list args;
        va_start(args, format_str);

        _LOGGING_PRINT_PREFIX(LOG_OUTPUT_STREAM, "INFO", INFO_COLOR, func_name, file, line_num);
        vfprintf(LOG_OUTPUT_STREAM, format_str, args);
        fprintf(LOG_OUTPUT_STREAM, "\n");

        va_end(args);
    }
}

void info_detailed_buffer_dump(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len, const char *format_str, ...) {
    if (INFO && use_file(file)) {

        _LOGGING_PRINT_PREFIX(LOG_OUTPUT_STREAM, "INFO", INFO_COLOR, func_name, file, line_num);
        if (format_str) {
            va_list args;
            va_start(args, format_str);
            vfprintf(LOG_OUTPUT_STREAM, format_str, args);
            va_end(args);
        }
        fprintf(LOG_OUTPUT_STREAM, "\n\n");
        generic_buffer_dump(LOG_OUTPUT_STREAM, (uint8_t *) buffer, len);
        fprintf(LOG_OUTPUT_STREAM, "\n");

    }
}

void warn_detailed_log(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...) {
    if (WARN) {
        va_list args;
        va_start(args, format_str);

        _LOGGING_PRINT_PREFIX(LOG_ERROR_STREAM, "WARN", WARN_COLOR, func_name, file, line_num);
        fprintf(LOG_ERROR_STREAM, ERRNO_COLOR "(ERRNO: %s) " TXT_ALL_RESET, strerror(errno));
        vfprintf(LOG_ERROR_STREAM, format_str, args);
        fprintf(LOG_ERROR_STREAM, "\n");

        va_end(args);
    }
}

void warn_detailed_buffer_dump(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len, const char *format_str, ...) {
    if (WARN) {

        _LOGGING_PRINT_PREFIX(LOG_ERROR_STREAM, "WARN", WARN_COLOR, func_name, file, line_num);
        if (format_str) {
            va_list args;
            va_start(args, format_str);
            vfprintf(LOG_ERROR_STREAM, format_str, args);
            va_end(args);
        }
        fprintf(LOG_ERROR_STREAM, "\n\n");
        generic_buffer_dump(LOG_ERROR_STREAM, (uint8_t *) buffer, len);
        fprintf(LOG_ERROR_STREAM, "\n");

    }
}


void error_detailed_log(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...) {
    if (ERROR) {
        va_list args;
        va_start(args, format_str);

        _LOGGING_PRINT_PREFIX(LOG_ERROR_STREAM, "ERROR", ERROR_COLOR, func_name, file, line_num);
        fprintf(LOG_ERROR_STREAM, ERRNO_COLOR "(ERRNO: %s) " TXT_ALL_RESET, strerror(errno));
        vfprintf(LOG_ERROR_STREAM, format_str, args);
        fprintf(LOG_ERROR_STREAM, "\n");

        va_end(args);
    }
}

void error_detailed_buffer_dump(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len, const char *format_str, ...) {
    if (ERROR) {

        _LOGGING_PRINT_PREFIX(LOG_ERROR_STREAM, "ERROR", ERROR_COLOR, func_name, file, line_num);
        if (format_str) {
            va_list args;
            va_start(args, format_str);
            vfprintf(LOG_ERROR_STREAM, format_str, args);
            va_end(args);
        }
        fprintf(LOG_ERROR_STREAM, "\n\n");
        generic_buffer_dump(LOG_ERROR_STREAM, (uint8_t *) buffer, len);
        fprintf(LOG_ERROR_STREAM, "\n");

    }
}

void error_detailed_exit(const char *func_name, const char *file, uint64_t line_num, int exit_code, const char *format_str, ...) {
    if (ERROR) {
        va_list args;
        va_start(args, format_str);

        _LOGGING_PRINT_PREFIX(LOG_ERROR_STREAM, "ERROR", ERROR_COLOR, func_name, file, line_num);
        fprintf(LOG_ERROR_STREAM, ERRNO_COLOR "(ERRNO: %s) " TXT_ALL_RESET, strerror(errno));
        vfprintf(LOG_ERROR_STREAM, format_str, args);
        fprintf(LOG_ERROR_STREAM, "\n");

        va_end(args);
    }
    exit(exit_code);
}

/***********************/
/* Raw Print Functions */
/***********************/

void trace_detailed_print(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...) {
    _LOGGING_LOCATION_PREFIX(LOGPRINT_OUTPUT_STREAM, func_name, file, line_num);
    if (TRACE && use_file(file)) {
        va_list args;
        va_start(args, format_str);
        vfprintf(LOGPRINT_OUTPUT_STREAM, format_str, args);
        va_end(args);
    }
}

void debug_detailed_print(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...) {
    _LOGGING_LOCATION_PREFIX(LOGPRINT_OUTPUT_STREAM, func_name, file, line_num);
    if (DEBUG && use_file(file)) {
        va_list args;
        va_start(args, format_str);
        vfprintf(LOGPRINT_OUTPUT_STREAM, format_str, args);
        va_end(args);
    }
}

void verbose_detailed_print(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...) {
    _LOGGING_LOCATION_PREFIX(LOGPRINT_OUTPUT_STREAM, func_name, file, line_num);
    if (VERBOSE && use_file(file)) {
        va_list args;
        va_start(args, format_str);
        vfprintf(LOGPRINT_OUTPUT_STREAM, format_str, args);
        va_end(args);
    }
}

void info_detailed_print(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...) {
    _LOGGING_LOCATION_PREFIX(LOGPRINT_OUTPUT_STREAM, func_name, file, line_num);
    if (INFO && use_file(file)) {
        va_list args;
        va_start(args, format_str);
        vfprintf(LOGPRINT_OUTPUT_STREAM, format_str, args);
        va_end(args);

    }
}
void warn_detailed_print(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...) {
    _LOGGING_LOCATION_PREFIX(LOGPRINT_ERROR_STREAM, func_name, file, line_num);
    if (WARN) {
        va_list args;
        va_start(args, format_str);
        vfprintf(LOGPRINT_ERROR_STREAM, format_str, args);
        va_end(args);
    }
}
void error_detailed_print(const char *func_name, const char *file, uint64_t line_num, const char *format_str, ...) {
    _LOGGING_LOCATION_PREFIX(LOGPRINT_ERROR_STREAM, func_name, file, line_num);
    if (ERROR) {
        va_list args;
        va_start(args, format_str);
        vfprintf(LOGPRINT_ERROR_STREAM, format_str, args);
        va_end(args);
    }
}

/***************************/
/* Raw Hex Print Functions */
/***************************/

void trace_detailed_hexprint(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len) {
    _LOGGING_LOCATION_PREFIX(LOGPRINT_OUTPUT_STREAM, func_name, file, line_num);
    if (TRACE && use_file(file)) {
        print_hex(LOGPRINT_OUTPUT_STREAM, buffer, len);
    }
}

void debug_detailed_hexprint(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len) {
    _LOGGING_LOCATION_PREFIX(LOGPRINT_OUTPUT_STREAM, func_name, file, line_num);
    if (DEBUG && use_file(file)) {
        print_hex(LOGPRINT_OUTPUT_STREAM, buffer, len);
    }
}

void verbose_detailed_hexprint(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len) {
    _LOGGING_LOCATION_PREFIX(LOGPRINT_OUTPUT_STREAM, func_name, file, line_num);
    if (VERBOSE && use_file(file)) {
        print_hex(LOGPRINT_OUTPUT_STREAM, buffer, len);
    }
}

void info_detailed_hexprint(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len) {
    _LOGGING_LOCATION_PREFIX(LOGPRINT_OUTPUT_STREAM, func_name, file, line_num);
    if (INFO && use_file(file)) {
        print_hex(LOGPRINT_OUTPUT_STREAM, buffer, len);
    }
}
void warn_detailed_hexprint(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len) {
    _LOGGING_LOCATION_PREFIX(LOGPRINT_ERROR_STREAM, func_name, file, line_num);
    if (WARN) {
        print_hex(LOGPRINT_ERROR_STREAM, buffer, len);
    }
}
void error_detailed_hexprint(const char *func_name, const char *file, uint64_t line_num, void *buffer, size_t len) {
    _LOGGING_LOCATION_PREFIX(LOGPRINT_ERROR_STREAM, func_name, file, line_num);
    if (ERROR) {
        print_hex(LOGPRINT_ERROR_STREAM, buffer, len);
    }
}