#include "piece_set.h"

#include <string.h>
#include <errno.h>

#include "common.h"
#include "fileio.h"
#include "metafile_parse.h"
#include "util.h"
#include "log.h"
#include "thread_registry.h"
#include "client.h"

static struct file_piece_set piece_set = {0};

/**************************/
/* PRIVATE LOCK FUNCTIONS */
/**************************/

#define piece_rdlock(piece) do { /*trace_log("READ LOCK %d", piece->piece_index);*/ pthread_rwlock_rdlock(&(piece)->lock); } while (0)
#define piece_wrlock(piece) do { /*trace_log("WRITE LOCK %d", piece->piece_index);*/ pthread_rwlock_wrlock(&(piece)->lock); } while (0)
#define piece_unlock(piece) do { /*trace_log("UNLOCK %d", piece->piece_index);*/ pthread_rwlock_unlock(&(piece)->lock); } while (0)

#define pieceset_rdlock() do { pthread_rwlock_rdlock(&piece_set.lock); } while(0)
#define pieceset_wrlock() do { pthread_rwlock_wrlock(&piece_set.lock); } while(0)
#define pieceset_unlock() do { pthread_rwlock_unlock(&piece_set.lock); } while(0)

/**************************/
/* PRIVATE INIT FUNCTIONS */
/**************************/

static void pieceset_calc_sizes() {
    if (piece_set.pieces) {
        for (uint32_t piece_i = 0; piece_i < piece_set.piece_count; piece_i++) {
            struct piece_data *piece = piece_set.pieces + piece_i;

            piece->piece_index = piece_i;

            if (piece_i + 1 == piece_set.piece_count) {
                /* Piece is the last piece, so size may be smaller than the piece size */
                piece->len = piece_set.file_size - (piece_set.piece_size * (piece_set.piece_count - 1));

            } else {
                piece->len = piece_set.piece_size;
            }
        }
    } else {
        error_log("Initialize the piece set fully before trying to load hashes");
    }
}

static bool pieceset_init_blocks() {
    if (piece_set.pieces) {
        for (uint32_t piece_i = 0; piece_i < piece_set.piece_count; piece_i++) {
            struct piece_data *piece = piece_set.pieces + piece_i;
            piece->blocks = NULL;

            // Use integer division to get the number of blocks
            piece->block_count = (piece->len + piece_set.block_size - 1) / piece_set.block_size;
        }

        bool initialized = true;
        for (uint32_t piece_i = 0; piece_i < piece_set.piece_count; piece_i++) {
            struct piece_data *piece = piece_set.pieces + piece_i;
            // piece->blocks = ;
            piece->blocks = calloc(piece->block_count, sizeof(struct block_data));
            if (!piece->blocks) {
                initialized = false;
                break;
            }

            /* Initialize each block */
            for (uint32_t block_i = 0; block_i < piece->block_count; block_i++) {
                struct block_data *block = piece->blocks + block_i;

                block->offset = block_i * piece_set.block_size;
                block->status = BLOCK_MISSING;
                block->piece_index = piece_i;
                block->block_index = block_i;

                if (piece->len - block->offset >= piece_set.block_size) {
                    block->len = piece_set.block_size;
                } else {
                    block->len = piece->len - block->offset;
                }
            }
        }

        if (!initialized) {
            /* Out of memory, go back and free the previously allocated blocks */
            for (uint32_t piece_i = 0; piece_i < piece_set.piece_count; piece_i++) {
                struct piece_data *piece = piece_set.pieces + piece_i;
                free(piece->blocks);
                piece->block_count = 0;
                piece->blocks = NULL;
            }
            return false;
        }
    } else {
        error_log("Initialize the piece set fully before trying to initialize the blocks");
        return false;
    }
    return true;
}

static void pieceset_load_hashes() {
    if (piece_set.pieces) {
        for (uint32_t piece_i = 0; piece_i < piece_set.piece_count; piece_i++) {
            struct piece_data *piece = piece_set.pieces + piece_i;
            memcpy(piece->hash, g_metafile->info.pieces[piece_i].hash, SHA1_SIZE);
        }
    } else {
        error_log("Initialize the piece set fully before trying to load hashes");
    }
}

static bool pieceset_init_locks() {
    if (piece_set.pieces) {
        uint32_t initialized_pieces = 0;
        bool success = true;
        for (uint32_t piece_i = 0; piece_i < piece_set.piece_count; piece_i++) {
            struct piece_data *piece = piece_set.pieces + piece_i;
            if (pthread_rwlock_init(&piece->lock, NULL) != 0) {
                success = false;
                break;
            }
            initialized_pieces++;
        }

        if (success) {
            success = (pthread_rwlock_init(&piece_set.lock, NULL) == 0);
        }

        if (!success) {
            for (uint32_t piece_i = 0; piece_i < initialized_pieces; piece_i++) {
                struct piece_data *piece = piece_set.pieces + piece_i;
                pthread_rwlock_destroy(&piece->lock);
            }
            return false;
        }

        return true;
    } else {
        error_log("Initialize the piece set fully before trying to load hashes");
        return false;
    }
}

static const char *pieceset_piece_status(enum piece_status status) {
    switch (status) {
        case PIECE_MISSING:
            return "PIECE_MISSING";
        case PIECE_DOWNLOADING:
            return "PIECE_DOWNLOADING";
        case PIECE_FULLY_REQUESTED:
            return "PIECE_FULLY_REQUESTED";
        case PIECE_STORED:
            return "PIECE_STORED";
    }
    return "<PIECE STATUS UNKNOWN>";
}

static const char *pieceset_block_status(enum block_status status) {
    switch (status) {
        case BLOCK_MISSING:
            return "BLOCK_MISSING";
        case BLOCK_REQUESTED:
            return "BLOCK_REQUESTED";
        case BLOCK_STORED:
            return "BLOCK_STORED";
    }
    return "<BLOCK STATUS UNKNOWN>";
}

/*************************/
/* SEMI-PUBLIC FUNCTIONS */
/*************************/

/* These functions are not defined in the header, but are non-static for testing purposes */

/**
 * Calculate the block offset and size for a particular block in a piece
 * @param  piece_index : the zero based index of the piece this block belongs to
 * @param  block_index : the zero based index of the block to get the offset of
 * @return             : true or false depending on if the struct was popultaed
 */
struct block_data *pieceset_block(uint32_t piece_index, uint32_t block_index) {
    struct piece_data *piece = pieceset_piece(piece_index);
    if (!piece) {
        warn_log("Piece not found");
        return NULL;
    }

    if (block_index >= piece->block_count) {
        warn_log("Block index out of bounds");
    }

    return piece->blocks + block_index;
}

struct piece_data *piece_next_random(struct peer *peer, bool endgame) {
    bool *piece_map = peer->piece_map;

    srand(time(NULL));

    uint32_t piece_start_i = rand_range(0, piece_set.piece_count - 1);
    uint32_t piece_i = (piece_start_i + 1) % piece_set.piece_count;


    bool (*available_blocks)(struct piece_data * piece) = (endgame) ? pieceset_endgame_available_blocks : pieceset_available_blocks;

    while (piece_i != piece_start_i) {
        struct piece_data *piece = piece_set.pieces + piece_i;
        if (piece_map[piece_i] && available_blocks(piece)) {
            return piece;
        }

        piece_i++;
        piece_i %= piece_set.piece_count;
    }

    // bool *piece_map = peer->piece_map;

    // srand(time(NULL));

    // uint32_t piece_start_i = rand_range(0, piece_set.piece_count - 1);
    // uint32_t piece_i = (piece_start_i + 1) % piece_set.piece_count;


    // bool (*available_blocks)(struct piece_data * piece) = (endgame) ? pieceset_endgame_available_blocks : pieceset_available_blocks;

    // uint32_t valid_pieces = 0;
    // while (piece_i != piece_start_i) {
    //     struct piece_data *piece = piece_set.pieces + piece_i;
    //     if (piece_map[piece_i] && available_blocks(piece)) {
    //         valid_pieces++;
    //     }

    //     piece_i++;
    //     piece_i %= piece_set.piece_count;
    // }

    // uint32_t offset = rand_range(0, valid_pieces / 2);
    // piece_i = (piece_start_i + 1) % piece_set.piece_count;
    // while (piece_i != piece_start_i) {
    //     struct piece_data *piece = piece_set.pieces + piece_i;
    //     if (piece_map[piece_i] && available_blocks(piece)) {
    //         if (offset == 0) {
    //             return piece;
    //         }
    //         offset--;
    //     }

    //     piece_i++;
    //     piece_i %= piece_set.piece_count;
    // }

    // info_log("Peer lacks pieces we are interested in");
    return NULL;
}

struct piece_data *piece_next_rare(struct peer *peer) {
    bool *piece_map = peer->piece_map;

    uint32_t min_rarity = 0;
    struct piece_data *best_piece = NULL;
    for (uint32_t piece_i = 0; piece_i < piece_set.piece_count; piece_i++) {
        struct piece_data *piece = piece_set.pieces + piece_i;

        if (piece_map[piece_i] && pieceset_available_blocks(piece)) {
            if ((piece_set.piece_rarity[piece_i] < min_rarity || min_rarity == 0) && piece_set.piece_rarity[piece_i] != 0) {
                min_rarity = piece_set.piece_rarity[piece_i];
                best_piece = piece;
            }
        }
    }

    // if (min_rarity) {
    //     info_log("Best piece has a rarity of %u", min_rarity);
    // }
    return best_piece;
}

/********************/
/* PUBLIC FUNCTIONS */
/********************/

/**
 * Get a piece from the piece set
 * @param  piece_index : index of the piece to get
 * @return             : a pointer to the piece_data struct or NULL if it doesn't exist
 */
struct piece_data *pieceset_piece(uint32_t piece_index) {
    if (piece_index < piece_set.piece_count) {
        return piece_set.pieces + piece_index;
    } else {
        warn_log("Piece index out of bounds");
    }
    return NULL;
}

void pieceset_print_piece(uint32_t piece_index) {
    struct piece_data *piece = pieceset_piece(piece_index);
    if (piece) {
        info_print("PIECE %u/%u - %s - Rarity %u - %u bytes - ", piece_index, piece_set.piece_count, pieceset_piece_status(piece->status), piece_set.piece_rarity[piece_index], piece->len);
        info_hex_print(piece->hash, SHA1_SIZE);
        info_print("\n");
    }
}

void pieceset_print_pieces() {
    if (piece_set.pieces) {
        for (uint32_t i = 0; i < piece_set.piece_count; i++) {
            pieceset_print_piece(i);
        }
    }
}

void pieceset_print_block(uint32_t piece_index, uint32_t block_index) {
    struct piece_data *piece = pieceset_piece(piece_index);
    struct block_data *block = pieceset_block(piece_index, block_index);
    if (piece && block) {
        info_print("BLOCK %u/%u (PIECE %u/%u) - %s - OFFSET %u bytes - LEN %u bytes - REQ %u\n",
                   block->block_index, piece->block_count, block->piece_index, piece_set.piece_count,
                   pieceset_block_status(block->status), block->offset, block->len, block->pending_requests);
    }
}

void pieceset_print_blocks(uint32_t piece_index) {
    struct piece_data *piece = pieceset_piece(piece_index);
    if (piece) {
        for (uint32_t i = 0; i < piece->block_count; i++) {
            pieceset_print_block(piece_index, i);
        }
    }
}

void pieceset_print_all() {
    if (piece_set.pieces) {
        for (uint32_t i = 0; i < piece_set.piece_count; i++) {
            pieceset_print_piece(i);
            struct piece_data *piece = pieceset_piece(i);
            if (piece) {
                for (uint32_t j = 0; j < piece->block_count; j++) {
                    info_print("   ");
                    pieceset_print_block(i, j);
                }
            }
        }
    }
}

void pieceset_print_stats() {
    info_print("=-=-= PIECE SET =-=-=\n");
    info_print("  File Size        : %u bytes\n", piece_set.file_size);
    info_print("  Piece Size       : %u bytes\n", piece_set.piece_size);
    info_print("  Block Size       : %u bytes\n", piece_set.block_size);
    info_print("  Total Pieces     : %u pieces\n", piece_set.piece_count);
    info_print("  Missing Pieces   : %u pieces\n", piece_set.missing_pieces);
    info_print("  Completed Pieces : %u pieces\n", piece_set.completed_pieces);
}

bool pieceset_init(const char *filepath, size_t file_size, uint32_t piece_count, size_t piece_size, size_t block_size, bool random_selection) {
    if (block_size > DEFAULT_BLOCK_SIZE || block_size > MAX_ALLOWED_BLOCK_SIZE) {
        warn_log("Block size must be less than or equal to the default and the max");
        return false;
    }

    piece_set.filepath = filepath;
    piece_set.file_size = file_size;
    piece_set.piece_count = 0;
    piece_set.piece_size = piece_size;
    piece_set.block_size = block_size;
    piece_set.completed_pieces = 0;
    piece_set.missing_pieces = 0;
    piece_set.use_random = random_selection;

    if (random_selection) {
        info_log("Using random piece selection");
    } else {
        info_log("Using rarest first piece selection");
    }

    /* Validate that the filesize, piece_size, and piece_count are consistent with each other */
    size_t max_filesize = piece_count * piece_size;
    size_t min_filesize = ((piece_count - 1) * piece_size) + 1;
    if (!(min_filesize <= file_size && file_size <= max_filesize)) {
        warn_log("The filesize (%u), piece_size (%u), and piece_count(%u) are not consistent", file_size, piece_size, piece_count);
        return false;
    }

    piece_set.pieces = calloc(piece_count, sizeof(struct piece_data));
    if (!piece_set.pieces) {
        mem_errlog();
        return false;
    }

    piece_set.piece_rarity = calloc(piece_count, sizeof(uint32_t));
    if (!piece_set.piece_rarity) {
        mem_errlog();
        free(piece_set.pieces);
        return false;
    }

    piece_set.piece_count = piece_count;
    piece_set.missing_pieces = piece_count;

    if (!pieceset_init_locks()) {
        error_log("Failed to init locks for all the pieces");
        free(piece_set.pieces);
        return false;
    }

    pieceset_calc_sizes();
    pieceset_init_blocks();
    pieceset_load_hashes();

    if (!fileio_init(filepath, file_size, piece_size)) {
        error_log("Failed to create file");
        return false;
    }

    return true;
}

bool pieceset_init_from_metafile(const char *filepath, bool random_selection) {
    return pieceset_init(filepath, get_filesize(g_metafile), g_metafile->info.piece_count, g_metafile->info.piece_length, DEFAULT_BLOCK_SIZE, random_selection);
}

bool pieceset_init_for_seeding(const char *filepath, bool random_selection) {
    if (!pieceset_init(filepath, get_filesize(g_metafile), g_metafile->info.piece_count, g_metafile->info.piece_length, DEFAULT_BLOCK_SIZE, random_selection)) {
        error_log("Failed to init piece set for seeding");
        return false;
    }


    for (uint32_t piece_i = 0; piece_i < piece_set.piece_count; piece_i++) {
        struct piece_data *piece = piece_set.pieces + piece_i;
        piece_rdlock(piece);
        if (!fileio_verify_piece(piece_i, piece->len, piece->hash)) {
            piece_unlock(piece);
            error_log("File has an invalid piece (%u), cannot be used for seeding", piece_i);
            pieceset_destroy();
            return false;
        }
        piece->status = PIECE_STORED;
        piece->blocks_completed = piece->block_count;
        piece->blocks_requested = piece->block_count;
        for (uint32_t block_i = 0; block_i < piece->block_count; block_i++) {
            struct block_data *block = piece->blocks + block_i;
            block->status = BLOCK_STORED;
        }
        piece_unlock(piece);
    }

    pieceset_wrlock();
    piece_set.completed_pieces = piece_set.piece_count;
    piece_set.missing_pieces = 0;
    pieceset_unlock();

    return true;
}

void pieceset_destroy() {
    if (piece_set.pieces) {
        pthread_rwlock_destroy(&piece_set.lock);
        for (uint32_t i = 0; i < piece_set.piece_count; i++) {
            struct piece_data *piece = pieceset_piece(i);
            pthread_rwlock_destroy(&piece->lock);
            free(piece->blocks);
            piece->blocks = NULL;
            piece->block_count = 0;
        }
    }
    free(piece_set.pieces);
    piece_set.pieces = NULL;

    free(piece_set.piece_rarity);
    piece_set.piece_rarity = NULL;

    fileio_destroy();
}

bool pieceset_generate_bitfield(uint8_t **bitfield) {
    uint8_t *tmp = calloc(piece_bitfield_size(), 1);
    if (!tmp) {
        mem_errlog();
        return false;
    }

    struct piece_data *piece = NULL;
    for (size_t i = 0; i < piece_set.piece_count; i++) {
        piece = piece_set.pieces + i;
        size_t byte_offset = i / 8;
        uint8_t bit_offset = i % 8;

        piece_rdlock(piece);
        switch (piece->status) {
            case PIECE_MISSING:
            case PIECE_DOWNLOADING:
            case PIECE_FULLY_REQUESTED:
                /* Nothing needs to be done sincce the buffer is already zeroed to begin with */
                break;
            case PIECE_STORED:
                tmp[byte_offset] += 1 << (7 - bit_offset);
                break;
        }
        piece_unlock(piece);
    }

    *bitfield = tmp;
    return true;
}

bool pieceset_generate_piecemap(bool **piece_map) {
    bool *tmp = calloc(piece_map_size(), 1);
    if (!tmp) {
        mem_errlog();
        return false;
    }

    struct piece_data *piece = NULL;
    for (size_t i = 0; i < piece_set.piece_count; i++) {
        piece = piece_set.pieces + i;

        piece_rdlock(piece);
        switch (piece->status) {
            case PIECE_MISSING:
            case PIECE_DOWNLOADING:
            case PIECE_FULLY_REQUESTED:
                /* Nothing needs to be done sincce the buffer is already zeroed to begin with */
                break;
            case PIECE_STORED:
                tmp[i] = true;
                break;
        }
        piece_unlock(piece);
    }

    *piece_map = tmp;
    return true;
}

bool pieceset_available_blocks(struct piece_data *piece) {
    if (piece) {
        switch (piece->status) {
            case PIECE_STORED:
            case PIECE_FULLY_REQUESTED:
                return false;
            case PIECE_MISSING:
            case PIECE_DOWNLOADING:
                return true;
        }
    }
    return false;
}

bool pieceset_endgame_available_blocks(struct piece_data *piece) {
    if (piece) {
        switch (piece->status) {
            case PIECE_STORED:
                return false;
            case PIECE_FULLY_REQUESTED:
            case PIECE_MISSING:
            case PIECE_DOWNLOADING:
                return true;
        }
    }
    return false;
}

struct block_data *pieceset_next_block(struct piece_data *piece) {
    if (piece) {

        piece_rdlock(piece);
        switch (piece->status) {
            case PIECE_STORED:
                // warn_log("Piece already stored");
                piece_unlock(piece);
                return NULL;
            case PIECE_FULLY_REQUESTED:
                piece_unlock(piece);
                return NULL;
            case PIECE_DOWNLOADING:
                break;
            case PIECE_MISSING:
                piece_unlock(piece);
                piece_wrlock(piece);
                pieceset_wrlock();

                /* Check inside the lock to be sure */
                if (piece->status == PIECE_MISSING) {
                    piece_set.missing_pieces--;
                }

                piece->status = PIECE_DOWNLOADING;

                pieceset_unlock();
                piece_unlock(piece);
                piece_rdlock(piece);
        }

        /* Return the first block that is missing */

        for (uint32_t block_i = 0; block_i < piece->block_count; block_i++) {
            struct block_data *block = &piece->blocks[block_i];

            /* If this block is marked as missing, this should be our next block */
            if (block->status == BLOCK_MISSING) {
                piece_unlock(piece);
                piece_wrlock(piece);

                /* Check again inside the lock to be sure */
                if (block->status == BLOCK_MISSING) {
                    block->status = BLOCK_REQUESTED;
                    piece->blocks_requested++;
                    block->pending_requests++;

                    if (piece->blocks_requested == piece->block_count) {
                        piece->status = PIECE_FULLY_REQUESTED;
                    }
                }

                piece_unlock(piece);
                return block;
            }
        }

        piece_unlock(piece);

    } else {
        error_log("Piece %d not found", piece->piece_index);
    }
    return NULL;
}

struct block_data *pieceset_endgame_next_block(struct piece_data *piece, struct block_data *last_block) {
    errno = 0;
    if (piece) {
        piece_rdlock(piece);
        switch (piece->status) {
            case PIECE_STORED:
                // warn_log("Piece already stored");
                piece_unlock(piece);
                return NULL;
            case PIECE_FULLY_REQUESTED:
            case PIECE_DOWNLOADING:
                break;
            case PIECE_MISSING:
                piece_unlock(piece);
                piece_wrlock(piece);
                pieceset_wrlock();

                /* Check inside the lock to be sure */
                if (piece->status == PIECE_MISSING) {
                    piece_set.missing_pieces--;
                }

                piece->status = PIECE_DOWNLOADING;

                pieceset_unlock();
                piece_unlock(piece);
                piece_rdlock(piece);
        }

        /* Return the first block that is missing */
        uint32_t start_index = 0;
        if (last_block) {
            start_index = last_block->block_index + 1;
        }


        for (uint32_t block_i = start_index; block_i < piece->block_count; block_i++) {
            struct block_data *block = &piece->blocks[block_i];

            /* If this block is marked as missing, this should be our next block */
            if (block->status != BLOCK_STORED && block->pending_requests < 2 * PEER_LIMIT) {
                piece_unlock(piece);
                piece_wrlock(piece);

                /* Check again inside the lock to be sure */
                /* TODO Make sure we don't issue out the same block thousands of times */
                if (block->status != BLOCK_STORED) {
                    // if (block->pending_requests > PEER_LIMIT) {
                    //     verbose_log(TXT_COLOR_MAGENTA TXT_FMT_BOLD "PENDING REQUESTS:" TXT_ALL_RESET " excessive requests (%u) of block %u of piece %u", block->pending_requests, block_i,  block->piece_index);
                    // }

                    if (block->status != BLOCK_REQUESTED) {
                        /* Block hasn't been requested by anyone else yet */
                        piece->blocks_requested++;
                        block->status = BLOCK_REQUESTED;
                    }

                    block->pending_requests++;

                    if (piece->blocks_requested == piece->block_count) {
                        piece->status = PIECE_FULLY_REQUESTED;
                    }
                }

                piece_unlock(piece);
                return block;
            }
        }

        piece_unlock(piece);

    } else {
        error_log("Piece %d not found", piece->piece_index);
    }
    return NULL;
}

void pieceset_timeout_block(struct block_data *block) {
    if (block) {
        struct piece_data *piece = pieceset_piece(block->piece_index);
        if (piece) {
            piece_wrlock(piece);
            if (block->status != BLOCK_STORED) {
                if (block->pending_requests == 0) {
                    error_log("[CRITICAL] Block %u of piece %u has tried to timeout a block that wan't requested", block->block_index, block->piece_index);
                    piece_unlock(piece);
                    return;
                }
                block->pending_requests--;
                if (block->pending_requests == 0) {
                    block->status = BLOCK_MISSING;
                    piece->blocks_requested--;

                    if (piece->status == PIECE_FULLY_REQUESTED) {
                        piece->status = PIECE_DOWNLOADING;
                    }

                    if (piece->blocks_requested == 0) {
                        pieceset_wrlock();
                        piece->status = PIECE_MISSING;
                        piece_set.missing_pieces++;
                        pieceset_unlock();
                    }
                }
            }
            piece_unlock(piece);
        } else {
            error_log("Piece %d not found", block->piece_index);
        }
    }
}

struct block_data *pieceset_find_block(uint32_t piece_index, uint32_t offset) {
    struct piece_data *piece = pieceset_piece(piece_index);
    if (piece) {
        if (offset % piece_set.block_size == 0) {
            uint32_t block_i = offset / piece_set.block_size;
            return pieceset_block(piece_index, block_i);
        }
    }
    return NULL;
}

void pieceset_peer_has(uint32_t piece_index) {
    pieceset_wrlock();
    piece_set.piece_rarity[piece_index]++;
    pieceset_unlock();
}

void pieceset_peer_bitfield(struct peer *peer_data) {
    uint32_t map_size = piece_map_size();
    peer_rdlock(peer_data);
    pieceset_wrlock();
    for (uint32_t i = 0; i < map_size; i++) {
        if (peer_data->piece_map[i]) {
            piece_set.piece_rarity[i]++;
        }
    }
    pieceset_unlock();
    peer_unlock(peer_data);
}

void pieceset_del_peer(struct peer *peer_data) {
    uint32_t map_size = piece_map_size();
    pieceset_wrlock();
    peer_rdlock(peer_data);
    for (uint32_t i = 0; i < map_size; i++) {
        if (peer_data->piece_map[i]) {
            if (piece_set.piece_rarity[i] != 0) {
                piece_set.piece_rarity[i]--;
            }
        }
    }
    pieceset_unlock();
    peer_unlock(peer_data);
}

void pieceset_write_block(struct block_data *block, void *data) {
    if (pieceset_done()) {
        return;
    }

    errno = 0;
    struct piece_data *piece = pieceset_piece(block->piece_index);
    if (!piece) {
        error_log("Failed to find piece");
        return;
    }

    piece_wrlock(piece);
    if (piece->status == PIECE_STORED) {
        //TODO uncomment this log
        // warn_log("Piece %u already stored", piece->piece_index);
        piece_unlock(piece);
        return;
    }

    if (!fileio_write_block(block->piece_index, block->offset, data, block->len)) {
        warn_log("Failed to sync data to file (SHOULD BE AUTOMATICALLY SYNCED LATER)");
    }

    block->status = BLOCK_STORED;
    piece->blocks_completed++;
    block->pending_requests = 0;

    for (uint32_t block_i = 0; block_i < piece->block_count; block_i++) {
        struct block_data *tmp_block = pieceset_block(block->piece_index, block_i);
        if (tmp_block->status != BLOCK_STORED) {
            piece_unlock(piece);
            return;
        }
    }

    bool verified = fileio_verify_piece(block->piece_index, piece->len, piece->hash);
    if (verified) {
        /* Piece hash is verified as correct, so the piece can be marked as stored */
        /* All blocks have been downloaded */
        pieceset_wrlock();
        if (piece->status != PIECE_STORED) {
            piece->status = PIECE_STORED;
            piece_set.completed_pieces++;
        }
        pieceset_unlock();
        // info_log("Piece %d verified", block->piece_index);
        piece_unlock(piece);

        pieceset_wrlock();
        if (piece_set.piece_count == piece_set.completed_pieces && !piece_set.finalized) {
            thread_start(fileio_finalize, NULL);
        }
        pieceset_unlock();


    } else {
        /* Piece hash was incorrect, so mark all blocks as missing*/
        info_log("Piece %d failed verification, discarding all blocks", block->piece_index);
        for (uint32_t block_i = 0; block_i < piece->block_count; block_i++) {
            struct block_data *tmp_block = pieceset_block(block->piece_index, block_i);
            tmp_block->status = BLOCK_MISSING;

        }
        piece->status = PIECE_MISSING;
        piece_unlock(piece);
        return;

    }
}

bool pieceset_read_block(uint32_t piece_index, size_t offset, void *data, size_t len) {
    struct piece_data *piece = pieceset_piece(piece_index);
    if (!piece) {
        return false;
    }

    piece_rdlock(piece);
    if (piece->status != PIECE_STORED) {
        piece_unlock(piece);
        return false;
    }
    piece_unlock(piece);

    if (offset + len > piece->len) {
        warn_log("Block would extend past the current piece");
        return false;
    }

    piece_rdlock(piece);
    if (!fileio_read_block(piece_index, offset, data, len)) {
        error_log("Failed to read data block");
        piece_unlock(piece);
        return false;
    }

    piece_unlock(piece);
    return true;
}

bool pieceset_has_new(bool *piece_map) {
    for (uint32_t i = 0; i < piece_set.piece_count; i++) {
        struct piece_data *piece = piece_set.pieces + i;
        piece_rdlock(piece);
        if (piece_map[i] && piece->status != PIECE_STORED) {
            piece_unlock(piece);
            return true;
        }
        piece_unlock(piece);
    }
    return false;
}

bool pieceset_endgame_has_new(bool *piece_map) {
    for (uint32_t i = 0; i < piece_set.piece_count; i++) {
        struct piece_data *piece = piece_set.pieces + i;
        piece_rdlock(piece);
        if (piece_map[i] && piece->status != PIECE_STORED) {
            piece_unlock(piece);
            return true;
        }
        piece_unlock(piece);
    }
    return false;
}

struct piece_data *pieceset_select_piece(struct peer *peer) {
    // TODO add a flag for this
    if (piece_set.use_random) {
        return piece_next_random(peer, false);
    } else {
        return piece_next_rare(peer);
    }
}

struct piece_data *pieceset_endgame_select_piece(struct peer *peer) {
    return piece_next_random(peer, true);
}

bool pieceset_blocks_missing() {
    bool done = 0 != piece_set.missing_pieces;
    return done;
}

bool pieceset_done() {
    bool done = piece_set.piece_count == piece_set.completed_pieces;
    return done;
}

uint32_t pieceset_pieces_completed() {
    pieceset_rdlock();
    uint32_t completed = piece_set.completed_pieces;
    pieceset_unlock();
    return completed;
}

struct piece_data *pieceset_find_piece(uint32_t index) {
    return &piece_set.pieces[index];
}

/***************************/
/* Generic Piece Functions */
/***************************/

uint32_t pieceset_piece_size() {
    return piece_set.piece_size;
}

size_t piece_bitfield_size() {
    /* 7 = num bits in a byte - 1, to give us the proper bitfield size with integer division */
    size_t bitfield_size = (piece_set.piece_count + 7) / 8;
    return bitfield_size;
}

size_t piece_map_size() {
    return piece_set.piece_count;
}

uint32_t piece_count() {
    return piece_set.piece_count;
}

bool piece_map_to_bitfield(bool *piece_map, uint8_t **bitfield) {
    size_t bitfield_size = piece_bitfield_size();

    uint8_t *tmp = calloc(bitfield_size, sizeof(uint8_t));
    if (!tmp) {
        mem_errlog();
        return false;
    }

    bool has_piece = false;
    for (size_t i = 0; i < piece_set.piece_count; i++) {
        has_piece = piece_map[i];
        size_t byte_offset = i / 8;
        uint8_t bit_offset = i % 8;

        if (has_piece) {

            tmp[byte_offset] += 1 << (7 - bit_offset);
        }
    }

    *bitfield = tmp;
    return true;
}

bool piece_bitfield_to_map(uint8_t *bitfield, bool **piece_map) {
    /* 7 = num bits in a byte - 1, to give us the proper bitfield size with integer division */
    size_t map_size = piece_map_size();

    bool *tmp = calloc(map_size, sizeof(bool));
    if (!tmp) {
        mem_errlog();
        return false;
    }


    for (size_t i = 0; i < piece_set.piece_count; i++) {
        size_t byte_offset = i / 8;
        uint8_t bit_offset = i % 8;

        tmp[i] += ((bitfield[byte_offset] << (bit_offset)) & 0xff) >> 7;
    }

    *piece_map = tmp;
    return true;
}