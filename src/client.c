#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>

#include "argtable3/argtable3.h"

#include "client.h"
#include "common.h"
#include "log.h"
#include "metafile_parse.h"
#include "tracker.h"
#include "piece_set.h"
#include "command.h"
#include "thread_registry.h"
#include "decider.h"

#include "accept.h"


/*Global args*/
int download_limit = 10000000;
int upload_limit = 10000000;

bool use_propshare = false;

struct arg_lit *help, *print;
struct arg_lit *rand_arg;
struct arg_lit *propshare_arg;
struct arg_file *seed_arg;
struct arg_file *metafile_arg;
struct arg_int *upload_arg, *download_arg;
struct arg_end *end;

char *prompt = "Client: ";

char *commands[] = {
    "Print "
};

struct client *init_client() {
    // Initialize struct to 0
    struct client *out = calloc(sizeof(struct client), 1);
    if (!out) {
        error_log("malloc() failed");
        return NULL;
    }
    // Default port
    out->port = 6881;
    // Seed random
    srand(time(NULL));
    // Build peer id
    char *id_prefix = "TOBY-417-";
    int curr;
    memcpy(out->peer_id, id_prefix, strlen(id_prefix));
    curr = strlen(id_prefix);
    for (curr = strlen(id_prefix); curr < 20; curr++) {
        // Get random digit in ASCII
        char c = (rand() % 10) + 48;
        out->peer_id[curr] = c;
    }
    info_log("Peer ID generated: %.20s", out->peer_id);

    clock_gettime(CLOCK_MONOTONIC, &(out->begin_time));

    return out;
}

int main(int argc, char *argv[]) {

    if (!thread_setup()) {
        error_log("Failed to setup state for the thread registry");
        return 1;
    }

    int num_errors;

    void *argtable[] = {
        help = arg_litn("h", "help", 0, 1, "Display this help and exit"),
        print = arg_litn("p", "print", 0, 2, "Print metafile contents"),
        propshare_arg = arg_litn(NULL, "propshare", 0, 1, "Use Propshare"),
        download_arg = arg_intn("d", "download", "kilobytes/sec", 0, 1, "Download rate limit"),
        upload_arg = arg_intn("u", "upload", "kilobytes/sec", 0, 1, "Upload rate limit"),
        rand_arg = arg_litn(NULL, "random", 0, 1, "Use random selection instead of rarest first"),
        seed_arg = arg_filen("s", "seed", "<file>", 0, 1, "Downloaded file to seed with"),
        metafile_arg = arg_filen(NULL, NULL, "<torrent>", 1, 1, "Torrent file to download"),
        end = arg_end(20)
    };

    num_errors = arg_parse(argc, argv, argtable);

    if (help->count > 0) {
        printf("Usage: %s", argv[0]);
        arg_print_syntax(stdout, argtable, "\n");
        printf("Client to download torrents\n\n");
        arg_print_glossary(stdout, argtable, "  %-25s %s\n");
        exit(0);
    }

    if (num_errors > 0) {
        arg_print_errors(stdout, end, argv[0]);
        printf("Try '%s --help' for more information.\n", argv[0]);
        exit(0);
    }

    if (download_arg->count == 1) {
        download_limit = download_arg->ival[0];
    }

    if (upload_arg->count == 1) {
        upload_limit = upload_arg->ival[0];
    }

    if(propshare_arg->count == 1){
        use_propshare = true;
    }

    FILE *metafile = fopen(metafile_arg->filename[0], "r");
    uint8_t *buf;
    int len, ret;

    if (!metafile) {
        error_log("Error in opening file");
        return  1;
    }

    fseek(metafile, 0L, SEEK_END);
    len = ftell(metafile);
    rewind(metafile);

    buf = calloc(len, 1);

    ret = fread(buf, 1, len, metafile);

    if (ret != len) {
        error_log("Issue with reading");
        return 1;
    }

    g_metafile = parse_metafile(buf, len);

    if (!g_metafile) {
        error_log("Issue with parsing");
        return 1;
    }

    if (print->count > 0) {
        print_metafile(g_metafile, print->count == 2);
        metafile_cleanup(g_metafile);
        exit(0);
    }

    if (seed_arg->count == 0 && !pieceset_init_from_metafile("download.part", rand_arg->count == 1)) {
        error_log("Failed to initialize piece set");
        return 1;
    } else if (seed_arg->count == 1) {
        if (pieceset_init_for_seeding(seed_arg->filename[0], rand_arg->count == 1)) {
            info_print("Starting in seeding mode with file '%s'\n", seed_arg->filename[0]);
        } else {
            metafile_cleanup(g_metafile);
            free(buf);
            return 1;
        }
    }

    global_peers_init();

    g_client_info = init_client();

    if (seed_arg->count == 1) {
        g_client_info->end_time.tv_sec = g_client_info->begin_time.tv_sec;
        g_client_info->end_time.tv_nsec = g_client_info->begin_time.tv_nsec;
    }

    pthread_t tracker_daemon = init_tracker();

    thread_start(decide, NULL);

    begin_accept();

    command_loop();

    thread_cleanup();

    if (pthread_cancel(tracker_daemon) != 0) {
        error_log("failed to perform clean exit for tracker thread during shutdown");
    }
    if (pthread_join(tracker_daemon, NULL) != 0) {
        error_log("failed to join on tracker thread during shutdown");
    }

    global_peers_destroy();
    metafile_cleanup(g_metafile);

    free(buf);
    return 0;
}
