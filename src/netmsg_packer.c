#include "netmsg_packer.h"

#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>

#include "netbuffer.h"
#include "log.h"

/*********************/
/* PACKING FUNCTIONS */
/*********************/

void pack_handshake(uint8_t *info_hash, uint8_t *peer_id, uint8_t **packet, size_t *packet_len) {
    size_t tmp_packet_len = 68;

    if (!packet) {
        warn_log("Packet cannot be NULL");
        return;
    }

    void *tmp_packet = calloc(tmp_packet_len, 1);
    if (!tmp_packet) {
        mem_errlog();
        return;
    }

    static const uint8_t tmp_packet_data[] = {0x13, /* Length of String */
                                              0x42, 0x69, 0x74, 0x54, 0x6f, 0x72, 0x72, 0x65, 0x6e, 0x74, 0x20, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x63, 0x6f, 0x6c, /* Bittorrent Protocol */
                                              0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 /* Reserved bytes of 0s */
                                             };

    struct netbuf nbuf = {0};
    create_network_buffer(&nbuf, tmp_packet, tmp_packet_len);

    /* Push the initialization data */
    push_data(&nbuf, tmp_packet_data, 28);

    /* Push the hash info */
    push_data(&nbuf, info_hash, 20);

    /* Push the peer id */
    push_data(&nbuf, peer_id, 20);

    *packet = tmp_packet;

    if (packet_len) {
        *packet_len = tmp_packet_len;
    }
}

void pack_keep_alive(uint8_t **packet, size_t *packet_len) {
    static const uint8_t data[] = {0x00, 0x00, 0x00, 0x00};
    *packet = (uint8_t *) data;
    *packet_len = sizeof(data);
}

void pack_choke(uint8_t **packet, size_t *packet_len) {
    static const uint8_t data[] = {0x00, 0x00, 0x00, 0x01, MSG_TYPE_CHOKE};
    *packet = (uint8_t *) data;
    *packet_len = sizeof(data);
}


void pack_unchoke(uint8_t **packet, size_t *packet_len) {
    static const uint8_t data[] = {0x00, 0x00, 0x00, 0x01, MSG_TYPE_UNCHOKE};
    *packet = (uint8_t *) data;
    *packet_len = sizeof(data);
}

void pack_interested(uint8_t **packet, size_t *packet_len) {
    static const uint8_t data[] = {0x00, 0x00, 0x00, 0x01, MSG_TYPE_INTERESTED};
    *packet = (uint8_t *) data;
    *packet_len = sizeof(data);
}

void pack_not_interested(uint8_t **packet, size_t *packet_len) {
    static const uint8_t data[] = {0x00, 0x00, 0x00, 0x01, MSG_TYPE_NOT_INTERESTED};
    *packet = (uint8_t *) data;
    *packet_len = sizeof(data);
}

bool pack_have(uint32_t piece_index, uint8_t **packet, size_t *packet_len) {
    /* 4 byte length + 1 byte type + 4 byte piece_index */
    size_t tmp_packet_len = 9;

    if (!packet) {
        warn_log("Packet cannot be NULL");
        return false;
    }

    void *tmp_packet = calloc(tmp_packet_len, 1);
    if (!tmp_packet) {
        mem_errlog();
        return false;
    }

    struct netbuf nbuf = {0};
    create_network_buffer(&nbuf, tmp_packet, tmp_packet_len);

    /* Push the length */
    push_uint32(&nbuf, tmp_packet_len - 4);

    /* Push the type */
    push_uint8(&nbuf, MSG_TYPE_HAVE);

    /* Push the piece_index */
    push_uint32(&nbuf, piece_index);

    *packet = tmp_packet;

    if (packet_len) {
        *packet_len = tmp_packet_len;
    }
    return true;
}

bool pack_bitfield(void *bitfield, size_t bitfield_len, uint8_t **packet, size_t *packet_len) {
    /* 4 byte length + 1 byte type + bitfield_len */
    size_t tmp_packet_len = 5 + bitfield_len;

    if (!packet) {
        warn_log("Packet cannot be NULL");
        return false;
    }

    void *tmp_packet = calloc(tmp_packet_len, 1);
    if (!tmp_packet) {
        mem_errlog();
        return false;
    }

    struct netbuf nbuf = {0};
    create_network_buffer(&nbuf, tmp_packet, tmp_packet_len);

    /* Push the length */
    push_uint32(&nbuf, tmp_packet_len - 4);

    /* Push the type */
    push_uint8(&nbuf, MSG_TYPE_BITFIELD);

    /* Push the bitfield */
    push_data(&nbuf, bitfield, bitfield_len);

    *packet = tmp_packet;

    if (packet_len) {
        *packet_len = tmp_packet_len;
    }
    return true;
}


bool pack_request(uint32_t piece_index, uint32_t offset, uint32_t block_len, uint8_t **packet, size_t *packet_len) {
    /* 4 byte length + 1 byte type + 4 byte piece_index + 4 byte offset + 4 byte block_len */
    size_t tmp_packet_len = 17;

    if (!packet) {
        warn_log("Packet cannot be NULL");
        return false;
    }

    void *tmp_packet = calloc(tmp_packet_len, 1);
    if (!tmp_packet) {
        mem_errlog();
        return false;
    }

    struct netbuf nbuf = {0};
    create_network_buffer(&nbuf, tmp_packet, tmp_packet_len);

    /* Push the length */
    push_uint32(&nbuf, tmp_packet_len - 4);

    /* Push the type */
    push_uint8(&nbuf, MSG_TYPE_REQUEST);

    /* Push the piece index */
    push_uint32(&nbuf, piece_index);

    /* Push the piece offset */
    push_uint32(&nbuf, offset);

    /* Push the block length */
    push_uint32(&nbuf, block_len);

    *packet = tmp_packet;

    if (packet_len) {
        *packet_len = tmp_packet_len;
    }
    return true;
}

bool pack_piece(uint32_t piece_index, uint32_t offset, void *block_data, uint32_t block_len, uint8_t **packet, size_t *packet_len) {
    /* 4 byte length + 1 byte type + 4 byte piece_index + 4 byte offset + block_len */
    size_t tmp_packet_len = 13 + block_len;

    if (!packet) {
        warn_log("Packet cannot be NULL");
        return false;
    }

    void *tmp_packet = calloc(tmp_packet_len, 1);
    if (!tmp_packet) {
        mem_errlog();
        return false;
    }

    struct netbuf nbuf = {0};
    create_network_buffer(&nbuf, tmp_packet, tmp_packet_len);

    /* Push the length */
    push_uint32(&nbuf, tmp_packet_len - 4);

    /* Push the type */
    push_uint8(&nbuf, MSG_TYPE_PIECE);

    /* Push the piece index */
    push_uint32(&nbuf, piece_index);

    /* Push the piece offset */
    push_uint32(&nbuf, offset);

    /* Push the block length */
    push_data(&nbuf, block_data, block_len);

    *packet = tmp_packet;

    if (packet_len) {
        *packet_len = tmp_packet_len;
    }
    return true;
}
bool pack_cancel(uint32_t piece_index, uint32_t offset, uint32_t block_len, uint8_t **packet, size_t *packet_len) {
    /* 4 byte length + 1 byte type + 4 byte piece_index + 4 byte offset + 4 byte block_len */
    size_t tmp_packet_len = 17;

    if (!packet) {
        warn_log("Packet cannot be NULL");
        return false;
    }

    void *tmp_packet = calloc(tmp_packet_len, 1);
    if (!tmp_packet) {
        mem_errlog();
        return false;
    }

    struct netbuf nbuf = {0};
    create_network_buffer(&nbuf, tmp_packet, tmp_packet_len);

    /* Push the length */
    push_uint32(&nbuf, tmp_packet_len - 4);

    /* Push the type */
    push_uint8(&nbuf, MSG_TYPE_CANCEL);

    /* Push the piece index */
    push_uint32(&nbuf, piece_index);

    /* Push the piece offset */
    push_uint32(&nbuf, offset);

    /* Push the block length */
    push_uint32(&nbuf, block_len);

    *packet = tmp_packet;

    if (packet_len) {
        *packet_len = tmp_packet_len;
    }
    return true;
}

/***********************/
/* UNPACKING FUNCTIONS */
/***********************/

void unpack_msg(void *packet, enum msg_type *type, uint8_t **payload, size_t *payload_len) {
    struct netbuf nbuf = {0};
    size_t packet_len = ntohl(*((uint32_t *) packet)) + 4;  // Length doesn't include the 4 byte length of the packet initially

    create_network_buffer(&nbuf, packet, packet_len);
    buffer_seek_offset(&nbuf, 4);

    uint8_t tmp_type = MSG_TYPE_UNKNOWN;
    if (packet_len == 4) {
        /* Packet is a keep-alive message */
        tmp_type = MSG_TYPE_KEEP_ALIVE;
    } else {
        read_uint8(&nbuf, &tmp_type);
    }

    /* Validate the type */
    switch (tmp_type) {
        case MSG_TYPE_HAVE:
        case MSG_TYPE_BITFIELD:
        case MSG_TYPE_REQUEST:
        case MSG_TYPE_PIECE:
        case MSG_TYPE_CANCEL:
            *type = tmp_type;
            break;

        case MSG_TYPE_CHOKE:
        case MSG_TYPE_UNCHOKE:
        case MSG_TYPE_INTERESTED:
        case MSG_TYPE_NOT_INTERESTED:
        case MSG_TYPE_KEEP_ALIVE:
            *type = tmp_type;
            *payload_len = 0;
            *payload = NULL;
            return;

        default:
            *type = MSG_TYPE_UNKNOWN;
            *payload_len = 0;
            *payload = NULL;
            return;
    }

    *payload_len = packet_len - 5;
    *payload = ((uint8_t *) packet) + 5;
}


void unpack_msg_len(void *packet, size_t len, enum msg_type *type, uint8_t **payload, size_t *payload_len) {
    struct netbuf nbuf = {0};
    size_t packet_len = len;  // Length doesn't include the 4 byte length of the packet initially

    create_network_buffer(&nbuf, packet, packet_len);

    uint8_t tmp_type = MSG_TYPE_UNKNOWN;
    if (len == 0) {
        /* Packet is a keep-alive message */
        tmp_type = MSG_TYPE_KEEP_ALIVE;
    } else {
        read_uint8(&nbuf, &tmp_type);
    }

    /* Validate the type */
    switch (tmp_type) {
        case MSG_TYPE_HAVE:
        case MSG_TYPE_BITFIELD:
        case MSG_TYPE_REQUEST:
        case MSG_TYPE_PIECE:
        case MSG_TYPE_CANCEL:
            *type = tmp_type;
            break;

        case MSG_TYPE_CHOKE:
        case MSG_TYPE_UNCHOKE:
        case MSG_TYPE_INTERESTED:
        case MSG_TYPE_NOT_INTERESTED:
        case MSG_TYPE_KEEP_ALIVE:
            *type = tmp_type;
            *payload_len = 0;
            *payload = NULL;
            return;

        default:
            *type = MSG_TYPE_UNKNOWN;
            *payload_len = 0;
            *payload = NULL;
            return;
    }

    *payload_len = packet_len - 1;
    *payload = ((uint8_t *) packet) + 1;
}
void unpack_have(void *payload, uint32_t *piece_index) {
    *piece_index = ntohl(*((uint32_t *) payload));
}

void unpack_bitfield(void *payload, size_t payload_len, uint8_t **bitfield) {
    *bitfield = calloc(payload_len, 1);
    if (*bitfield) {
        memcpy(*bitfield, payload, payload_len);
    } else {
        mem_errlog();
    }
}

void unpack_request(void *payload, uint32_t *piece_index, uint32_t *offset, uint32_t *block_len) {
    struct netbuf nbuf;
    create_network_buffer(&nbuf, payload, 12);

    read_uint32(&nbuf, piece_index);
    read_uint32(&nbuf, offset);
    read_uint32(&nbuf, block_len);
}

void unpack_piece(void *payload, size_t payload_len, uint32_t *piece_index, uint32_t *offset, uint8_t **block) {
    *block = calloc(payload_len, 1);
    if (*block) {
        struct netbuf nbuf;
        create_network_buffer(&nbuf, payload, payload_len);

        read_uint32(&nbuf, piece_index);
        read_uint32(&nbuf, offset);
        read_data(&nbuf, *block, payload_len - 8);

    } else {
        mem_errlog();
    }
}

void unpack_cancel(void *payload, uint32_t *piece_index, uint32_t *offset, uint32_t *block_len) {
    struct netbuf nbuf;
    create_network_buffer(&nbuf, payload, 12);

    read_uint32(&nbuf, piece_index);
    read_uint32(&nbuf, offset);
    read_uint32(&nbuf, block_len);
}

void unpack_handshake(void *packet, uint32_t packet_len, char **pstr, uint8_t **info_hash, uint8_t **peer_id) {

    /* Initialize netbuf struct */

    struct netbuf nbuf;
    create_network_buffer(&nbuf, (uint8_t *)packet, packet_len);

    /* Set up variables */

    uint8_t pstrlen = packet_len - 48;
    uint8_t reserved[8];

    /*HACK Hacked together by Ciler. Ask me if issues arise*/
    *pstr = calloc(pstrlen+1, 1);

    *info_hash = malloc(20);
    *peer_id = malloc(20);

    /* Read strlen */

    /* Read string */
    read_string_data(&nbuf, *pstr, pstrlen);

    /* Read reserved bytes, throw them away for now */
    read_data(&nbuf, reserved, 8);

    /* Read info hash and peer id */
    read_data(&nbuf, *info_hash, 20);
    read_data(&nbuf, *peer_id, 20);
}