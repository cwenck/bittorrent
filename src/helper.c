#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <poll.h>

#include "helper.h"
#include "log.h"
#include "common.h"

int32_t open_connection(struct sockaddr_in *addr) {
    int32_t sock;
    // Safety
    addr->sin_family = PF_INET;
    // Try and get a socket
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        error_log("socket() failed");
        return -1;
    }

    int flags = fcntl(sock, F_GETFL, 0);
    fcntl(sock, F_SETFL, flags | O_NONBLOCK);

    // Try and connect on this socket
    errno = 0;
    if (connect(sock, (struct sockaddr *)addr, sizeof(*addr)) != 0) {
        int flags = fcntl(sock, F_GETFL, 0);
        fcntl(sock, F_SETFL, flags & ~O_NONBLOCK);

        if (errno == EINPROGRESS) {
            errno = 0;
            struct pollfd fds[1];
            fds[0].fd = sock;
            fds[0].events = POLLOUT;
            fds[0].revents = 0;

            int poll_status = poll(fds, 1, CONNECT_TIMEOUT_MILLIS);
            if (poll_status == 0) {
                /* Poll timeout */
                // debug_log("Poll timeout");
                close(sock);
                return -1;
            } else if (poll_status && (fds[0].revents & POLLOUT)) {
                int sock_status;
                socklen_t len = sizeof(sock_status);
                if (getsockopt(fds[0].fd, SOL_SOCKET, SO_ERROR, &sock_status, &len) < 0 || sock_status != 0) {
                    error_log("Failed to connect socket");
                    return -1;
                }
                return sock;
            } else {
                error_log("Poll failure");
                close(sock);
                return -1;
            }
        } else {
            error_log("connect() failed");
            close(sock);
            return -1;
        }
    }
    verbose_log("After connect");
    return sock;
}

int32_t open_udp_connection() {
    int32_t sock;
    // Try and get a socket
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
        error_log("socket() failed");
        return -1;
    }
    return sock;
}
