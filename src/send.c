#include "send.h"
#include <poll.h>
#include <stdio.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <time.h>

#include "peer.h"
#include "common.h"
#include "log.h"
#include "util.h"
#include "netmsg_packer.h"

static void tbl_rdlock() {
    verbose_log("Tbl rd lock %d", pthread_self());
    pthread_rwlock_rdlock(&g_peers->tbl_lock);
}

static void tbl_wrlock() {
    verbose_log("Tbl wr lock %d", pthread_self());
    pthread_rwlock_wrlock(&g_peers->tbl_lock);
}

static void tbl_unlock() {
    verbose_log("Tbl unlock %d", pthread_self());
    pthread_rwlock_unlock(&g_peers->tbl_lock);
}


/* Very important: This assumes that the peer is already locked */
void update_send_timestamp(struct peer *peer) {
    clock_gettime(CLOCK_REALTIME, &(peer->last_sent));
}

void *timestamp_send_thread(void *args) {
    struct peer *entry, *tmp;
    struct timespec tv;

    /* Continuously check for elapsed timestamps */
    trace_log("Thread %d", pthread_self());
    while(1) {
        tbl_rdlock();
        HASH_ITER(hh, g_peers->head, entry, tmp) {

            /* Check times */
            peer_rdlock(entry);
            clock_gettime(CLOCK_REALTIME, &tv);
            uint64_t elapsed_sec = timespec_elapsed_sec(&(entry->last_sent), &tv);

            if(elapsed_sec >= 60) {

                /* Unlock and get a write lock */

                peer_unlock(entry);
                //trace_log("Trying to get lock");
                peer_wrlock(entry);

                /* Get correct packet */

                uint8_t *packet;
                size_t packetlen;
                pack_keep_alive(&packet, &packetlen);

                /* Send */

                int ret = send(entry->sockfd, packet, packetlen, MSG_NOSIGNAL);
                if(ret < 1){
                    warn_log("Send failed");
                    peer_unlock(entry);
                    tbl_unlock();
                    peer_disconnect(entry);
                    //trace_log("Finished");
                    tbl_rdlock();
                    continue;
                }
                    update_send_timestamp(entry);
            }
            peer_unlock(entry);
        } 

        /* Sleep for approximately one long time */
        tbl_unlock();
        sleep(15);
    }

    debug_log("exiting thread");
}

void *send_thread(void *args) {

    /* Continuously check */
    trace_log("Thread %d", pthread_self());
    while(1) {
        sem_wait(&(g_peers->send_peer_sem));

        /* Lock whole table */
        tbl_rdlock();
        
        /* Iterate through all peers */
        struct peer *entry, *tmp;
        HASH_ITER(hh, g_peers->head, entry, tmp) {

            /* Lock individual entry */

            peer_wrlock(entry);
            
            //Todo: Check if block being sent

            /* Send all data commands from linked list first */

            struct link_list *queue_head = entry->send_queue;
            uint8_t *data;
            size_t data_len;

            if(linklist_tryread(queue_head, &data, &data_len)) {
                
                /* Send data and decrement semaphore */
                
                int ret = send(entry->sockfd, data, data_len, MSG_NOSIGNAL );

                if(ret < 1){
                    warn_log("Send failed");
                    peer_unlock(entry);
                    peer_disconnect(entry);
                    continue;
                }
                    update_send_timestamp(entry);
                /* End */                    
                free(data);
                peer_unlock(entry);
                break;
            }

            /* Send block */

            if(entry->block_send_buf != NULL) {
                //TODO: Make this unblocking
                int ret = send(entry->sockfd, entry->block_send_buf, entry->block_send_len, MSG_NOSIGNAL);

                if(ret < 1){
                    warn_log("Send failed");
                    peer_unlock(entry);
                    peer_disconnect(entry);
                    continue;
                }

                /* Get this ready for others */
                free(entry->block_send_buf);
                entry->block_send_buf = NULL;
                entry->block_send_len = 0;
                update_send_timestamp(entry);
                peer_unlock(entry);
                break;
            }

            /* Finish up */
            peer_unlock(entry);

        }

        /* Unlock whole table */
        tbl_unlock();
    }
    
}
/* OLD */

// void send_poll() {

//     /* Get Peer count */

//     int nfds = peer_count();

//     /* Continuously poll */

//     while (1) {
//         if (poll(g_peers->send_poll_fds, nfds, -1) > 0) { //continuously poll

//             /* Deal with all connections */

//             for (int i = 0; i < nfds; i++) {

//                 /* Normal peer sending data */

//                 if (g_peers->send_poll_fds[i].revents & POLLOUT && g_peers->send_poll_fds[i].fd != -1) {
//                      Note that there is a check if fd is -1 

//                     /* Get correct object */

//                     struct peer *send_peer = peer_find(g_peers->send_poll_fds[i].fd);

//                     ssize_t amount = send(g_peers->send_poll_fds[i].fd, send_peer->send_buf, send_peer->send_len, 0);
//                     if (amount != send_peer->send_len) {
//                         warn_log("Recv error");
//                         continue;
//                     }

//                 }


//             }

//         }
//         nfds = peer_count();
//     }
// }