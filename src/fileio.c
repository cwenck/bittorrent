#include "fileio.h"

#include <errno.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <thread_registry.h>
#include <linux/limits.h>

#include "log.h"
#include "client.h"
#include "hash.h"
#include "common.h"
#include "metafile_parse.h"

struct fileio_data {
    int file_fd;
    uint8_t *file;
    size_t file_len;
    size_t piece_size;
};

static struct fileio_data file_data;

static bool fileio_flush() {
    errno = 0;
    if (!file_data.file) {
        warn_log("File not inititalized [call fileio_init() first]");
        return false;
    }

    return (msync(file_data.file, file_data.file_len, MS_ASYNC) == 0);
}

bool fileio_init(const char *filepath, size_t total_length, size_t piece_size) {
    errno = 0;
    file_data.file_len = 0;
    file_data.file = NULL;

    int fd = open(filepath, O_RDWR | O_CREAT, 0664);

    if (fd < 0) {
        error_log("Failed to open the file '%s'", filepath);
        return false;
    }


    if (ftruncate(fd, total_length) == -1) {
        error_log("Failed to allocate file space");
        return false;
    }

    file_data.file = mmap(NULL, total_length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (file_data.file == NULL) {
        error_log("Failed to create a mapping for the file '%s'", filepath);
        return false;
    }

    file_data.file_len = total_length;
    file_data.piece_size = piece_size;
    file_data.file_fd = fd;

    return true;
}

void fileio_destroy() {
    if (file_data.file) {
        close(file_data.file_fd);
        msync(file_data.file, file_data.file_len, MS_SYNC);
        munmap(file_data.file, file_data.file_len);
    }
}

bool fileio_write_block(uint32_t piece_index, size_t offset, void *data, size_t data_len) {
    uint64_t file_offset = (piece_index * file_data.piece_size) + offset;

    if (file_offset + data_len > file_data.file_len) {
        warn_log("fileio write block would write past the end of the file");
        return false;
    }

    memcpy(file_data.file + file_offset, data, data_len);
    if (!fileio_flush()) {
        warn_log("Failed to flush data to disk");
    }

    return true;
}

bool fileio_read_block(uint32_t piece_index, size_t offset, void *data, size_t data_len) {
    uint64_t file_offset = (piece_index * file_data.piece_size) + offset;

    if (file_offset + data_len > file_data.file_len) {
        warn_log("Fileio write block would write past the end of the file");
        return false;
    }

    memcpy(data, file_data.file + file_offset, data_len);
    return true;
}

bool fileio_read_piece(uint32_t piece_index, void *data) {
    return fileio_read_block(piece_index, 0, data, file_data.piece_size);
}

bool fileio_write_piece(uint32_t piece_index, void *data) {
    return fileio_write_block(piece_index, 0, data, file_data.piece_size);
}

bool fileio_verify_piece(uint32_t piece_index, size_t piece_len, void *expected_hash) {
    uint64_t file_offset = piece_index * file_data.piece_size;

    EVP_MD_CTX ctx;
    if (!checksum_init(&ctx, SHA1)) {
        error_log("checksum_init() failed");
        return false;
    }

    uint8_t hash[SHA1_SIZE];

    bool checksum_status = checksum_finish(&ctx, file_data.file + file_offset, piece_len, hash, SHA1_SIZE);

    if (!checksum_status) {
        error_log("checksum_finish() failed");
        checksum_cleanup(&ctx);
        return false;
    }

    checksum_cleanup(&ctx);

    return (memcmp(hash, expected_hash, SHA1_SIZE) == 0);
}

static bool fileio_pop_file(char *path, size_t file_size) {
    bool retval = true;

    errno = 0;
    int fd = open(path, O_RDWR | O_CREAT, 0664);

    if (fd < 0) {
        error_log("Failed to open the file '%s'", path);
        return false;
    }

    if (ftruncate(fd, file_size) == -1) {
        error_log("Failed to allocate file space");
        return false;
    }

    void *tmp_file = mmap(NULL, file_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (tmp_file == NULL) {
        error_log("Failed to create a mapping for the file '%s'", path);
        return false;
    }


    if (!(madvise(file_data.file, file_data.file_len, MADV_SEQUENTIAL) == 0 && madvise(tmp_file, file_size, MADV_SEQUENTIAL) == 0)) {
        warn_log("Call to madvise() failed");
    }

    size_t start_offset = file_data.file_len - file_size;
    memcpy(tmp_file, file_data.file + start_offset, file_size);
    file_data.file_len -= file_size;

    close(fd);

    if (msync(tmp_file, file_size, MS_SYNC) != 0) {
        error_log("Failed to sync data to disk");
        retval = false;
    }

    if (munmap(tmp_file, file_size) != 0) {
        warn_log("Call to munmap failed");
    }

    if (ftruncate(file_data.file_fd, file_data.file_len) == -1) {
        warn_log("Failed to shrink the file size");
    }

    return retval;
}

static int fileio_make_path_dirs(char *path) {
    char *ret;
    int r;
    char cwd[PATH_MAX];
    ret = getcwd(cwd, PATH_MAX);

    char *save_ptr = NULL;
    char *path_cpy = malloc(strlen(path) + 1);

    if (!path_cpy) {
        mem_errlog();
        return false;
    }

    strcpy(path_cpy, path);

    char *prev_dir = "";
    char *curr = NULL;

    bool retval = true;
    int dirs_made = 0;
    char *strtok_arg = path_cpy;

    while ((curr = strtok_r(strtok_arg, "/", &save_ptr))) {
        strtok_arg = NULL;

        /* If the prev dir is not empty, make the directory and move into it */
        if (strlen(prev_dir) != 0) {
            if (mkdir(prev_dir, 0775) == 0 || errno == EEXIST) {
                if (chdir(prev_dir) != 0) {
                    error_log("Failed to change directory");
                    retval = false;
                    break;
                }
                verbose_log("Created dir '%s'", prev_dir);
                dirs_made++;
            } else {
                error_log("Failed to create directory");
                retval = false;
                break;
            }
        }
        prev_dir = curr;
    }

    /* Reset the cwd */
    r = chdir(cwd);
    UNUSED(r);
    UNUSED(ret);

    return (retval) ? dirs_made : -1;
}

void *fileio_finalize(void *thread_args) {
    clock_gettime(CLOCK_MONOTONIC, &g_client_info->end_time);

    info_log(TXT_COLOR_BLUE TXT_FMT_BOLD "DOWNLOAD FINISHED!" TXT_ALL_RESET);

    uint32_t file_count = g_metafile->info.file_count;
    char *folder = g_metafile->info.folder_name;
    struct mf_file *files = g_metafile->info.files;
    int ret;
    char *r;

    char cwd[PATH_MAX];
    r = getcwd(cwd, PATH_MAX);

    info_log("Folder is '%s' with %u files", folder, file_count);

    if (folder) {
        errno = 0;
        if (mkdir(folder, 0775) == 0 || errno == EEXIST) {
            if (chdir(folder) != 0) {
                error_log("Failed to change directory");
                thread_finished();
                return NULL;
            }
        } else {
            error_log("Failed to create directory");
            thread_finished();
            return NULL;
        }
    }

    /* Store each file, and make dirs as needed */
    for (int64_t file_i = file_count - 1; file_i >= 0; file_i--) {
        struct mf_file *file = files + file_i;
        info_log("Storing file with path '%s'", file->path);
        if (fileio_make_path_dirs(file->path) != -1) {
            if (file_i == 0) {
                /* Last file to extract, so we can just use the entire download.part file remaining */

                /* If there is only one file that isn't in any directories, then hard link it to the correct place */
                /* The original download.part will be deleted after leaving just this link to the file */
                info_log("Writing last file '%s'", file->path);
                if (folder) {
                    ret = link("../download.part", file->path);
                } else {
                    ret = link("download.part", file->path);
                }
            } else {
                fileio_pop_file(file->path, file->length);
            }
        } else {
            error_log("Failed to make directories");
            return NULL;
        }
    }

    /* Reset the cwd */
    ret = chdir(cwd);
    UNUSED (ret);
    UNUSED(r);

    /* Remove the 0 byte download.part file */
    if (remove("download.part") != 0) {
        warn_log("Failed to remove download.part");
    }

    thread_finished();
    info_log(TXT_COLOR_BLUE TXT_FMT_BOLD "FINAL FILES GENERATED!" TXT_ALL_RESET);
    return NULL;
}