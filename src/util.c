#include "util.h"

#include <stdlib.h>

double rand_double() {
    return (double) rand() / (double) RAND_MAX;
}

uint32_t rand_range(uint32_t min, uint32_t max) {
    return (uint32_t)(rand_double() * (max - min + 1) + min);
}

struct timespec timespec_elapsed(struct timespec *t1, struct timespec *t2) {
    struct timespec delta;

    if (t1->tv_sec > t2->tv_sec) {
        delta.tv_sec = t1->tv_sec - t2->tv_sec;
    } else {
        delta.tv_sec = t2->tv_sec - t1->tv_sec;
    }

    if (t1->tv_nsec > t2->tv_nsec) {
        delta.tv_nsec = t1->tv_nsec - t2->tv_nsec;
    } else {
        delta.tv_nsec = t2->tv_nsec - t1->tv_nsec;
    }

    return delta;
}

uint64_t timespec_elapsed_sec(struct timespec *t1, struct timespec *t2) {
    struct timespec tmp;
    tmp = timespec_elapsed(t1, t2);
    return tmp.tv_sec;
}
