#include "peer.h"

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/eventfd.h>

#include "netmsg_packer.h"
#include "metafile_parse.h"
#include "common.h"
#include "hash.h"
#include "log.h"

static void tbl_rdlock() {
    //verbose_log("Tbl rd lock %d", pthread_self());
    pthread_rwlock_rdlock(&g_peers->tbl_lock);
}

static void tbl_wrlock() {
    //verbose_log("Tbl wr lock %d", pthread_self());
    pthread_rwlock_wrlock(&g_peers->tbl_lock);
}

static void tbl_unlock() {
    //verbose_log("Tbl unlock %d", pthread_self());
    pthread_rwlock_unlock(&g_peers->tbl_lock);
}

void peer_rdlock(struct peer *peer_data) {
    pthread_rwlock_rdlock(&peer_data->lock);
}

void peer_wrlock(struct peer *peer_data) {
    pthread_rwlock_wrlock(&peer_data->lock);
}

void peer_unlock(struct peer *peer_data) {
    pthread_rwlock_unlock(&peer_data->lock);
}

// static void peer_switch_rdlock(struct peer *peer_data) {
//     peer_unlock(peer_data);
//     peer_rdlock(peer_data);
// }

// static void peer_switch_wrlock(struct peer *peer_data) {
//     peer_unlock(peer_data);
//     peer_wrlock(peer_data);
// }

bool global_peers_init() {
    g_peers = calloc(sizeof(struct peer_hash), 1);
    g_peers->head = NULL;

    if (!g_peers) {
        mem_errlog();
        return false;
    }

    if (pthread_rwlock_init(&g_peers->tbl_lock, NULL) != 0) {
        error_log("Failed to init lock");
        free(g_peers);
        return false;
    }

    return true;
}

void global_peers_destroy() {
    pthread_rwlock_destroy(&g_peers->tbl_lock);
    free(g_peers);
}

struct peer *peer_create() {
    struct peer *tmp = calloc(sizeof(struct peer), 1);
    if (!tmp) {
        mem_errlog();
        return NULL;
    }

    tmp->piece_map = calloc(pieceset_piece_size(), 1);
    if (!tmp->piece_map) {
        free(tmp);
        mem_errlog();
        return NULL;
    }

    if (pthread_rwlock_init(&tmp->lock, NULL) != 0) {
        error_log("Failed to initialize lock");
        free(tmp->piece_map);
        free(tmp);
        return false;
    }

    tmp->notify = eventfd(0, EFD_NONBLOCK );

    if (tmp->notify == -1) {
        error_log("Failed to initialize eventfd");
        pthread_rwlock_destroy(&tmp->lock);
        free(tmp->piece_map);
        free(tmp);
        return false;
    }


    tmp->am_choking = true;
    tmp->peer_choking = true;
    tmp->bytes_received = 0;
    tmp->piece_downloading = NULL;

    for(int i = 0; i < NUM_OUTGOING; i++){
        tmp->outgoing_requests[i].completed = true;
    }

    return tmp;
}

void peer_destroy(struct peer *peer_data) {
    if (peer_data) {
        peer_wrlock(peer_data);
        free(peer_data->piece_map);
        peer_unlock(peer_data);
        pthread_rwlock_destroy(&peer_data->lock);
        free(peer_data);
    }
}

bool peer_add(struct peer *peer_data) {
    if (peer_find(peer_data->sockfd)) {
        return false;
    }

    tbl_wrlock();
    HASH_ADD_INT(g_peers->head, sockfd, peer_data);
    tbl_unlock();

    return true;
}

struct peer *peer_find(int sockfd) {
    struct peer *tmp;
    tbl_rdlock();
    HASH_FIND_INT(g_peers->head, &sockfd, tmp);
    tbl_unlock();
    return tmp;
}

void peer_del(int sockfd) {
    struct peer *tmp = peer_find(sockfd);
    if (tmp) {

        //trace_log("Getting lock");
        tbl_wrlock();
        HASH_DEL(g_peers->head, tmp);
        tbl_unlock();
        pieceset_del_peer(tmp);
    }
    // trace_log("Finished del");
}

int peer_count() {
    tbl_rdlock();
    int count = HASH_COUNT(g_peers->head);
    tbl_unlock();
    return count;
}


/*******************/
/* Peer Processing */
/*******************/

void peer_disconnect(struct peer *peer_data) {
    peer_wrlock(peer_data);
    // info_log("Disconnecting Peer %s:%d", inet_ntoa(peer_data->addr.sin_addr), ntohs(peer_data->addr.sin_port));
    close(peer_data->sockfd);
    close(peer_data->notify);
    peer_unlock(peer_data);
    peer_del(peer_data->sockfd);
    peer_destroy(peer_data);
}
