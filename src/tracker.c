#include <byteswap.h>
#include <ctype.h>
#include <math.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "client.h"
#include "common.h"
#include "helper.h"
#include "log.h"
#include "metafile_parse.h"
#include "tracker.h"
#include "netmsg_packer.h"
#include "piece_set.h"
#include "peer_thread.h"
#include "thread_registry.h"

/**
 * Get the length of characters needed to represent an integer as a string.
 */
size_t intlen(int32_t i) {
    if (i == 0) {
        return 1;
    } else if (i < 0) {
        // Extra for negative sign
        return floor(log10(abs(i))) + 2;
    } else {
        return floor(log10(abs(i))) + 1;
    }
}

/**
 * Helper function for URL encoding binary blob into a string.
 * Returns string allocated on the heap.
 */
char *encode(uint8_t *blob, uint32_t size) {
    char *res = malloc(size + 1);
    if (!res) {
        error_log("malloc() failed");
        return NULL;
    }
    uint32_t i;
    uint32_t pos = 0;
    uint32_t res_len = size;
    for (i = 0; i < size; i++) {
        if (!isalnum(blob[i]) && blob[i] != '.' && blob[i] != '-' &&
                blob[i] != '_' && blob[i] != '~') {
            // Realloc the buffer (w/ \0 terminator) to include 2 extra chars
            res_len += 2;
            res = realloc(res, res_len + 1);
            if (!res) {
                error_log("malloc() failed");
                return NULL;
            }
            // Encode the byte as %<hex-value>
            sprintf(res + pos, "%%%02X", blob[i]);
            pos += 3;
        } else {
            // Just copy it in
            res[pos] = blob[i];
            pos++;
        }
    }
    // NULL terminator
    res[res_len] = '\0';
    return res;
}

/**
 * Helper function pulls in global client data to construct an HTTP request to
 * be sent along to the tracker.
 * Returns string allocated on the heap.
 * TODO numwant (by default, typical response is 50. do we actually need more?)
 */
char *build_request(char *event, char *hostname, char *res) {
    // Pull in the global data
    char *info_hash = encode(g_metafile->info.hash, 20);
    char *peer_id = encode(g_client_info->peer_id, 20);
    int64_t left = get_filesize(g_metafile) - g_client_info->downloaded;

    if (strlen(event) != 0) {
        // Allocate the string
        char *req = calloc(strlen("GET /?info_hash=") + strlen(res) +
                           + strlen(info_hash) + 1
                           + strlen("peer_id=") + strlen(peer_id) + 1
                           + strlen("port=") + intlen(g_client_info->port) + 1
                           + strlen("uploaded=") + intlen(g_client_info->uploaded) + 1
                           + strlen("downloaded=") + intlen(g_client_info->downloaded) + 1
                           + strlen("left=") + intlen(left) + 1
                           + strlen("event=") + strlen(event) + 1
                           + strlen("compact=1&numwant=80&no_peer_id=1 HTTP/1.1\r\nHost: ")
                           + strlen(hostname) + strlen("\r\n\r\n") + 1 + 1, 1);
        if (!req) {
            error_log("malloc() failed");
            return NULL;
        }
        // Build the string
        sprintf(req, "GET /%s?info_hash=%s&peer_id=%s&port=%d&uploaded=%lu&"
                "downloaded=%lu&left=%lu&event=%s&compact=1&numwant=80&no_peer_id=1 HTTP/1.1\r\n"
                "Host: %s\r\n\r\n", res, info_hash, peer_id, g_client_info->port,
                g_client_info->uploaded, g_client_info->downloaded, left, event,
                hostname);
        // Cleanup heap-allocated strings
        free(info_hash);
        free(peer_id);
        // Return our request string
        return req;
    } else {
        // Allocate the string
        char *req = calloc(strlen("GET /?info_hash=") + strlen(res) +
                           + strlen(info_hash) + 1
                           + strlen("peer_id=") + strlen(peer_id) + 1
                           + strlen("port=") + intlen(g_client_info->port) + 1
                           + strlen("uploaded=") + intlen(g_client_info->uploaded) + 1
                           + strlen("downloaded=") + intlen(g_client_info->downloaded) + 1
                           + strlen("left=") + intlen(left) + 1
                           + strlen("compact=1&numwant=80&no_peer_id=1 HTTP/1.1\r\nHost: ")
                           + strlen(hostname) + strlen("\r\n\r\n") + 1 + 1, 1);
        if (!req) {
            error_log("malloc() failed");
            return NULL;
        }
        // Build the string
        sprintf(req, "GET /%s?info_hash=%s&peer_id=%s&port=%d&uploaded=%lu&"
                "downloaded=%lu&left=%lu&compact=1&numwant=80&no_peer_id=1 HTTP/1.1\r\n"
                "Host: %s\r\n\r\n", res, info_hash, peer_id, g_client_info->port,
                g_client_info->uploaded, g_client_info->downloaded, left,
                hostname);
        // Cleanup heap-allocated strings
        free(info_hash);
        free(peer_id);
        // Return our request string
        return req;
    }
}

/**
 * Keeps receiving on the provided socket until the data stops coming.
 * Dynamically reallocates the buffer as more data comes in.
 * Returns a string allocated on the heap with the complete response.
 */
char *receive_http(int32_t sock, int32_t *len) {
    int got = 0, total = 0, size = 4096, buf_limit = 512;
    char *res = malloc(size);
    if (!res) {
        error_log("malloc() failed");
        return NULL;
    }
    char *tmp = malloc(buf_limit);
    if (!tmp) {
        error_log("malloc() failed");
        free(res);
        return NULL;
    }
    // Receive forever
    for (;;) {
        got = recv(sock, tmp, buf_limit, 0);
        // Done receiving
        if (got == 0) {
            break;
        } else if (got < 0) {
            error_log("recv() failed");
            free(res);
            res = NULL;
            break;
        }
        // Would overflow the buffer: double
        if (got + total > size - 1) {
            size *= 2;
            char *t = realloc(res, size);
            if (!t) {
                error_log("realloc() failed");
                free(res);
                res = NULL;
                break;
            }
            res = t;
        }
        // Copy in the results
        memcpy(res + total, tmp, got);
        total += got;
    }
    *len = total;
    *(res + total) = '\0';
    free(tmp);
    return res;
}

/**
 * Parses the hostname and port from a URL.
 */
char *get_hostname(char *prefix, int16_t *port, char **res) {
    info_log("get_hostname() called with %s", g_metafile->announce);
    char *hostname = malloc(strlen(g_metafile->announce));
    if (!hostname) {
        error_log("malloc() failed");
        return NULL;
    }
    *res = malloc(strlen(g_metafile->announce));
    if (!*res) {
        error_log("malloc() failed");
        return NULL;
    }
    char *format = calloc(strlen(prefix) + 16, 1);
    if (!format) {
        error_log("malloc() failed");
        return NULL;
    }
    strcpy(format, prefix);
    strcat(format, "%[^:]:%d/%[^\n]");
    // prefix is in the announce
    if (!strncmp(prefix, g_metafile->announce, strlen(prefix))) {
        int p;
        int32_t err = sscanf(g_metafile->announce, format, hostname, &p, *res);
        if (err == 3 || err == 2) {
            *port = (int16_t)p;
            info_log("get_hostname() returned %s with port %hd and page %s",
                    hostname, *port, *res);
            return hostname;
        } else {
            strcpy(format + strlen(prefix), "%[^/]/%[^\n]");
            err = sscanf(g_metafile->announce, format, hostname, *res);
            if (err == 2) {
                info_log("get_hostname() returned %s with port %hd and page %s",
                        hostname, *port, *res);
                return hostname;
            }
        }
    }
    // TODO failed to parse announce
    return NULL;
}

/**
 * Helper method for HTTP update tracker that populates all the relevant data
 * fields from a bencoded dictionary into the tracker_info structure. Makes
 * connections to all the peers in the list.
 */
void update_tracker(struct tracker *tracker_info, struct tracker_resp *dict) {
    // Lock the tracker_info struct
    pthread_rwlock_wrlock(&tracker_info->lock);
    // Populate the data fields
    tracker_info->interval = dict->interval;
    tracker_info->min_interval = dict->min_interval;
    tracker_info->complete = dict->complete;
    tracker_info->incomplete = dict->incomplete;
    // Unlock the tracker_info struct while connecting
    pthread_rwlock_unlock(&tracker_info->lock);
    int32_t idx;
    verbose_log("Peer Count = %u", dict->peer_count);
    for (idx = 0; idx < dict->peer_count; idx++) {
        // Don't open connections to more than PEER_LIMIT peers
        if (peer_count() > PEER_LIMIT) {
            break;
        }
        struct tracker_peer curr = dict->peers[idx];
        // Initialize new peer
        struct peer *peer = peer_create();
        if (!peer) {
            error_log("Failed to create peer");
            continue;
        }
        inet_aton(curr.address, &peer->addr.sin_addr);
        peer->addr.sin_port = htons(curr.port);
        // Check if peer already exists
        pthread_rwlock_rdlock(&g_peers->tbl_lock);
        struct peer *p;
        int32_t found = 0;
        for (p = g_peers->head; p != NULL; p = p->hh.next) {
           if (p->addr.sin_addr.s_addr == peer->addr.sin_addr.s_addr &&
                   p->addr.sin_port == peer->addr.sin_port) {
               found = 1;
               break;
            }
        }
        pthread_rwlock_unlock(&g_peers->tbl_lock);
        if (found) {
            info_log("Peer already in hashtable, passing");
            peer_destroy(peer);
            continue;
        }
        // Connect failed, just pass this peer
        peer->sockfd = open_connection(&peer->addr);
        if (peer->sockfd < 0 ) {
            // warn_log("Peer %s:%d failed to connect.", curr.address, curr.port);
            peer_destroy(peer);
            continue;
        } else {
            struct peer_thread_options *opts = malloc(sizeof(struct peer_thread_options));
            opts->self = peer;
            info_log("Connected to %s:%d on socket %d", curr.address, curr.port,
                     peer->sockfd);
            // Add to the hash table
            peer_add(peer);
            thread_start(peer_thread, opts);
        }
    }
}

/**
 * Parses the tracker's HTTP response and populates the global tracker struct
 * with a peer list and other tracker-related information.
 */
int32_t http_update_tracker(char *resp, int32_t resp_len) {
    if (!strncmp(resp, "HTTP/1.0 200 OK", strlen("HTTP/1.0 200 OK")) ||
            !strncmp(resp, "HTTP/1.1 200 OK", strlen("HTTP/1.1 200 OK"))) {
        // Get the dictionary
        char *split = strstr(resp, "\r\n\r\n");
        split += 4;
        resp_len -= (split - resp);
        struct tracker_resp *dict = parse_tracker_resp(split, resp_len);
        if (!dict) {
            warn_log("Trying to find dictionary within tracker's response.");
            char *needle = "d";
            char *tmp = strstr(split, needle);
            int32_t tmp_len = resp_len - (tmp - split);
            dict = parse_tracker_resp(tmp, tmp_len);
            if (!dict) {
                error_log("Tracker response could not be parsed");
                return -1;
            }
            info_log("Successfully found dictionary in tracker's response.");
        }
        // Populate the tracker directory
        update_tracker(g_tracker_info, dict);
        tracker_resp_cleanup(dict);
    } else {
        warn_log("Non-200 response from tracker");
        return -1;
    }
    return 0;
}

/**
 * Main driver for HTTP requests to the tracker. Builds and sends
 * a request using global client information and parses the response
 * into a global struct for other pieces of the client to use.
 */
int32_t do_http(char *event) {
    // Parse hostname from metainfo file (default port is 80)
    int16_t port = 80;
    char *res;
    char *hostname = get_hostname("http://", &port, &res);
    if (!hostname) {
        error_log("get_hostname() failed to retrieve HTTP hostname and port");
        return -1;
    }
    // Get tracker's IP address
    struct hostent *host = gethostbyname(hostname);
    if (!host) {
        error_log("gethostbyname() failed");
        free(hostname);
        return -1;
    }
    // Construct the request
    char *req = build_request(event, hostname, res);
    free(res);
    free(hostname);
    // Build the connection
    struct sockaddr_in tracker;
    memcpy(&tracker.sin_addr, host->h_addr, host->h_length);
    tracker.sin_port = htons(port);
    int32_t sock = open_connection(&tracker);
    if (sock < 0) {
        error_log("Failed to establish connection to tracker");
        return -1;
    }
    // Send the request
    if ((size_t)send(sock, req, strlen(req), 0) != strlen(req)) {
        error_log("send() failed");
        free(req);
        return -1;
    }
    free(req);
    // Get the response
    int32_t resp_len = 0;
    char *resp = receive_http(sock, &resp_len);
    if (!resp) {
        warn_log("Tracker HTTP receive failed");
        return -1;
    }
    //info_log("Received response from tracker:\n%s", resp);
    // Populate data structure
    if (!g_tracker_info) {
        // Does not exist yet (first announce)
        g_tracker_info = calloc(sizeof(struct tracker), 1);
        if (!g_tracker_info) {
            error_log("calloc() failed");
            free(resp);
            return -1;
        }
    }
    if (http_update_tracker(resp, resp_len) != 0) {
        warn_log("Bad response from tracker");
        return -1;
    }
    free(resp);
    return 0;
}

/**
 * Parses the tracker's UDP response and populates the global tracker struct
 * with a peer list and other tracker-related information. Makes a connection
 * to each peer in the list.
 */
void udp_update_tracker(struct annc_resp *resp, int32_t resp_len) {
    int32_t num_peers = (resp_len - 20) / 6;
    // Lock the tracker_info struct
    pthread_rwlock_wrlock(&g_tracker_info->lock);
    // Populate data fields
    g_tracker_info->interval = ntohl(resp->interval);
    g_tracker_info->min_interval = ntohl(resp->interval);
    g_tracker_info->complete = ntohl(resp->seeders);
    g_tracker_info->incomplete = ntohl(resp->leechers);
    // Unlock the tracker_info struct while connecting
    pthread_rwlock_unlock(&g_tracker_info->lock);
    int32_t idx;

    uint8_t *handshake, *bitfield, *bitfield_packet;
    unsigned long handshake_len, bitfield_len;
    pack_handshake(g_metafile->info.hash, g_client_info->peer_id, &handshake, &handshake_len);
    pieceset_generate_bitfield(&bitfield);
    pack_bitfield(bitfield, piece_bitfield_size(), &bitfield_packet, &bitfield_len);
    free(bitfield);

    for (idx = 0; idx < num_peers; idx++) {
        struct annc_peer curr = resp->peers[idx];
        // Initialize new peer
        struct peer *peer = peer_create();
        if (!peer) {
            error_log("Failed to create peer");
            continue;
        }
        memcpy(&peer->addr.sin_addr, &curr.ip, sizeof(int32_t));
        memcpy(&peer->addr.sin_port, &curr.port, sizeof(int16_t));
        // Check if peer already exists
        pthread_rwlock_rdlock(&g_peers->tbl_lock);
        struct peer *p;
        int32_t found = 0;
        for (p = g_peers->head; p != NULL; p = p->hh.next) {
            if (p->addr.sin_addr.s_addr == peer->addr.sin_addr.s_addr &&
                    p->addr.sin_port == peer->addr.sin_port) {
                 found = 1;
             }
         }
        pthread_rwlock_unlock(&g_peers->tbl_lock);
        if (found) {
            info_log("Peer already in hashtable, passing");
            peer_destroy(peer);
            continue;
        }
        // Connect failed, just pass this peer
        peer->sockfd = open_connection(&peer->addr);
        if (peer->sockfd < 0 ) {
            peer_destroy(peer);
            continue;
        } else {
            struct peer_thread_options *opts = malloc(sizeof(struct peer_thread_options));
            opts->self = peer;
            // Add to the hash table
            peer_add(peer);
            thread_start(peer_thread, opts);
        }
    }

    free(handshake);
    free(bitfield_packet);
}

/**
 * Build a UDP connect request for the tracker.
 */
void *build_connect_request() {
    struct cnct_req *req = malloc(sizeof(struct cnct_req));
    if (!req) {
        error_log("malloc() failed");
        return NULL;
    }
    // Network byte order magic
    req->proto_id = 0x8019102717040000;
    req->action = 0;
    req->trans_id = rand();
    return req;
}

/**
 * Receives a connect response from the specified tracker.
 * Returns a heap-allocated buffer.
 */
struct cnct_resp *receive_connect_response(int32_t sock, int32_t trans_id) {
    struct cnct_resp *resp = malloc(sizeof(struct cnct_resp));
    if (!resp) {
        error_log("malloc() failed");
        return NULL;
    }
    struct sockaddr_in addr;
    memset(&addr, 0x00, sizeof(struct sockaddr_in));
    socklen_t addr_len = sizeof(addr);
    if (recvfrom(sock, resp, sizeof(struct cnct_resp), 0,
                 (struct sockaddr *)&addr, &addr_len)
            != sizeof(struct cnct_resp)) {
        error_log("recvfrom() failed");
        free(resp);
        return NULL;
    }
    if (trans_id != resp->trans_id) {
        error_log("Bad transaction ID %d from tracker. Expected %d.",
                  resp->trans_id, trans_id);
        free(resp);
        return NULL;
    }
    return resp;
}

/**
 * Build a UDP announce request for the tracker.
 * Takes the connection id pulled from the connect request and
 * the event being requested.
 */
void *build_announce_request(int64_t cnct_id, int32_t event) {
    struct annc_req *req = malloc(sizeof(struct annc_req));
    if (!req) {
        error_log("malloc() failed");
        return NULL;
    }
    req->cnct_id = cnct_id;
    req->action = htonl(1);
    req->trans_id = rand();
    memcpy(req->info_hash, g_metafile->info.hash, 20);
    memcpy(req->peer_id, g_client_info->peer_id, 20);
    req->downloaded = bswap_64(g_client_info->downloaded);
    req->left = bswap_64(get_filesize(g_metafile) - g_client_info->downloaded);
    req->uploaded = bswap_64(g_client_info->uploaded);
    req->event = htonl(event);
    req->ip = 0;
    req->key = 0;
    req->num_want = -1;
    req->port = htons(g_client_info->port);
    return req;
}


/**
 * Receives an announce response from the specified tracker.
 * Returns a heap-allocated buffer.
 */
struct annc_resp *receive_announce_response(int32_t sock, int32_t trans_id,
        int32_t *resp_len) {
    // Just get the size of the message
    struct sockaddr_in addr;
    memset(&addr, 0x00, sizeof(struct sockaddr_in));
    socklen_t addr_len = sizeof(addr);
    char tmp[65535] = { 0 };
    int size = recvfrom(sock, &tmp, 65535, MSG_PEEK, (struct sockaddr *)&addr,
                        &addr_len);
    if (size < 0) {
        error_log("recvfrom() failed");
        return NULL;
    }
    struct annc_resp *resp = malloc(size);
    if (!resp) {
        error_log("malloc() failed");
        return NULL;
    }
    if ((recvfrom(sock, resp, size, 0, (struct sockaddr *)&addr, &addr_len))
            != size) {
        error_log("recvfrom() failed");
        free(resp);
        return NULL;
    }
    if (trans_id != resp->trans_id) {
        error_log("Bad transaction ID %d from tracker. Expected %d.",
                  resp->trans_id, trans_id);
        free(resp);
        return NULL;
    }
    *resp_len = size;
    return resp;
}


/**
 * Main driver for UDP requests to the tracker. Builds and sends
 * a request using global client information and parses the response
 * into a global struct for other pieces of the client to use.
 */
int32_t do_udp(int32_t event) {
    // Parse hostname from metainfo file (default port is 80)
    int16_t port = 80;
    char *res;
    char *hostname = get_hostname("udp://", &port, &res);
    if (!hostname) {
        error_log("get_hostname() failed to retrieve UDP hostname and port");
        return -1;
    }
    free(res);
    res = NULL;
    // Get tracker's IP address
    struct hostent *host = gethostbyname(hostname);
    if (!host) {
        error_log("gethostbyname() failed");
        free(hostname);
        return -1;
    }
    // Construct the connection request
    void *req = build_connect_request();
    free(hostname);
    // Build the connection
    struct sockaddr_in tracker;
    memcpy(&tracker.sin_addr, host->h_addr, host->h_length);
    tracker.sin_port = htons(port);
    tracker.sin_family = PF_INET;
    int32_t sock = open_udp_connection();
    if (sock < 0) {
        error_log("Failed to open UDP socket");
        return -1;
    }
    // Send connect request
    if ((size_t)sendto(sock, req, sizeof(struct cnct_req), 0,
                       (struct sockaddr *)&tracker, sizeof(tracker))
            != sizeof(struct cnct_req)) {
        error_log("sendto() failed");
        free(req);
        return -1;
    }
    // Receive connect response with connection id
    struct cnct_resp *c_resp = receive_connect_response(sock,
                               ((struct cnct_req *)req)->trans_id);
    free(req);
    if (!c_resp) {
        warn_log("Failed to receive a connect response");
        return -1;
    }
    // Send announce request
    req = build_announce_request(c_resp->cnct_id, event);
    free(c_resp);
    if ((size_t)sendto(sock, req, sizeof(struct annc_req), 0,
                       (struct sockaddr *)&tracker, sizeof(tracker))
            != sizeof(struct annc_req)) {
        error_log("sendto() failed");
        free(req);
        return -1;
    }
    int32_t resp_len = 0;
    struct annc_resp *a_resp = receive_announce_response(sock,
                               ((struct annc_req *)req)->trans_id, &resp_len);
    free(req);
    req = NULL;
    if (!a_resp) {
        warn_log("Failed to receive an announce response");
        return -1;
    }
    // Populate data structure
    if (!g_tracker_info) {
        // Does not exist yet (first announce)
        g_tracker_info = calloc(sizeof(struct tracker), 1);
        if (!g_tracker_info) {
            error_log("calloc() failed");
            free(a_resp);
            return -1;
        }
    }
    udp_update_tracker(a_resp, resp_len);
    free(a_resp);
    return 0;
}

int32_t http_start() {
    return do_http("started");
}

int32_t http_update() {
    return do_http("");
}

int32_t http_stop() {
    return do_http("stopped");
}

int32_t http_complete() {
    return do_http("completed");
}

int32_t udp_start() {
    return do_udp(2);
}

int32_t udp_update() {
    return do_udp(0);
}

int32_t udp_stop() {
    return do_udp(3);
}

int32_t udp_complete() {
    return do_udp(1);
}

void *http_tracker_daemon(void *args) {
    // Keep running forever
    for (;;)
    {
        // Update with the tracker
        int32_t rc = http_update();
        if (rc < 0) {
            warn_log("Tracker update over HTTP failed");
        }
        // We can go as often as we like
        if (g_tracker_info->min_interval == 0) {
            sleep(DEFAULT_TRACKER_UPDATE_TIME);
        } else {
            sleep(g_tracker_info->min_interval);
        }
    }

    return args;
}

void *udp_tracker_daemon(void *args) {
    for (;;)
    {
        // Update with the tracker
        int32_t rc = udp_update();
        if (rc < 0) {
            warn_log("Tracker update over HTTP failed");
        }
        // We can go as often as we like
        if (g_tracker_info->min_interval == 0) {
            sleep(DEFAULT_TRACKER_UPDATE_TIME);
        } else {
            sleep(g_tracker_info->min_interval);
        }
    }

    return args;
}

pthread_t init_tracker() {
    pthread_t tracker_thread;
    // Detect UDP
    if (!strncmp("udp://", g_metafile->announce, strlen("udp://"))) {
        info_log("Detected UDP tracker...");
        // Initial tracker request
        if (udp_start() != 0) {
            warn_log("Issue with initial tracker announce, exiting...");
            exit(-1);
        }
        // Start tracker daemon
        if (pthread_create(&tracker_thread, NULL, udp_tracker_daemon, NULL)
                != 0) {
            error_log("pthread_create(tracker_daemon) failed");
            exit(-1);
        }
    // Default to HTTP
    } else {
        // Initial tracker request
        if (http_start() != 0) {
            warn_log("Issue with initial tracker announce, exiting...");
            exit(-1);
        }
        // Start tracker daemon
        if(pthread_create(&tracker_thread, NULL, http_tracker_daemon, NULL)
                != 0) {
            error_log("pthread_create(tracker_daemon) failed");
            exit(-1);
        }
    }

    info_log("init_tracker() complete");
    // Return thread for client to manage
    return tracker_thread;
}
