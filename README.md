## MASTER : [![build status](https://gitlab.com/cwenck/bittorrent/badges/master/build.svg)](https://gitlab.com/cwenck/bittorrent/commits/master) [![coverage report](https://gitlab.com/cwenck/bittorrent/badges/master/coverage.svg)](https://gitlab.com/cwenck/bittorrent/commits/master)
## DEV : [![build status](https://gitlab.com/cwenck/bittorrent/badges/dev/build.svg)](https://gitlab.com/cwenck/bittorrent/commits/dev) [![coverage report](https://gitlab.com/cwenck/bittorrent/badges/dev/coverage.svg)](https://gitlab.com/cwenck/bittorrent/commits/dev)
------------------------

### Testing Library
Testing lib criterion can be found [here](https://github.com/Snaipe/Criterion)

### Project Spect
The project spec can be found [here](https://cs.umd.edu/class/spring2017/cmsc417-0101/public/assignments/final.pdf)

### Bencode Spec
The Bencode spec can be found [here](https://wiki.theory.org/BitTorrentSpecification#Bencoding)
