#include <criterion/criterion.h>
#include "netmsg_packer.h"

#include <stdlib.h>
#include "log.h"


Test(netmsg_packer, msg_keep_alive) {
    uint8_t *packet = NULL;
    size_t len = 0;

    pack_keep_alive(&packet, &len);

    static uint8_t expected_data[] = {0x00, 0x00, 0x00, 0x00};
    cr_assert_eq(len, 4, "incorrect packed size");
    cr_assert_arr_eq(packet, expected_data, 4, "incorrect packed data");

    enum msg_type type = MSG_TYPE_UNKNOWN;
    uint8_t *payload = packet;  // initialized to a non-null value to make sure it gets set
    size_t payload_len = 0;
    unpack_msg(packet, &type, &payload, &payload_len);

    cr_assert_eq(type, MSG_TYPE_KEEP_ALIVE, "incorrect unpacked message type");
    cr_assert_eq(payload_len, 0, "incorrect unpacked payload_len");
    cr_assert_not(payload, "incorrect unpacked payload");
}

Test(netmsg_packer, msg_choke) {
    uint8_t *packet = NULL;
    size_t len = 0;

    pack_choke(&packet, &len);

    static uint8_t expected_data[] = {0x00, 0x00, 0x00, 0x01, 0x00};
    cr_assert_eq(len, 5, "incorrect packed size");
    cr_assert_arr_eq(packet, expected_data, sizeof(expected_data), "incorrect packed data");

    enum msg_type type = MSG_TYPE_UNKNOWN;
    uint8_t *payload = packet;  // initialized to a non-null value to make sure it gets set
    size_t payload_len = 0;
    unpack_msg(packet, &type, &payload, &payload_len);

    cr_assert_eq(type, 0, "incorrect unpacked message type");
    cr_assert_eq(payload_len, 0, "incorrect unpacked payload_len");
    cr_assert_not(payload, "incorrect unpacked payload");
}

Test(netmsg_packer, msg_unchoke) {
    uint8_t *packet = NULL;
    size_t len = 0;

    pack_unchoke(&packet, &len);

    static uint8_t expected_data[] = {0x00, 0x00, 0x00, 0x01, 0x01};
    cr_assert_eq(len, 5, "incorrect packed size");
    cr_assert_arr_eq(packet, expected_data, sizeof(expected_data), "incorrect packed data");

    enum msg_type type = MSG_TYPE_UNKNOWN;
    uint8_t *payload = packet;  // initialized to a non-null value to make sure it gets set
    size_t payload_len = 0;
    unpack_msg(packet, &type, &payload, &payload_len);

    cr_assert_eq(type, 1, "incorrect unpacked message type");
    cr_assert_eq(payload_len, 0, "incorrect unpacked payload_len");
    cr_assert_not(payload, "incorrect unpacked payload");
}

Test(netmsg_packer, msg_interested) {
    uint8_t *packet = NULL;
    size_t len = 0;

    pack_interested(&packet, &len);

    static uint8_t expected_data[] = {0x00, 0x00, 0x00, 0x01, 0x02};
    cr_assert_eq(len, 5, "incorrect packed size");
    cr_assert_arr_eq(packet, expected_data, sizeof(expected_data), "incorrect packed data");

    enum msg_type type = MSG_TYPE_UNKNOWN;
    uint8_t *payload = packet;  // initialized to a non-null value to make sure it gets set
    size_t payload_len = 0;
    unpack_msg(packet, &type, &payload, &payload_len);

    cr_assert_eq(type, 2, "incorrect unpacked message type");
    cr_assert_eq(payload_len, 0, "incorrect unpacked payload_len");
    cr_assert_not(payload, "incorrect unpacked payload");
}

Test(netmsg_packer, msg_not_interested) {
    uint8_t *packet = NULL;
    size_t len = 0;

    pack_not_interested(&packet, &len);

    static uint8_t expected_data[] = {0x00, 0x00, 0x00, 0x01, 0x03};
    cr_assert_eq(len, 5, "incorrect packed size");
    cr_assert_arr_eq(packet, expected_data, sizeof(expected_data), "incorrect packed data");

    enum msg_type type = MSG_TYPE_UNKNOWN;
    uint8_t *payload = packet;  // initialized to a non-null value to make sure it gets set
    size_t payload_len = 0;
    unpack_msg(packet, &type, &payload, &payload_len);

    cr_assert_eq(type, 3, "incorrect unpacked message type");
    cr_assert_eq(payload_len, 0, "incorrect unpacked payload_len");
    cr_assert_not(payload, "incorrect unpacked payload");
}

Test(netmsg_packer, msg_have) {
    uint8_t *packet = NULL;
    size_t len = 0;

    pack_have(0xaabbccdd, &packet, &len);

    static uint8_t expected_data[] = {0x00, 0x00, 0x00, 0x05, 0x04, 0xaa, 0xbb, 0xcc, 0xdd};
    cr_expect_eq(len, 9, "incorrect packed size");
    cr_expect_arr_eq(packet, expected_data, sizeof(expected_data), "incorrect packed data");

    enum msg_type type = MSG_TYPE_UNKNOWN;
    uint8_t *payload = packet;  // initialized to a non-null value to make sure it gets set
    size_t payload_len = 0;
    unpack_msg(packet, &type, &payload, &payload_len);

    cr_expect_eq(type, 4, "incorrect unpacked message type");
    cr_expect_eq(payload_len, 4, "incorrect unpacked payload_len");
    cr_expect(payload, "incorrect unpacked payload");

    uint32_t piece_index = 0;
    unpack_have(payload, &piece_index);
    cr_expect_eq(piece_index, 0xaabbccdd, "incorrect unpacked piece index");

    free(packet);
}

Test(netmsg_packer, msg_bitfield) {
    uint8_t *packet = NULL;
    size_t len = 0;

    uint8_t bitfield[] = {0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xcd};
    pack_bitfield(bitfield, sizeof(bitfield), &packet, &len);

    static uint8_t expected_data[] = {0x00, 0x00, 0x00, 0x08, 0x05, 0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xcd};
    cr_expect_eq(len, 12, "incorrect packed size");
    cr_expect_arr_eq(packet, expected_data, sizeof(expected_data), "incorrect packed data");

    enum msg_type type = MSG_TYPE_UNKNOWN;
    uint8_t *payload = packet;  // initialized to a non-null value to make sure it gets set
    size_t payload_len = 0;
    unpack_msg(packet, &type, &payload, &payload_len);

    cr_expect_eq(type, 5, "incorrect unpacked message type");
    cr_expect_eq(payload_len, sizeof(bitfield), "incorrect unpacked payload_len");
    cr_expect(payload, "incorrect unpacked payload");

    uint8_t *unpacked_bitfield = NULL;
    unpack_bitfield(payload, payload_len, &unpacked_bitfield);
    cr_expect(unpacked_bitfield, "failed to unpack bitfield");
    cr_expect_arr_eq(unpacked_bitfield, bitfield, sizeof(bitfield), "incorrect unpacked bitfield");

    free(unpacked_bitfield);
    free(packet);
}

Test(netmsg_packer, msg_request) {
    uint8_t *packet = NULL;
    size_t len = 0;

    pack_request(0xaabbccdd, 0x01020304, 0xa1a2a3a4, &packet, &len);

    static uint8_t expected_data[] = {0x00, 0x00, 0x00, 0x0d, 0x06, 0xaa, 0xbb, 0xcc, 0xdd, 0x01, 0x02, 0x03, 0x04, 0xa1, 0xa2, 0xa3, 0xa4};
    cr_expect_eq(len, 17, "incorrect packed size");
    cr_expect_arr_eq(packet, expected_data, sizeof(expected_data), "incorrect packed data");

    enum msg_type type = MSG_TYPE_UNKNOWN;
    uint8_t *payload = packet;  // initialized to a non-null value to make sure it gets set
    size_t payload_len = 0;
    unpack_msg(packet, &type, &payload, &payload_len);

    cr_expect_eq(type, 6, "incorrect unpacked message type");
    cr_expect_eq(payload_len, 12, "incorrect unpacked payload_len");
    cr_expect(payload, "incorrect unpacked payload");

    uint32_t piece_index = 0;
    uint32_t offset = 0;
    uint32_t block_len = 0;
    unpack_request(payload, &piece_index, &offset, &block_len);

    cr_expect_eq(piece_index, 0xaabbccdd, "incorrect unpacked piece index");
    cr_expect_eq(offset, 0x01020304, "incorrect unpacked block offset");
    cr_expect_eq(block_len, 0xa1a2a3a4, "incorrect unpacked block length");

    free(packet);
}

Test(netmsg_packer, msg_piece) {
    uint8_t *packet = NULL;
    size_t len = 0;

    uint8_t block_data[] = {0xa1, 0xa2, 0xa3, 0xa4};
    pack_piece(0xaabbccdd, 0x01020304, block_data, sizeof(block_data), &packet, &len);

    static uint8_t expected_data[] = {0x00, 0x00, 0x00, 0x0d, 0x07, 0xaa, 0xbb, 0xcc, 0xdd, 0x01, 0x02, 0x03, 0x04, 0xa1, 0xa2, 0xa3, 0xa4};
    cr_expect_eq(len, 17, "incorrect packed size");
    cr_expect_arr_eq(packet, expected_data, sizeof(expected_data), "incorrect packed data");

    enum msg_type type = MSG_TYPE_UNKNOWN;
    uint8_t *payload = packet;  // initialized to a non-null value to make sure it gets set
    size_t payload_len = 0;
    unpack_msg(packet, &type, &payload, &payload_len);

    cr_expect_eq(type, 7, "incorrect unpacked message type");
    cr_expect_eq(payload_len, 8 + sizeof(block_data), "incorrect unpacked payload_len");
    cr_expect(payload, "incorrect unpacked payload");

    uint32_t piece_index = 0;
    uint32_t offset = 0;
    uint8_t *block = NULL;
    unpack_piece(payload, payload_len, &piece_index, &offset, &block);

    cr_expect_eq(piece_index, 0xaabbccdd, "incorrect unpacked piece index");
    cr_expect_eq(offset, 0x01020304, "incorrect unpacked block offset");
    cr_expect_arr_eq(block, block_data, sizeof(block_data), "incorrect unpacked block length");

    free(block);
    free(packet);
}

Test(netmsg_packer, msg_cancel) {
    uint8_t *packet = NULL;
    size_t len = 0;

    pack_cancel(0xaabbccdd, 0x01020304, 0xa1a2a3a4, &packet, &len);

    static uint8_t expected_data[] = {0x00, 0x00, 0x00, 0x0d, 0x08, 0xaa, 0xbb, 0xcc, 0xdd, 0x01, 0x02, 0x03, 0x04, 0xa1, 0xa2, 0xa3, 0xa4};
    cr_expect_eq(len, 17, "incorrect packed size");
    cr_expect_arr_eq(packet, expected_data, sizeof(expected_data), "incorrect packed data");

    enum msg_type type = MSG_TYPE_UNKNOWN;
    uint8_t *payload = packet;  // initialized to a non-null value to make sure it gets set
    size_t payload_len = 0;
    unpack_msg(packet, &type, &payload, &payload_len);

    cr_expect_eq(type, 8, "incorrect unpacked message type");
    cr_expect_eq(payload_len, 12, "incorrect unpacked payload_len");
    cr_expect(payload, "incorrect unpacked payload");

    uint32_t piece_index = 0;
    uint32_t offset = 0;
    uint32_t block_len = 0;
    unpack_cancel(payload, &piece_index, &offset, &block_len);

    cr_expect_eq(piece_index, 0xaabbccdd, "incorrect unpacked piece index");
    cr_expect_eq(offset, 0x01020304, "incorrect unpacked block offset");
    cr_expect_eq(block_len, 0xa1a2a3a4, "incorrect unpacked block length");

    free(packet);
}