#include <criterion/criterion.h>

#include <stdio.h>
#include <errno.h>

#include "common.h"
#include "metafile_parse.h"
#include "piece_set.h"
#include "log.h"

/* Prototypes for internal non-static functions not in piece_set header for testing */
bool pieceset_init(const char *filepath, size_t file_size, uint32_t piece_count, size_t piece_size, size_t block_size);

struct piece_data *pieceset_piece(uint32_t piece_index);
struct block_data *pieceset_block(uint32_t piece_index, uint32_t block_index);

static bool load_metafile(const char *filepath) {
    FILE *metafile = fopen(filepath, "r");
    uint8_t *buf;
    int len, ret;

    if (!metafile) {
        error_log("Error in opening file");
        return false;
    }

    fseek(metafile, 0L, SEEK_END);
    len = ftell(metafile);
    rewind(metafile);

    buf = calloc(len, 1);

    ret = fread(buf, 1, len, metafile);

    if (ret != len) {
        error_log("Issue with reading");
        return false;
    }

    g_metafile = parse_metafile(buf, len);

    if (!g_metafile) {
        error_log("Issue with parsing");
        return false;
    }

    free(buf);
    return true;
}

static void setup() {
    cr_assert(load_metafile("tests/metafiles/ubuntu-17.04-desktop-amd64.iso.torrent"));
    errno = 0;
}

static void teardown() {
    metafile_cleanup(g_metafile);
}

Test(piece_set, basic, .init = setup, .fini = teardown) {
    const char *filename = "__piece_set_basic_test.out";
    remove(filename);
    cr_assert(pieceset_init(filename, 1000, 10, 100, 10), "piece set init failed");

    cr_expect_eq(pieceset_piece(0)->len, 100, "incorrect first piece size");
    cr_expect_eq(pieceset_piece(3)->len, 100, "incorrect piece size");
    cr_expect_eq(pieceset_piece(9)->len, 100, "incorrect final piece size");

    pieceset_destroy();
    remove(filename);
}

Test(piece_set, basic2, .init = setup, .fini = teardown) {
    const char *filename = "__piece_set_basic2_test.out";
    remove(filename);
    cr_assert(pieceset_init(filename, 901, 10, 100, 10), "piece set init failed");

    cr_expect_eq(pieceset_piece(0)->len, 100, "incorrect first piece size");
    cr_expect_eq(pieceset_piece(3)->len, 100, "incorrect piece size");
    cr_expect_eq(pieceset_piece(9)->len, 1, "incorrect final piece size");

    pieceset_destroy();
    remove(filename);
}

Test(piece_set, basic3, .init = setup, .fini = teardown) {
    const char *filename = "__piece_set_basic3_test.out";
    remove(filename);
    cr_assert(pieceset_init(filename, 958, 10, 100, 10), "piece set init failed");

    cr_expect_eq(pieceset_piece(0)->len, 100, "incorrect first piece size");
    cr_expect_eq(pieceset_piece(3)->len, 100, "incorrect piece size");
    cr_expect_eq(pieceset_piece(9)->len, 58, "incorrect final piece size");

    pieceset_destroy();
    remove(filename);
}

Test(piece_set, basic_file_piece_mismatch, .init = setup, .fini = teardown) {
    const char *filename = "__piece_set_basic_file_piece_mismatch_test.out";
    remove(filename);
    cr_assert_not(pieceset_init(filename, 900, 10, 100, 10), "piece set init didn't fail with inconsistant params for file_set, piece_size, and/or piece_count");
}

Test(piece_set, basic_file_piece_mismatch2, .init = setup, .fini = teardown) {
    const char *filename = "__piece_set_basic_file_piece_mismatch2_test.out";
    remove(filename);
    cr_assert_not(pieceset_init(filename, 1001, 10, 100, 10), "piece set init didn't fail with inconsistant params for file_set, piece_size, and/or piece_count");
}

Test(piece_set, bitfield, .init = setup, .fini = teardown)  {
    const char *filename = "__piece_set_bitfield_test.out";
    remove(filename);

    cr_assert(pieceset_init(filename, 958, 10, 100, 10), "piece set init failed");

    uint8_t *bitfield = NULL;
    cr_expect(pieceset_generate_bitfield(&bitfield), "failed to generate bitfield");

    bool expected_bitfield[2] = {0};

    cr_expect_eq(piece_bitfield_size(), 2, "incorrect bitfield size");
    cr_expect_arr_eq(bitfield, expected_bitfield, 2, "incorret bitfield");


    free(bitfield);
    pieceset_destroy();
    remove(filename);
}

Test(piece_set, bitfield2, .init = setup, .fini = teardown)  {
    const char *filename = "__piece_set_bitfield_test.out";
    remove(filename);

    cr_assert(pieceset_init(filename, 958, 10, 100, 10), "piece set init failed");

    pieceset_piece(0)->status = PIECE_STORED;
    pieceset_piece(1)->status = PIECE_STORED;
    pieceset_piece(2)->status = PIECE_FOUND;
    pieceset_piece(3)->status = PIECE_STORED;
    pieceset_piece(6)->status = PIECE_DOWNLOADING;
    pieceset_piece(7)->status = PIECE_STORED;

    pieceset_piece(8)->status = PIECE_STORED;
    pieceset_piece(9)->status = PIECE_STORED;

    uint8_t *bitfield = NULL;
    cr_expect(pieceset_generate_bitfield(&bitfield), "failed to generate bitfield");

    /* {1 1 0 1 0 0 0 1}, {1 1 0 0 0 0 0 0}*/
    uint8_t expected_bitfield[2] = {0xd1, 0xc0};

    cr_expect_eq(piece_bitfield_size(), 2, "incorrect bitfield size");
    cr_expect_arr_eq(bitfield, expected_bitfield, 2, "incorret bitfield");

    free(bitfield);
    pieceset_destroy();
    remove(filename);
}

Test(piece_set, basic_block, .init = setup, .fini = teardown) {
    const char *filename = "__piece_set_basic_block_test.out";
    remove(filename);

    cr_assert(pieceset_init(filename, 958, 10, 100, 100), "piece set init failed");

    struct block_data *block;
    cr_expect(block = pieceset_block(9, 0), "failed to find block");

    for (int i = 0; i <= 8; i++) {
        cr_expect_eq(pieceset_block(i, 0)->len, 100, "incorrect block size");
        cr_expect_eq(pieceset_block(i, 0)->offset, 0, "incorrect block offset");
        cr_expect_eq(pieceset_block(i, 0)->status, BLOCK_MISSING, "incorrect block status");
    }

    cr_expect_eq(pieceset_block(9, 0)->len, 58, "incorrect block size");
    cr_expect_eq(pieceset_block(9, 0)->offset, 0, "incorrect block offset");
    cr_expect_eq(pieceset_block(9, 0)->status, BLOCK_MISSING, "incorrect block status");

    pieceset_destroy();
    remove(filename);
}

Test(piece_set, basic_block2, .init = setup, .fini = teardown) {
    const char *filename = "__piece_set_basic_block2_test.out";
    remove(filename);

    cr_assert(pieceset_init(filename, 958, 10, 100, 20), "piece set init failed");

    for (int i = 0; i <= 8; i++) {
        for (int j = 0; j < 5; j++) {
            cr_expect_eq(pieceset_block(i, j)->len, 20, "incorrect block size");
            cr_expect_eq(pieceset_block(i, j)->offset, j * 20, "incorrect block offset");
            cr_expect_eq(pieceset_block(i, j)->status, BLOCK_MISSING, "incorrect block status");
        }
    }


    cr_expect_eq(pieceset_block(9, 0)->len, 20, "incorrect block size");
    cr_expect_eq(pieceset_block(9, 0)->offset, 0, "incorrect block offset");
    cr_expect_eq(pieceset_block(9, 0)->status, BLOCK_MISSING, "incorrect block status");

    cr_expect_eq(pieceset_block(9, 1)->len, 20, "incorrect block size");
    cr_expect_eq(pieceset_block(9, 1)->offset, 20, "incorrect block offset");
    cr_expect_eq(pieceset_block(9, 1)->status, BLOCK_MISSING, "incorrect block status");

    cr_expect_eq(pieceset_block(9, 2)->len, 18, "incorrect block size");
    cr_expect_eq(pieceset_block(9, 2)->offset, 40, "incorrect block offset");
    cr_expect_eq(pieceset_block(9, 2)->status, BLOCK_MISSING, "incorrect block status");

    pieceset_destroy();
    remove(filename);
}

Test(piece_set, basic_block_requested, .init = setup, .fini = teardown) {
    const char *filename = "__piece_set_basic_block_requested_test.out";
    remove(filename);

    cr_assert(pieceset_init(filename, 958, 10, 100, 20), "piece set init failed");

    pieceset_mark_block_requested(pieceset_block(9, 0));
    pieceset_mark_block_requested(pieceset_block(9, 2));

    for (int i = 0; i <= 8; i++) {
        for (int j = 0; j < 5; j++) {
            cr_expect_eq(pieceset_block(i, j)->len, 20, "incorrect block size");
            cr_expect_eq(pieceset_block(i, j)->offset, j * 20, "incorrect block offset");
            cr_expect_eq(pieceset_block(i, j)->status, BLOCK_MISSING, "incorrect block status");
        }
    }


    cr_expect_eq(pieceset_block(9, 0)->len, 20, "incorrect block size");
    cr_expect_eq(pieceset_block(9, 0)->offset, 0, "incorrect block offset");
    cr_expect_eq(pieceset_block(9, 0)->status, BLOCK_REQUESTED, "incorrect block status");

    cr_expect_eq(pieceset_block(9, 1)->len, 20, "incorrect block size");
    cr_expect_eq(pieceset_block(9, 1)->offset, 20, "incorrect block offset");
    cr_expect_eq(pieceset_block(9, 1)->status, BLOCK_MISSING, "incorrect block status");

    cr_expect_eq(pieceset_block(9, 2)->len, 18, "incorrect block size");
    cr_expect_eq(pieceset_block(9, 2)->offset, 40, "incorrect block offset");
    cr_expect_eq(pieceset_block(9, 2)->status, BLOCK_REQUESTED, "incorrect block status");

    pieceset_destroy();
    remove(filename);
}

Test(piece_set, bitfield_convert, .init = setup, .fini = teardown) {
    const char *filename = "__piece_set_bitfield_convert`_test.out";
    remove(filename);
    cr_assert(pieceset_init(filename, 958, 10, 100, 10), "piece set init failed");

    bool piece_map[] = {true, true, false, true, false, false, false, true, true, true};
    uint8_t *bitfield = NULL;
    cr_expect(piece_map_to_bitfield(piece_map, &bitfield), "failed to make bitfield");

    /* {1 1 0 1 0 0 0 1}, {1 1 0 0 0 0 0 0}*/
    uint8_t expected_bitfield[2] = {0xd1, 0xc0};
    cr_expect_eq(piece_bitfield_size(), 2, "incorrect bitfield size");
    cr_expect_arr_eq(bitfield, expected_bitfield, 2, "incorret bitfield");


    bool *new_piece_map;
    cr_expect(piece_bitfield_to_map(bitfield, &new_piece_map));
    cr_expect_eq(piece_map_size(), 10, "incorrect bitfield size");
    cr_expect_arr_eq(piece_map, new_piece_map, 10, "incorret piece map");

    free(new_piece_map);
    free(bitfield);
    pieceset_destroy();
    remove(filename);
}