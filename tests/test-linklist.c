#include <criterion/criterion.h>
#include <stdint.h>

#include "lockless_linklist.h"

static void cleanup(void *data) {
    free(data);
}

Test(linked_list, create_destroy) {
    struct link_list *lst = linklist_create();
    cr_assert(lst, "failed to create list");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");
    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");
    linklist_destroy(lst, cleanup);
}

Test(linked_list, one_element_tryread) {
    struct link_list *lst = linklist_create();
    cr_assert(lst, "failed to create list");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");
    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");

    uint8_t data[] = {0xab, 0xcd, 0xef};

    cr_expect(linklist_append(lst, data, sizeof(data)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 1, "incorrect list size");

    uint8_t *read_data;
    size_t read_data_len = 0;
    cr_expect(linklist_tryread(lst, &read_data, &read_data_len), "failed to read from linked list");
    cr_expect(read_data, "data read is NULL for nonempty list");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");
    cr_expect_arr_eq(read_data, data, sizeof(data), "read data doesn't match stored data");

    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");
    linklist_destroy(lst, NULL);
}

Test(linked_list, one_element_read) {
    struct link_list *lst = linklist_create();
    cr_assert(lst, "failed to create list");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");
    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");

    uint8_t data[] = {0xab, 0xcd, 0xef};

    cr_expect(linklist_append(lst, data, sizeof(data)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 1, "incorrect list size");

    uint8_t *read_data;
    size_t read_data_len = 0;
    cr_expect(linklist_read(lst, &read_data, &read_data_len), "failed to read from linked list");
    cr_expect(read_data, "data read is NULL for nonempty list");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");
    cr_expect_arr_eq(read_data, data, sizeof(data), "read data doesn't match stored data");

    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");
    linklist_destroy(lst, NULL);
}

Test(linked_list, two_element_tryread) {
    struct link_list *lst = linklist_create();
    cr_assert(lst, "failed to create list");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");
    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");

    uint8_t data1[] = {0xab, 0xcd, 0xef};
    uint8_t data2[] = {0x01, 0x02, 0x03};

    cr_expect(linklist_append(lst, data1, sizeof(data1)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 1, "incorrect list size");

    cr_expect(linklist_append(lst, data2, sizeof(data2)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 2, "incorrect list size");

    uint8_t *read_data1;
    size_t read_data1_len = 0;
    cr_expect(linklist_tryread(lst, &read_data1, &read_data1_len), "failed to read from linked list");
    cr_expect(read_data1, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data1, data1, sizeof(data1), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 1, "incorrect list size");

    uint8_t *read_data2;
    size_t read_data2_len = 0;
    cr_expect(linklist_tryread(lst, &read_data2, &read_data2_len), "failed to read from linked list");
    cr_expect(read_data2, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data2, data2, sizeof(data2), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");

    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");
    linklist_destroy(lst, NULL);
}

Test(linked_list, two_element_read) {
    struct link_list *lst = linklist_create();
    cr_assert(lst, "failed to create list");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");
    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");

    uint8_t data1[] = {0xab, 0xcd, 0xef};
    uint8_t data2[] = {0x01, 0x02, 0x03};

    cr_expect(linklist_append(lst, data1, sizeof(data1)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 1, "incorrect list size");

    cr_expect(linklist_append(lst, data2, sizeof(data2)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 2, "incorrect list size");

    uint8_t *read_data1;
    size_t read_data1_len = 0;
    cr_expect(linklist_read(lst, &read_data1, &read_data1_len), "failed to read from linked list");
    cr_expect(read_data1, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data1, data1, sizeof(data1), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 1, "incorrect list size");

    uint8_t *read_data2;
    size_t read_data2_len = 0;
    cr_expect(linklist_read(lst, &read_data2, &read_data2_len), "failed to read from linked list");
    cr_expect(read_data2, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data2, data2, sizeof(data2), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");

    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");
    linklist_destroy(lst, NULL);
}

Test(linked_list, three_element_tryread) {
    struct link_list *lst = linklist_create();
    cr_assert(lst, "failed to create list");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");
    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");

    uint8_t data1[] = {0xab, 0xcd, 0xef};
    uint8_t data2[] = {0x01, 0x02, 0x03};
    uint8_t data3[] = {0x04, 0x05, 0x06};

    cr_expect(linklist_append(lst, data1, sizeof(data1)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 1, "incorrect list size");

    cr_expect(linklist_append(lst, data2, sizeof(data2)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 2, "incorrect list size");

    cr_expect(linklist_append(lst, data3, sizeof(data3)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 3, "incorrect list size");

    uint8_t *read_data1;
    size_t read_data1_len = 0;
    cr_expect(linklist_tryread(lst, &read_data1, &read_data1_len), "failed to read from linked list");
    cr_expect(read_data1, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data1, data1, sizeof(data1), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 2, "incorrect list size");

    uint8_t *read_data2;
    size_t read_data2_len = 0;
    cr_expect(linklist_tryread(lst, &read_data2, &read_data2_len), "failed to read from linked list");
    cr_expect(read_data2, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data2, data2, sizeof(data2), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 1, "incorrect list size");

    uint8_t *read_data3;
    size_t read_data3_len = 0;
    cr_expect(linklist_tryread(lst, &read_data3, &read_data3_len), "failed to read from linked list");
    cr_expect(read_data3, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data3, data3, sizeof(data3), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");

    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");
    linklist_destroy(lst, NULL);
}

Test(linked_list, three_element_read) {
    struct link_list *lst = linklist_create();
    cr_assert(lst, "failed to create list");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");
    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");

    uint8_t data1[] = {0xab, 0xcd, 0xef};
    uint8_t data2[] = {0x01, 0x02, 0x03};
    uint8_t data3[] = {0x04, 0x05, 0x06};

    cr_expect(linklist_append(lst, data1, sizeof(data1)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 1, "incorrect list size");

    cr_expect(linklist_append(lst, data2, sizeof(data2)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 2, "incorrect list size");

    cr_expect(linklist_append(lst, data3, sizeof(data3)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 3, "incorrect list size");

    uint8_t *read_data1;
    size_t read_data1_len = 0;
    cr_expect(linklist_read(lst, &read_data1, &read_data1_len), "failed to read from linked list");
    cr_expect(read_data1, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data1, data1, sizeof(data1), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 2, "incorrect list size");

    uint8_t *read_data2;
    size_t read_data2_len = 0;
    cr_expect(linklist_read(lst, &read_data2, &read_data2_len), "failed to read from linked list");
    cr_expect(read_data2, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data2, data2, sizeof(data2), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 1, "incorrect list size");

    uint8_t *read_data3;
    size_t read_data3_len = 0;
    cr_expect(linklist_read(lst, &read_data3, &read_data3_len), "failed to read from linked list");
    cr_expect(read_data3, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data3, data3, sizeof(data3), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");

    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");
    linklist_destroy(lst, NULL);
}

Test(linked_list, multi_element_tryread) {
    struct link_list *lst = linklist_create();
    cr_assert(lst, "failed to create list");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");
    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");

    uint8_t data1[] = {0xab, 0xcd, 0xef};
    uint8_t data2[] = {0x01, 0x02, 0x03};
    uint8_t data3[] = {0x04, 0x05, 0x06};
    uint8_t data4[] = {0x08, 0x09, 0x0a};
    uint8_t data5[] = {0x0b, 0x0c, 0x0d};

    cr_expect(linklist_append(lst, data1, sizeof(data1)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 1, "incorrect list size");

    cr_expect(linklist_append(lst, data2, sizeof(data2)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 2, "incorrect list size");

    cr_expect(linklist_append(lst, data3, sizeof(data3)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 3, "incorrect list size");

    cr_expect(linklist_append(lst, data4, sizeof(data4)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 4, "incorrect list size");

    cr_expect(linklist_append(lst, data5, sizeof(data5)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 5, "incorrect list size");

    uint8_t *read_data1;
    size_t read_data1_len = 0;
    cr_expect(linklist_tryread(lst, &read_data1, &read_data1_len), "failed to read from linked list");
    cr_expect(read_data1, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data1, data1, sizeof(data1), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 4, "incorrect list size");

    uint8_t *read_data2;
    size_t read_data2_len = 0;
    cr_expect(linklist_tryread(lst, &read_data2, &read_data2_len), "failed to read from linked list");
    cr_expect(read_data2, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data2, data2, sizeof(data2), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 3, "incorrect list size");

    uint8_t *read_data3;
    size_t read_data3_len = 0;
    cr_expect(linklist_tryread(lst, &read_data3, &read_data3_len), "failed to read from linked list");
    cr_expect(read_data3, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data3, data3, sizeof(data3), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 2, "incorrect list size");

    uint8_t *read_data4;
    size_t read_data4_len = 0;
    cr_expect(linklist_tryread(lst, &read_data4, &read_data4_len), "failed to read from linked list");
    cr_expect(read_data4, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data4, data4, sizeof(data4), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 1, "incorrect list size");

    uint8_t *read_data5;
    size_t read_data5_len = 0;
    cr_expect(linklist_tryread(lst, &read_data5, &read_data5_len), "failed to read from linked list");
    cr_expect(read_data5, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data5, data5, sizeof(data5), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");

    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");
    linklist_destroy(lst, NULL);
}

Test(linked_list, multi_element_read) {
    struct link_list *lst = linklist_create();
    cr_assert(lst, "failed to create list");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");
    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");

    uint8_t data1[] = {0xab, 0xcd, 0xef};
    uint8_t data2[] = {0x01, 0x02, 0x03};
    uint8_t data3[] = {0x04, 0x05, 0x06};
    uint8_t data4[] = {0x08, 0x09, 0x0a};
    uint8_t data5[] = {0x0b, 0x0c, 0x0d};

    cr_expect(linklist_append(lst, data1, sizeof(data1)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 1, "incorrect list size");

    cr_expect(linklist_append(lst, data2, sizeof(data2)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 2, "incorrect list size");

    cr_expect(linklist_append(lst, data3, sizeof(data3)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 3, "incorrect list size");

    cr_expect(linklist_append(lst, data4, sizeof(data4)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 4, "incorrect list size");

    cr_expect(linklist_append(lst, data5, sizeof(data5)), "failed to append to list");
    cr_expect_eq(linklist_size(lst), 5, "incorrect list size");

    uint8_t *read_data1;
    size_t read_data1_len = 0;
    cr_expect(linklist_read(lst, &read_data1, &read_data1_len), "failed to read from linked list");
    cr_expect(read_data1, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data1, data1, sizeof(data1), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 4, "incorrect list size");

    uint8_t *read_data2;
    size_t read_data2_len = 0;
    cr_expect(linklist_read(lst, &read_data2, &read_data2_len), "failed to read from linked list");
    cr_expect(read_data2, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data2, data2, sizeof(data2), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 3, "incorrect list size");

    uint8_t *read_data3;
    size_t read_data3_len = 0;
    cr_expect(linklist_read(lst, &read_data3, &read_data3_len), "failed to read from linked list");
    cr_expect(read_data3, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data3, data3, sizeof(data3), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 2, "incorrect list size");

    uint8_t *read_data4;
    size_t read_data4_len = 0;
    cr_expect(linklist_read(lst, &read_data4, &read_data4_len), "failed to read from linked list");
    cr_expect(read_data4, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data4, data4, sizeof(data4), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 1, "incorrect list size");

    uint8_t *read_data5;
    size_t read_data5_len = 0;
    cr_expect(linklist_read(lst, &read_data5, &read_data5_len), "failed to read from linked list");
    cr_expect(read_data5, "data read is NULL for nonempty list");
    cr_expect_arr_eq(read_data5, data5, sizeof(data5), "read data doesn't match stored data");
    cr_expect_eq(linklist_size(lst), 0, "incorrect list size");

    cr_expect_not(linklist_tryread(lst, NULL, NULL), "empty list doen't return null with tryread()");
    linklist_destroy(lst, NULL);
}