#include <criterion/criterion.h>

#include <stdio.h>
#include <signal.h>
#include "log.h"

// ReportHook(PRE_INIT)(struct criterion_test *test) {
//     printf("[" TXT_COLOR_MAGENTA "TEST START" TXT_COLOR_DEFAULT "] %s::%s" TXT_ALL_RESET "\n", test->category, test->name);
// }

ReportHook(POST_FINI)(struct criterion_test_stats *stats) {
    char *status = "";

    switch (stats->test_status) {
        case CR_STATUS_PASSED:
            status = TXT_FMT_BOLD TXT_COLOR_GREEN "PASSED" TXT_ALL_RESET;
            break;
        case CR_STATUS_SKIPPED:
            status = TXT_FMT_BOLD TXT_COLOR_YELLOW "SKIPPED" TXT_ALL_RESET;
            break;
        case CR_STATUS_FAILED:
            status = TXT_FMT_BOLD TXT_COLOR_RED "FAILED" TXT_ALL_RESET;
            break;
    }

    printf("[" TXT_COLOR_CYAN "TEST RESULT" TXT_COLOR_DEFAULT "] %s::%s --> %s" TXT_ALL_RESET "\n", stats->test->category, stats->test->name, status);
}


ReportHook(TEST_CRASH)(struct criterion_test_stats *stats) {
    printf("[" TXT_COLOR_RED "TEST CRASH" TXT_COLOR_DEFAULT "] %s::%s --> { SIGNAL = " TXT_FMT_BOLD TXT_COLOR_YELLOW "%s" TXT_ALL_RESET ", EXITCODE = %d }" TXT_ALL_RESET "\n", stats->test->category, stats->test->name, sys_siglist[stats->signal], stats->exit_code);
}