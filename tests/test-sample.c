#include <criterion/criterion.h>

/* Test(test_suite_name, test_name) */

Test(sample, test1) {
    cr_assert_str_eq("hello", "hello");
}

Test(sample, test2) {
    cr_assert_str_eq("hello", "world");
}
