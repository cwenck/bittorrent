#include <criterion/criterion.h>

#include "fileio.h"

#define __TEST_FILEIO_PIECE_SIZE 8

Test(fileio, only_mapping) {
    const char *filepath = "__fileio_only_mapping_testfile.out";
    remove(filepath);

    fileio_init(filepath, 500, __TEST_FILEIO_PIECE_SIZE);
    fileio_destroy();

    remove(filepath);
}

Test(fileio, write) {
    const char *filepath = "__fileio_write_testfile.out";
    remove(filepath);
    fileio_init(filepath, 500, __TEST_FILEIO_PIECE_SIZE);

    uint8_t block[__TEST_FILEIO_PIECE_SIZE] = { 1, 2, 3, 4, 5, 6, 7, 8 };
    cr_expect(fileio_write_block(5, 0, block, __TEST_FILEIO_PIECE_SIZE), "fileio block write failed");

    fileio_destroy();
    remove(filepath);
}

Test(fileio, read_write) {
    const char *filepath = "__fileio_read_write_testfile.out";
    remove(filepath);
    fileio_init(filepath, 500, __TEST_FILEIO_PIECE_SIZE);

    uint8_t block[__TEST_FILEIO_PIECE_SIZE] = { 1, 2, 3, 4, 5, 6, 7, 8 };
    cr_expect(fileio_write_block(5, 0, block, __TEST_FILEIO_PIECE_SIZE), "fileio block write failed");

    uint8_t read_block[__TEST_FILEIO_PIECE_SIZE] = {0};
    cr_expect(fileio_read_block(5, 0, read_block, __TEST_FILEIO_PIECE_SIZE), "fileio block read failed");

    cr_expect_arr_eq(read_block, block, __TEST_FILEIO_PIECE_SIZE);

    fileio_destroy();
    remove(filepath);
}

Test(fileio, overwrite) {
    const char *filepath = "__fileio_overwrite_testfile.out";
    remove(filepath);
    fileio_init(filepath, __TEST_FILEIO_PIECE_SIZE * 2, __TEST_FILEIO_PIECE_SIZE);

    uint8_t block[__TEST_FILEIO_PIECE_SIZE] = { 1, 2, 3, 4, 5, 6, 7, 8 };
    cr_expect_not(fileio_write_block(1, 1, block, __TEST_FILEIO_PIECE_SIZE), "fileio block write didn't fail on overwrite");
    cr_expect(fileio_write_block(1, 0, block, __TEST_FILEIO_PIECE_SIZE), "fileio block write failed");

    fileio_destroy();
    remove(filepath);
}

Test(fileio, overread) {
    const char *filepath = "__fileio_overread_testfile.out";
    remove(filepath);
    fileio_init(filepath, __TEST_FILEIO_PIECE_SIZE * 2, __TEST_FILEIO_PIECE_SIZE);

    uint8_t read_block[__TEST_FILEIO_PIECE_SIZE] = {0};
    cr_expect_not(fileio_read_block(1, 1, read_block, __TEST_FILEIO_PIECE_SIZE), "fileio block read didn't fail on overread");
    cr_expect(fileio_read_block(1, 0, read_block, __TEST_FILEIO_PIECE_SIZE), "fileio block read failed");

    fileio_destroy();
    remove(filepath);
}

Test(fileio, read_write_piece) {
    const char *filepath = "__fileio_read_write_piece_testfile.out";
    remove(filepath);
    fileio_init(filepath, 500, __TEST_FILEIO_PIECE_SIZE);

    uint8_t piece[__TEST_FILEIO_PIECE_SIZE] = { 1, 2, 3, 4, 5, 6, 7, 8 };
    cr_expect(fileio_write_piece(5, piece), "fileio piece write failed");

    uint8_t read_piece[__TEST_FILEIO_PIECE_SIZE] = {0};
    cr_expect(fileio_read_piece(5, read_piece), "fileio piece read failed");

    cr_expect_arr_eq(read_piece, piece, __TEST_FILEIO_PIECE_SIZE);

    fileio_destroy();
    remove(filepath);
}