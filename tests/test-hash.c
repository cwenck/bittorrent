#include <criterion/criterion.h>

#include "hash.h"

Test(hash, init_md5) {
    EVP_MD_CTX ctx;

    cr_assert(&ctx);
    cr_expect(checksum_init(&ctx, MD5));

    checksum_cleanup(&ctx);
}

Test(hash, init_sha1) {
    EVP_MD_CTX ctx;

    cr_assert(&ctx);
    cr_expect(checksum_init(&ctx, SHA1));

    checksum_cleanup(&ctx);
}

Test(hash, generate_md5) {
    const char *str = "helloworld";
    uint8_t expected_hash[MD5_SIZE] = {
        0xfc, 0x5e, 0x03, 0x8d, 0x38, 0xa5, 0x70, 0x32,
        0x08, 0x54, 0x41, 0xe7, 0xfe, 0x70, 0x10, 0xb0
    };

    EVP_MD_CTX ctx;

    cr_expect(checksum_init(&ctx, MD5), "failed to init context with MD5");

    uint8_t hash[MD5_SIZE] = {0};
    cr_expect(checksum_finish(&ctx, str, strlen(str), hash, MD5_SIZE), "failed to finish hash");

    cr_expect_arr_eq(hash, expected_hash, MD5_SIZE, "MD5 digest is not correct");

    checksum_cleanup(&ctx);
}

Test(hash, generate_sha1) {
    const char *str = "helloworld";
    uint8_t expected_hash[SHA1_SIZE] = {
        0x6a, 0xdf, 0xb1, 0x83, 0xa4,
        0xa2, 0xc9, 0x4a, 0x2f, 0x92,
        0xda, 0xb5, 0xad, 0xe7, 0x62,
        0xa4, 0x78, 0x89, 0xa5, 0xa1
    };

    EVP_MD_CTX ctx;

    cr_expect(checksum_init(&ctx, SHA1), "failed to init context with SHA1");

    uint8_t hash[SHA1_SIZE] = {0};
    cr_expect(checksum_finish(&ctx, str, strlen(str), hash, SHA1_SIZE), "failed to finish hash");

    cr_expect_arr_eq(hash, expected_hash, SHA1_SIZE, "SHA1 digest is not correct");

    checksum_cleanup(&ctx);
}

Test(hash, generate_md5_with_update) {
    const char *str1 = "hello";
    const char *str2 = "world";
    uint8_t expected_hash[MD5_SIZE] = {
        0xfc, 0x5e, 0x03, 0x8d, 0x38, 0xa5, 0x70, 0x32,
        0x08, 0x54, 0x41, 0xe7, 0xfe, 0x70, 0x10, 0xb0
    };

    EVP_MD_CTX ctx;

    cr_expect(checksum_init(&ctx, MD5), "failed to init context with MD5");

    cr_expect(checksum_update(&ctx, str1, strlen(str1)), "failed to update context");

    uint8_t hash[MD5_SIZE] = {0};
    cr_expect(checksum_finish(&ctx, str2, strlen(str2), hash, MD5_SIZE), "failed to finish hash");

    cr_expect_arr_eq(hash, expected_hash, MD5_SIZE, "MD5 digest is not correct");

    checksum_cleanup(&ctx);
}

Test(hash, generate_sha1_with_update) {
    const char *str1 = "hello";
    const char *str2 = "world";
    uint8_t expected_hash[SHA1_SIZE] = {
        0x6a, 0xdf, 0xb1, 0x83, 0xa4,
        0xa2, 0xc9, 0x4a, 0x2f, 0x92,
        0xda, 0xb5, 0xad, 0xe7, 0x62,
        0xa4, 0x78, 0x89, 0xa5, 0xa1
    };

    EVP_MD_CTX ctx;

    cr_expect(checksum_init(&ctx, SHA1), "failed to init context with SHA1");

    cr_expect(checksum_update(&ctx, str1, strlen(str1)), "failed to update context");

    uint8_t hash[SHA1_SIZE] = {0};
    cr_expect(checksum_finish(&ctx, str2, strlen(str2), hash, SHA1_SIZE), "failed to finish hash");

    cr_expect_arr_eq(hash, expected_hash, SHA1_SIZE, "SHA1 digest is not correct");

    checksum_cleanup(&ctx);
}

Test(hash, generate_md5_with_update2) {
    const char *str1 = "hello";
    const char *str2 = "world";
    uint8_t expected_hash[MD5_SIZE] = {
        0xfc, 0x5e, 0x03, 0x8d, 0x38, 0xa5, 0x70, 0x32,
        0x08, 0x54, 0x41, 0xe7, 0xfe, 0x70, 0x10, 0xb0
    };

    EVP_MD_CTX ctx;

    cr_expect(checksum_init(&ctx, MD5), "failed to init context with MD5");

    cr_expect(checksum_update(&ctx, str1, strlen(str1)), "failed to update context");
    cr_expect(checksum_update(&ctx, str2, strlen(str2)), "failed to update context");

    uint8_t hash[MD5_SIZE] = {0};
    cr_expect(checksum_finish(&ctx, NULL, 0, hash, MD5_SIZE), "failed to finish hash");

    cr_expect_arr_eq(hash, expected_hash, MD5_SIZE, "MD5 digest is not correct");

    checksum_cleanup(&ctx);
}

Test(hash, generate_sha1_with_update2) {
    const char *str1 = "hello";
    const char *str2 = "world";
    uint8_t expected_hash[SHA1_SIZE] = {
        0x6a, 0xdf, 0xb1, 0x83, 0xa4,
        0xa2, 0xc9, 0x4a, 0x2f, 0x92,
        0xda, 0xb5, 0xad, 0xe7, 0x62,
        0xa4, 0x78, 0x89, 0xa5, 0xa1
    };

    EVP_MD_CTX ctx;

    cr_expect(checksum_init(&ctx, SHA1), "failed to init context with SHA1");

    cr_expect(checksum_update(&ctx, str1, strlen(str1)), "failed to update context");
    cr_expect(checksum_update(&ctx, str2, strlen(str2)), "failed to update context");

    uint8_t hash[SHA1_SIZE] = {0};
    cr_expect(checksum_finish(&ctx, NULL, 0, hash, SHA1_SIZE), "failed to finish hash");

    cr_expect_arr_eq(hash, expected_hash, SHA1_SIZE, "SHA1 digest is not correct");

    checksum_cleanup(&ctx);
}

Test(hash, generate_md5_finish_failure) {
    EVP_MD_CTX ctx;

    cr_expect(checksum_init(&ctx, MD5), "failed to init context with MD5");

    uint8_t hash[MD5_SIZE] = {0};
    cr_expect_not(checksum_finish(&ctx, NULL, 1, hash, MD5_SIZE), "finish didn't rejct NULL data with nonzero length");

    checksum_cleanup(&ctx);
}

Test(hash, generate_sha1_finish_failure) {

    EVP_MD_CTX ctx;

    cr_expect(checksum_init(&ctx, SHA1), "failed to init context with SHA1");

    uint8_t hash[SHA1_SIZE] = {0};
    cr_expect_not(checksum_finish(&ctx, NULL, 1, hash, SHA1_SIZE), "finish didn't rejct NULL data with nonzero length");

    checksum_cleanup(&ctx);
}

Test(hash, generate_md5_finish_failure2) {
    const char *str = "helloworld";

    EVP_MD_CTX ctx;

    cr_expect(checksum_init(&ctx, MD5), "failed to init context with MD5");

    uint8_t hash[MD5_SIZE] = {0};
    cr_expect_not(checksum_finish(&ctx, str, strlen(str), hash, MD5_SIZE - 1), "finish didn't rejct NULL data with nonzero length");

    checksum_cleanup(&ctx);
}

Test(hash, generate_sha1_finish_failure2) {
    const char *str = "helloworld";

    EVP_MD_CTX ctx;

    cr_expect(checksum_init(&ctx, SHA1), "failed to init context with SHA1");

    uint8_t hash[SHA1_SIZE] = {0};
    cr_expect_not(checksum_finish(&ctx, str, strlen(str), hash, SHA1_SIZE - 1), "finish didn't rejct NULL data with nonzero length");

    checksum_cleanup(&ctx);
}