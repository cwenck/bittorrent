#include <criterion/criterion.h>
#include <stdint.h>

#include "tracker.h"

Test(tracker, url_encode) {
    const char *answer = "%124Vx%9A%BC%DE%F1%23Eg%89%AB%CD%EF%124Vx%9A";
    uint8_t blob[20] = {0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf1, 0x23,
        0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x12, 0x34, 0x56, 0x78, 0x9a};

    char *result = encode(blob, 20);
    
    cr_assert_str_eq(result, answer);
}
